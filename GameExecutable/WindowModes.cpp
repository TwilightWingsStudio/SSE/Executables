/* Copyright (c) 2022 by Dreamy Cecil & ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include "WindowModes.h"
#include "MainWindow.h"

// Make game application be aware of the DPI scaling on Windows Vista and later
static BOOL SetDPIAwareness(void) {
  typedef BOOL (*CSetAwarenessFunc)(void);

  // Load the library
  HMODULE hUser = LoadLibraryA("User32.dll");

  if (hUser == NULL) {
    return FALSE;
  }

  // Try to find the DPI awareness method
  CSetAwarenessFunc pFunc = (CSetAwarenessFunc)GetProcAddress(hUser, "SetProcessDPIAware");

  if (pFunc == NULL) {
    return FALSE;
  }

  // Execute it
  return pFunc();
};

// Mark game application as DPI-aware
static BOOL _bDPIAware = SetDPIAwareness();

// Window mode names
CTString _astrWindowModes[3] = { "", "", "" };

// Open the main application window in borderless mode
void OpenMainWindowBorderless(PIX pixSizeI, PIX pixSizeJ) {
  ASSERT(_hwndMainExe == NULL);

  // Create an invisible window
  _hwndMainExe = CreateWindowExA(WS_EX_APPWINDOW, APPLICATION_NAME, "",
    WS_POPUP, 0, 0, pixSizeI, pixSizeJ, NULL, NULL, _hInstance, NULL);

  // Can't create the window
  if (_hwndMainExe == NULL) {
    FatalError(TRANS("Cannot open main window!"));
  }

  SE_UpdateWindowHandle(_hwndMainExe);

  // Set window title and show it
  sprintf(_achWindowTitle, TRANS("Serious Sam (Borderless %dx%d)"), pixSizeI, pixSizeJ);
  SetWindowTextA(_hwndMainExe, _achWindowTitle);

  _pixLastSizeI = pixSizeI;
  _pixLastSizeJ = pixSizeJ;

  ResetMainWindowNormal();
};
