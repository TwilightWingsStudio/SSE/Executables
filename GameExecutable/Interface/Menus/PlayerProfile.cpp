/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "Interface/Common/MenuStuff.h"
#include "PlayerProfile.h"
#include "Interface/Common/MenuManager.h"

extern BOOL  _bPlayerMenuFromSinglePlayer;
extern CTString _strLastPlayerAppearance;
extern void PPOnPlayerSelect(void);

CPlayerProfileMenu::CPlayerProfileMenu()
{
  gm_pProfileTitle = new CTitleWidget();
  gm_pNoLabel = new CButtonWidget();
  
  for (INDEX iProfile = 0; iProfile < MAX_PLAYER_PROFILES; iProfile++)
  {
    CButtonWidget *pNew = new CButtonWidget();
    gm_pNumber[iProfile] = pNew;
  }

  gm_pNameLabel = new CButtonWidget();
  gm_pNameField = new CEditWidget();
  gm_pTeamLabel = new CButtonWidget();
  gm_pTeam = new CEditWidget();
  gm_pCustomizeControls = new CButtonWidget();
  gm_pCrosshair = new CTriggerWidget();
  gm_pWeaponSelect = new CTriggerWidget();
  gm_pWeaponHide = new CTriggerWidget();
  gm_p3rdPerson = new CTriggerWidget();
  gm_pQuotes = new CTriggerWidget();
  gm_pAutoSave = new CTriggerWidget();
  gm_pCompDoubleClick = new CTriggerWidget();
  gm_pViewBobbing = new CTriggerWidget();
  gm_pSharpTurning = new CTriggerWidget();
  gm_pModel = new CModelWidget();
}

void CPlayerProfileMenu::Initialize_t(void)
{
  // intialize player and controls menu
  _bPlayerMenuFromSinglePlayer = FALSE;
  gm_pProfileTitle->mg_boxOnScreen = BoxTitle();
  gm_pProfileTitle->SetText(TRANS("PLAYER PROFILE"));
  AddChild(gm_pProfileTitle);

  gm_pNoLabel->mg_strText = TRANS("PROFILE:");
  gm_pNoLabel->mg_boxOnScreen = BoxMediumLeft(0.0f);
  gm_pNoLabel->mg_bfsFontSize = BFS_MEDIUM;
  gm_pNoLabel->mg_iCenterI = -1;
  AddChild(gm_pNoLabel);

  for (INDEX i = 0; i < MAX_PLAYER_PROFILES; i++)
  {
    CButtonWidget *pPrev = i == 0 ? gm_pNumber[MAX_PLAYER_PROFILES - 1] : gm_pNumber[i - 1];
    CButtonWidget *pNext = i == (MAX_PLAYER_PROFILES - 1) ? gm_pNumber[0] : gm_pNumber[i + 1];
    CButtonWidget *pWidget = gm_pNumber[i];

    pWidget->mg_iIndex = i;
    pWidget->mg_bfsFontSize = BFS_MEDIUM;
    pWidget->mg_boxOnScreen = BoxNoUp(i);
    pWidget->mg_bRectangle = TRUE;
    pWidget->mg_pmgLeft = pPrev;
    pWidget->mg_pmgRight = pNext;
    pWidget->mg_pmgUp = gm_pCustomizeControls;
    pWidget->mg_pmgDown = gm_pNameField;
    pWidget->mg_pActivatedFunction = &PPOnPlayerSelect;
    pWidget->mg_strText.PrintF("%d", i);
    pWidget->SetTooltip(TRANS("select new currently active player"));
    AddChild(pWidget);
  }

  gm_pNumber[7]->mg_pmgRight = gm_pModel;

  gm_pNameLabel->mg_strText = TRANS("NAME:");
  gm_pNameLabel->mg_boxOnScreen = BoxMediumLeft(1.25f);
  gm_pNameLabel->mg_bfsFontSize = BFS_MEDIUM;
  gm_pNameLabel->mg_iCenterI = -1;
  AddChild(gm_pNameLabel);

  // setup of player name button is done on start menu
  gm_pNameField->mg_strText = "<???>";
  gm_pNameField->mg_ctMaxStringLen = 25;
  gm_pNameField->mg_boxOnScreen = BoxPlayerEdit(1.25);
  gm_pNameField->mg_bfsFontSize = BFS_MEDIUM;
  gm_pNameField->mg_iCenterI = -1;
  gm_pNameField->mg_pmgUp = gm_pNumber[0];
  gm_pNameField->mg_pmgDown = gm_pTeam;
  gm_pNameField->mg_pmgRight = gm_pModel;
  gm_pNameField->SetTooltip(TRANS("rename currently active player"));
  AddChild(gm_pNameField);

  // Team Label
  gm_pTeamLabel->mg_strText = TRANS("TEAM:");
  gm_pTeamLabel->mg_boxOnScreen = BoxMediumLeft(2.25f);
  gm_pTeamLabel->mg_bfsFontSize = BFS_MEDIUM;
  gm_pTeamLabel->mg_iCenterI = -1;
  AddChild(gm_pTeamLabel);

  // setup of player name button is done on start menu
  gm_pTeam->mg_strText = "<???>";
  gm_pTeam->mg_ctMaxStringLen = 25;
  gm_pTeam->mg_boxOnScreen = BoxPlayerEdit(2.25f);
  gm_pTeam->mg_bfsFontSize = BFS_MEDIUM;
  gm_pTeam->mg_iCenterI = -1;
  gm_pTeam->mg_pmgUp = gm_pNameField->mg_pmgUp = gm_pNumber[0];

  gm_pTeam->mg_pmgDown = gm_pCrosshair;
  gm_pTeam->mg_pmgRight = gm_pModel;
  //gm_pTeam->SetTooltip(TRANS("teamplay is disabled in this version");
  gm_pTeam->SetTooltip(TRANS("enter team name, if playing in team"));
  AddChild(gm_pTeam);

  // Crosshair
  gm_pCrosshair->mg_strLabel = TRANS("CROSSHAIR");
  gm_pCrosshair->mg_pmgUp = gm_pTeam;
  gm_pCrosshair->mg_pmgDown = gm_pWeaponSelect;
  gm_pCrosshair->mg_boxOnScreen = BoxPlayerSwitch(5.0f);
  gm_pCrosshair->mg_astrTexts = astrCrosshair;
  gm_pCrosshair->mg_ctTexts = sizeof(astrCrosshair) / sizeof(astrCrosshair[0]);
  gm_pCrosshair->mg_iSelected = 0;
  gm_pCrosshair->mg_strValue = astrCrosshair[0];
  gm_pCrosshair->mg_bVisual = TRUE;
  gm_pCrosshair->mg_iCenterI = -1;
  gm_pCrosshair->mg_pOnTriggerChange = NULL;
  AddChild(gm_pCrosshair);

  // Weapon Select
  gm_pWeaponSelect->mg_strLabel = TRANS("AUTO SELECT WEAPON");
  gm_pWeaponSelect->mg_pmgUp = gm_pCrosshair;
  gm_pWeaponSelect->mg_pmgDown = gm_pWeaponHide;
  gm_pWeaponSelect->mg_boxOnScreen = BoxPlayerSwitch(6.0f);
  gm_pWeaponSelect->mg_astrTexts = astrWeapon;
  gm_pWeaponSelect->mg_ctTexts = sizeof(astrWeapon) / sizeof(astrWeapon[0]);
  gm_pWeaponSelect->mg_iSelected = 0;
  gm_pWeaponSelect->mg_strValue = astrWeapon[0];
  gm_pWeaponSelect->mg_iCenterI = -1;
  gm_pWeaponSelect->mg_pOnTriggerChange = NULL;
  AddChild(gm_pWeaponSelect);

  // Weapon Hide
  gm_pWeaponHide->mg_strLabel = TRANS("HIDE WEAPON MODEL");
  gm_pWeaponHide->mg_pmgUp = gm_pWeaponSelect;
  gm_pWeaponHide->mg_pmgDown = gm_p3rdPerson;
  gm_pWeaponHide->mg_boxOnScreen = BoxPlayerSwitch(7.0f);
  gm_pWeaponHide->mg_astrTexts = astrNoYes;
  gm_pWeaponHide->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pWeaponHide->mg_iSelected = 0;
  gm_pWeaponHide->mg_strValue = astrNoYes[0];
  gm_pWeaponHide->mg_iCenterI = -1;
  gm_pWeaponHide->mg_pOnTriggerChange = NULL;
  AddChild(gm_pWeaponHide);
  
  // 3rd Person
  gm_p3rdPerson->mg_strLabel = TRANS("PREFER 3RD PERSON VIEW");
  gm_p3rdPerson->mg_pmgUp = gm_pWeaponHide;
  gm_p3rdPerson->mg_pmgDown = gm_pQuotes;
  gm_p3rdPerson->mg_boxOnScreen = BoxPlayerSwitch(8.0f);
  gm_p3rdPerson->mg_astrTexts = astrNoYes;
  gm_p3rdPerson->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_p3rdPerson->mg_iSelected = 0;
  gm_p3rdPerson->mg_strValue = astrNoYes[0];
  gm_p3rdPerson->mg_iCenterI = -1;
  gm_p3rdPerson->mg_pOnTriggerChange = NULL;

  // Voice Quotes
  gm_pQuotes->mg_strLabel = TRANS("VOICE QUOTES");
  gm_pQuotes->mg_pmgUp = gm_p3rdPerson;
  gm_pQuotes->mg_pmgDown = gm_pAutoSave;
  gm_pQuotes->mg_boxOnScreen = BoxMediumRow(0);
  gm_pQuotes->mg_boxOnScreen = BoxPlayerSwitch(9.0f);
  gm_pQuotes->mg_astrTexts = astrNoYes;
  gm_pQuotes->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pQuotes->mg_iSelected = 0;
  gm_pQuotes->mg_strValue = astrNoYes[0];
  gm_pQuotes->mg_iCenterI = -1;
  gm_pQuotes->mg_pOnTriggerChange = NULL;

  // Auto Save
  gm_pAutoSave->mg_strLabel = TRANS("AUTO SAVE");
  gm_pAutoSave->mg_pmgUp = gm_pQuotes;
  gm_pAutoSave->mg_pmgDown = gm_pCompDoubleClick;
  gm_pAutoSave->mg_boxOnScreen = BoxPlayerSwitch(10.0f);
  gm_pAutoSave->mg_astrTexts = astrNoYes;
  gm_pAutoSave->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pAutoSave->mg_iSelected = 0;
  gm_pAutoSave->mg_strValue = astrNoYes[0];
  gm_pAutoSave->mg_iCenterI = -1;
  gm_pAutoSave->mg_pOnTriggerChange = NULL;

  // Double Click
  gm_pCompDoubleClick->mg_strLabel = TRANS("INVOKE COMPUTER");
  gm_pCompDoubleClick->mg_pmgUp = gm_pAutoSave;
  gm_pCompDoubleClick->mg_pmgDown = gm_pSharpTurning;
  gm_pCompDoubleClick->mg_boxOnScreen = BoxPlayerSwitch(11.0f);
  gm_pCompDoubleClick->mg_astrTexts = astrNoYes;
  gm_pCompDoubleClick->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pCompDoubleClick->mg_iSelected = 0;
  gm_pCompDoubleClick->mg_strValue = astrNoYes[0];
  gm_pCompDoubleClick->mg_iCenterI = -1;
  gm_pCompDoubleClick->mg_pOnTriggerChange = NULL;

  // Sharp Turning
  gm_pSharpTurning->mg_strLabel = TRANS("SHARP TURNING");
  gm_pSharpTurning->mg_pmgUp = gm_pCompDoubleClick;
  gm_pSharpTurning->mg_pmgDown = gm_pViewBobbing;
  gm_pSharpTurning->mg_boxOnScreen = BoxPlayerSwitch(12.0f);
  gm_pSharpTurning->mg_astrTexts = astrNoYes;
  gm_pSharpTurning->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pSharpTurning->mg_iSelected = 0;
  gm_pSharpTurning->mg_strValue = astrNoYes[0];
  gm_pSharpTurning->mg_iCenterI = -1;
  gm_pSharpTurning->mg_pOnTriggerChange = NULL;

  // View Bobbing
  gm_pViewBobbing->mg_strLabel = TRANS("VIEW BOBBING");
  gm_pViewBobbing->mg_pmgUp = gm_pSharpTurning;
  gm_pViewBobbing->mg_pmgDown = gm_pCustomizeControls;
  gm_pViewBobbing->mg_boxOnScreen = BoxPlayerSwitch(13.0f);
  gm_pViewBobbing->mg_astrTexts = astrNoYes;
  gm_pViewBobbing->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pViewBobbing->mg_iSelected = 0;
  gm_pViewBobbing->mg_strValue = astrNoYes[0];
  gm_pViewBobbing->mg_iCenterI = -1;
  gm_pViewBobbing->mg_pOnTriggerChange = NULL;

  // Customize Controls
  gm_pCustomizeControls->mg_strText = TRANS("CUSTOMIZE CONTROLS");
  gm_pCustomizeControls->mg_boxOnScreen = BoxMediumLeft(14.5f);
  gm_pCustomizeControls->mg_bfsFontSize = BFS_MEDIUM;
  gm_pCustomizeControls->mg_iCenterI = -1;
  gm_pCustomizeControls->mg_pmgUp = gm_pViewBobbing;
  gm_pCustomizeControls->mg_pActivatedFunction = NULL;
  gm_pCustomizeControls->mg_pmgDown = gm_pNumber[0];
  gm_pCustomizeControls->mg_pmgRight = gm_pModel;
  gm_pCustomizeControls->SetTooltip(TRANS("customize controls for this player"));

  // Model
  gm_pModel->mg_boxOnScreen = BoxPlayerModel();
  gm_pModel->mg_pmgLeft = gm_pNameField;
  gm_pModel->mg_pActivatedFunction = NULL;
  gm_pModel->mg_pmgDown = gm_pNameField;
  gm_pModel->mg_pmgLeft = gm_pNameField;
  gm_pModel->SetTooltip(TRANS("change model for this player"));

  AddChild(gm_p3rdPerson);
  AddChild(gm_pQuotes);
  AddChild(gm_pAutoSave);
  AddChild(gm_pCompDoubleClick);
  AddChild(gm_pSharpTurning);
  AddChild(gm_pViewBobbing);
  AddChild(gm_pCustomizeControls);
  AddChild(gm_pModel);
}

INDEX CPlayerProfileMenu::ComboFromPlayer(INDEX iPlayer)
{
  return iPlayer;
}

INDEX CPlayerProfileMenu::PlayerFromCombo(INDEX iCombo)
{
  return iCombo;
}

void CPlayerProfileMenu::SelectPlayer(INDEX iPlayer)
{
  CPlayerCharacter &pc = _pGame->GetPlayers()[iPlayer];

  for (INDEX iPl = 0; iPl < 8; iPl++)
  {
    gm_pNumber[iPl]->mg_bHighlighted = FALSE;
  }

  gm_pNumber[iPlayer]->mg_bHighlighted = TRUE;

  iPlayer = Clamp(iPlayer, INDEX(0), INDEX(7));

  if (_iLocalPlayer >= 0 && _iLocalPlayer<4) {
    _pGame->GetMenuLocalPlayers()[_iLocalPlayer] = iPlayer;
  } else {
    _pGame->GetSinglePlayerIndexRef() = iPlayer;
  }

  gm_pNameField->mg_pstrToChange = &pc.pc_strName;
  gm_pNameField->SetText(*gm_pNameField->mg_pstrToChange);
  gm_pTeam->mg_pstrToChange = &pc.pc_strTeam;
  gm_pTeam->SetText(*gm_pTeam->mg_pstrToChange);

  CPlayerSettings *pps = (CPlayerSettings *)pc.pc_aubAppearance;

  gm_pCrosshair->mg_iSelected = pps->ps_iCrossHairType + 1;
  gm_pCrosshair->ApplyCurrentSelection();

  gm_pWeaponSelect->mg_iSelected = pps->ps_iWeaponAutoSelect;
  gm_pWeaponSelect->ApplyCurrentSelection();

  gm_pWeaponHide->mg_iSelected = (pps->ps_ulFlags&PSF_HIDEWEAPON) ? 1 : 0;
  gm_pWeaponHide->ApplyCurrentSelection();

  gm_p3rdPerson->mg_iSelected = (pps->ps_ulFlags&PSF_PREFER3RDPERSON) ? 1 : 0;
  gm_p3rdPerson->ApplyCurrentSelection();

  gm_pQuotes->mg_iSelected = (pps->ps_ulFlags&PSF_NOQUOTES) ? 0 : 1;
  gm_pQuotes->ApplyCurrentSelection();

  gm_pAutoSave->mg_iSelected = (pps->ps_ulFlags&PSF_AUTOSAVE) ? 1 : 0;
  gm_pAutoSave->ApplyCurrentSelection();

  gm_pCompDoubleClick->mg_iSelected = (pps->ps_ulFlags&PSF_COMPSINGLECLICK) ? 0 : 1;
  gm_pCompDoubleClick->ApplyCurrentSelection();

  gm_pViewBobbing->mg_iSelected = (pps->ps_ulFlags&PSF_NOBOBBING) ? 0 : 1;
  gm_pViewBobbing->ApplyCurrentSelection();

  gm_pSharpTurning->mg_iSelected = (pps->ps_ulFlags&PSF_SHARPTURNING) ? 1 : 0;
  gm_pSharpTurning->ApplyCurrentSelection();

  // get function that will set player appearance
  CShellSymbol *pss = _pShell->GetSymbol("SetPlayerAppearance", /*bDeclaredOnly=*/ TRUE);
  // if none
  if (pss == NULL) {
    // no model
    gm_pModel->mg_moModel.SetData(NULL);
    // if there is some
  } else {
    // set the model
    BOOL(*pFunc)(CModelObject *, CPlayerCharacter *, CTString &, BOOL) =
      (BOOL(*)(CModelObject *, CPlayerCharacter *, CTString &, BOOL))pss->ss_pvValue;
    CTString strName;
    BOOL bSet;
    if (_gmRunningGameMode != GM_SINGLE_PLAYER && !_bPlayerMenuFromSinglePlayer) {
      bSet = pFunc(&gm_pModel->mg_moModel, &pc, strName, TRUE);
      gm_pModel->SetTooltip(TRANS("change model for this player"));
      gm_pModel->mg_bEnabled = TRUE;
    }
    else {
      // cannot change player appearance in single player mode
      bSet = pFunc(&gm_pModel->mg_moModel, NULL, strName, TRUE);
      gm_pModel->SetTooltip(TRANS("cannot change model for single-player game"));
      gm_pModel->mg_bEnabled = FALSE;
    }
    // ignore gender flags, if any
    strName.RemovePrefix("#female#");
    strName.RemovePrefix("#male#");
    gm_pModel->mg_plModel = CPlacement3D(FLOAT3D(0.1f, -1.0f, -3.5f), ANGLE3D(150, 0, 0));
    gm_pModel->mg_strText = strName;
    CPlayerSettings *pps = (CPlayerSettings *)pc.pc_aubAppearance;
    _strLastPlayerAppearance = pps->GetModelFilename();
    try {
      gm_pModel->mg_moFloor.SetData_t(CTFILENAME("Models\\Computer\\Floor.mdl"));
      gm_pModel->mg_moFloor.mo_toTexture.SetData_t(CTFILENAME("Models\\Computer\\Floor.tex"));
    }
    catch (char *strError) {
      (void)strError;
    }
  }
}

void CPlayerProfileMenu::StartMenu(void)
{
  _pGUIM->gmPlayerProfile.gm_pmgSelectedByDefault = gm_pNameField;

  if (_gmRunningGameMode == GM_NONE || _gmRunningGameMode == GM_DEMO) {
    for (INDEX i = 0; i < 8; i++)
    {
      gm_pNumber[i]->mg_bEnabled = TRUE;
    }

  } else {
    for (INDEX i = 0; i < 8; i++)
    {
      gm_pNumber[i]->mg_bEnabled = FALSE;
    }

    INDEX iFirstEnabled = 0;

    {for (INDEX ilp = 0; ilp<4; ilp++) {
      const CLocalPlayer &lp = _pGame->GetLocalPlayers()[ilp];
      
      if (lp.lp_bActive) {
        gm_pNumber[lp.lp_iPlayer]->mg_bEnabled = TRUE;

        if (iFirstEnabled == 0) {
          iFirstEnabled = lp.lp_iPlayer;
        }
      }
    }}

    // backup to first player in case current player is disabled
    if (!gm_pNumber[*gm_piCurrentPlayer]->mg_bEnabled) *gm_piCurrentPlayer = iFirstEnabled;
  }
  // done
  SelectPlayer(*gm_piCurrentPlayer);
  CGameMenu::StartMenu();
}

void CPlayerProfileMenu::EndMenu(void)
{
  _pGame->SavePlayersAndControls();
  CGameMenu::EndMenu();
}