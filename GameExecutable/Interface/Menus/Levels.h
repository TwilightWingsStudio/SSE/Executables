/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_GAME_MENU_LEVELS_H
#define SE_INCL_GAME_MENU_LEVELS_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include "GameMenu.h"
#include "Interface/Widgets/Arrow.h"
#include "Interface/Widgets/LevelButton.h"
#include "Interface/Widgets/Title.h"

class CLevelsMenu : public CGameMenu
{
  public:
    CTitleWidget *gm_pTitle;
    CLevelButtonWidget *gm_pManualLevel[LEVELS_ON_SCREEN];
    CArrowWidget *gm_pArrowUp;
    CArrowWidget *gm_pArrowDn;

  public:
    //! Constructor.
    CLevelsMenu();

    void Initialize_t(void);
    void FillListItems(void);
    void StartMenu(void);
};

#endif  /* include-once check. */