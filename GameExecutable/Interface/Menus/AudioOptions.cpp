/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "Interface/Common/MenuStuff.h"
#include "AudioOptions.h"

extern void RefreshSoundFormat(void);

CAudioOptionsMenu::CAudioOptionsMenu()
{
  gm_pTitle = new CTitleWidget();
  gm_pAudioAutoTrigger = new CTriggerWidget();
  gm_pAudioAPITrigger = new CTriggerWidget();
  gm_pFrequencyTrigger = new CTriggerWidget();
  gm_pEffectsVolume = new CSliderWidget();
  gm_pMusicVolume = new CSliderWidget();
  gm_pApply = new CButtonWidget();
}

void CAudioOptionsMenu::Initialize_t(void)
{ 
  // intialize Audio options menu
  gm_pTitle->SetText(TRANS("AUDIO"));
  gm_pTitle->mg_boxOnScreen = BoxTitle();
  AddChild(gm_pTitle);

  // Auto Adjust
  gm_pAudioAutoTrigger->mg_strLabel = TRANS("AUTO-ADJUST");
  gm_pAudioAutoTrigger->mg_pmgUp = gm_pApply;
  gm_pAudioAutoTrigger->mg_pmgDown = gm_pFrequencyTrigger;
  gm_pAudioAutoTrigger->mg_boxOnScreen = BoxMediumRow(0);
  gm_pAudioAutoTrigger->mg_astrTexts = astrNoYes;
  gm_pAudioAutoTrigger->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pAudioAutoTrigger->mg_iSelected = 0;
  gm_pAudioAutoTrigger->mg_strValue = astrNoYes[0];
  AddChild(gm_pAudioAutoTrigger);
  gm_pAudioAutoTrigger->SetTooltip(TRANS("adjust quality to fit your system"));

  // Frequency
  gm_pFrequencyTrigger->mg_strLabel = TRANS("FREQUENCY");
  gm_pFrequencyTrigger->mg_pmgUp = gm_pAudioAutoTrigger;
  gm_pFrequencyTrigger->mg_pmgDown = gm_pAudioAPITrigger;
  gm_pFrequencyTrigger->mg_boxOnScreen = BoxMediumRow(1);
  gm_pFrequencyTrigger->mg_astrTexts = astrFrequencyRadioTexts;
  gm_pFrequencyTrigger->mg_ctTexts = sizeof(astrFrequencyRadioTexts) / sizeof(astrFrequencyRadioTexts[0]);
  gm_pFrequencyTrigger->mg_iSelected = 0;
  gm_pFrequencyTrigger->mg_strValue = astrNoYes[0];
  AddChild(gm_pFrequencyTrigger);
  gm_pFrequencyTrigger->SetTooltip(TRANS("select sound quality or turn sound off"));
  gm_pFrequencyTrigger->mg_pOnTriggerChange = NULL;
  
  // Audio API
  gm_pAudioAPITrigger->mg_pmgUp = gm_pFrequencyTrigger;
  gm_pAudioAPITrigger->mg_pmgDown = gm_pEffectsVolume;
  gm_pAudioAPITrigger->mg_boxOnScreen = BoxMediumRow(2);
  gm_pAudioAPITrigger->mg_astrTexts = astrSoundAPIRadioTexts;
  gm_pAudioAPITrigger->mg_ctTexts = sizeof(astrSoundAPIRadioTexts) / sizeof(astrSoundAPIRadioTexts[0]);
  gm_pAudioAPITrigger->mg_iSelected = 0;
  gm_pAudioAPITrigger->mg_strLabel = TRANS("SOUND SYSTEM");
  gm_pAudioAPITrigger->mg_strValue = astrNoYes[0];
  AddChild(gm_pAudioAPITrigger);
  gm_pAudioAPITrigger->SetTooltip(TRANS("choose sound system (API) to use"));
  gm_pAudioAPITrigger->mg_pOnTriggerChange = NULL;

  // Wave Volume
  gm_pEffectsVolume->mg_strText = TRANS("SOUND EFFECTS VOLUME");
  gm_pEffectsVolume->mg_boxOnScreen = BoxMediumRow(3);
  gm_pEffectsVolume->SetTooltip(TRANS("adjust volume of in-game sound effects"));
  gm_pEffectsVolume->mg_pmgUp = gm_pAudioAPITrigger;
  gm_pEffectsVolume->mg_pmgDown = gm_pMusicVolume;
  gm_pEffectsVolume->mg_pOnSliderChange = NULL;
  gm_pEffectsVolume->mg_pActivatedFunction = NULL;
  AddChild(gm_pEffectsVolume);

  // Music Volume
  gm_pMusicVolume->mg_strText = TRANS("MUSIC VOLUME");
  gm_pMusicVolume->mg_boxOnScreen = BoxMediumRow(4);
  gm_pMusicVolume->SetTooltip(TRANS("adjust volume of in-game music"));
  gm_pMusicVolume->mg_pmgUp = gm_pEffectsVolume;
  gm_pMusicVolume->mg_pmgDown = gm_pApply;
  gm_pMusicVolume->mg_pOnSliderChange = NULL;
  gm_pMusicVolume->mg_pActivatedFunction = NULL;
  AddChild(gm_pMusicVolume);

  // Apply
  gm_pApply->SetText(TRANS("APPLY"));
  gm_pApply->mg_bfsFontSize = BFS_LARGE;
  gm_pApply->mg_boxOnScreen = BoxBigRow(4);
  gm_pApply->SetTooltip(TRANS("activate selected options"));
  AddChild(gm_pApply);
  gm_pApply->mg_pmgUp = gm_pMusicVolume;
  gm_pApply->mg_pmgDown = gm_pAudioAutoTrigger;
  gm_pApply->mg_pActivatedFunction = NULL;
}

void CAudioOptionsMenu::StartMenu(void)
{
  RefreshSoundFormat();
  CGameMenu::StartMenu();
}