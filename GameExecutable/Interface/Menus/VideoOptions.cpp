/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "Interface/Common/MenuStuff.h"
#include "VideoOptions.h"

// Screen resolution lists and window modes
#include "ScreenResolutions.h"
#include "WindowModes.h"

extern void InitVideoOptionsButtons();
extern void UpdateVideoOptionsButtons(INDEX iSelected);

CVideoOptionsMenu::CVideoOptionsMenu()
{
  gm_pTitle = new CTitleWidget();
  gm_pDisplayAPITrigger = new CTriggerWidget();
  gm_pDisplayAdaptersTrigger = new CTriggerWidget();
  gm_pWindowModeTrigger = new CTriggerWidget();
  gm_pAspectRatiosTrigger = new CTriggerWidget();
  gm_pResolutionsTrigger = new CTriggerWidget();
  gm_pDisplayPrefsTrigger = new CTriggerWidget();
  gm_pVideoRendering = new CButtonWidget();
  gm_pBitsPerPixelTrigger = new CTriggerWidget();
  gm_pApply = new CButtonWidget();
}

void CVideoOptionsMenu::Initialize_t(void)
{
  // intialize video options menu
  gm_pTitle->mg_boxOnScreen = BoxTitle();
  gm_pTitle->SetText(TRANS("VIDEO"));
  AddChild(gm_pTitle);

  // API
  gm_pDisplayAPITrigger->mg_strLabel = TRANS("GRAPHICS API");
  gm_pDisplayAPITrigger->mg_pmgUp = gm_pApply;
  gm_pDisplayAPITrigger->mg_pmgDown = gm_pDisplayAdaptersTrigger;
  gm_pDisplayAPITrigger->mg_boxOnScreen = BoxMediumRow(0);
  gm_pDisplayAPITrigger->mg_astrTexts = astrDisplayAPIRadioTexts;
  gm_pDisplayAPITrigger->mg_ctTexts = sizeof(astrDisplayAPIRadioTexts) / sizeof(astrDisplayAPIRadioTexts[0]);
  gm_pDisplayAPITrigger->mg_iSelected = 0;
  gm_pDisplayAPITrigger->mg_strValue = astrDisplayAPIRadioTexts[0];
  gm_pDisplayAPITrigger->SetTooltip(TRANS("choose graphics API to be used"));
  AddChild(gm_pDisplayAPITrigger);

  // Display Adapter
  gm_pDisplayAdaptersTrigger->mg_strLabel = TRANS("DISPLAY ADAPTER");
  gm_pDisplayAdaptersTrigger->mg_pmgUp = gm_pDisplayAPITrigger;
  gm_pDisplayAdaptersTrigger->mg_pmgDown = gm_pDisplayPrefsTrigger;
  gm_pDisplayAdaptersTrigger->mg_boxOnScreen = BoxMediumRow(1);
  gm_pDisplayAdaptersTrigger->mg_astrTexts = astrNoYes;
  gm_pDisplayAdaptersTrigger->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pDisplayAdaptersTrigger->mg_iSelected = 0;
  gm_pDisplayAdaptersTrigger->mg_strValue = astrNoYes[0];
  gm_pDisplayAdaptersTrigger->SetTooltip(TRANS("choose display adapter to be used"));
  AddChild(gm_pDisplayAdaptersTrigger);

  // Preferences
  gm_pDisplayPrefsTrigger->mg_strLabel = TRANS("PREFERENCES");
  gm_pDisplayPrefsTrigger->mg_pmgUp = gm_pDisplayAdaptersTrigger;
  gm_pDisplayPrefsTrigger->mg_pmgDown = gm_pAspectRatiosTrigger;
  gm_pDisplayPrefsTrigger->mg_boxOnScreen = BoxMediumRow(2);
  gm_pDisplayPrefsTrigger->mg_astrTexts = astrDisplayPrefsRadioTexts;
  gm_pDisplayPrefsTrigger->mg_ctTexts = sizeof(astrDisplayPrefsRadioTexts) / sizeof(astrDisplayPrefsRadioTexts[0]);
  gm_pDisplayPrefsTrigger->mg_iSelected = 0;
  gm_pDisplayPrefsTrigger->mg_strValue = astrDisplayPrefsRadioTexts[0];
  gm_pDisplayPrefsTrigger->SetTooltip(TRANS("balance between speed and rendering quality, depending on your system"));
  AddChild(gm_pDisplayPrefsTrigger);

  // [SSE] Aspect ratio
  gm_pAspectRatiosTrigger->mg_strLabel = TRANS("ASPECT RATIO");
  gm_pAspectRatiosTrigger->mg_pmgUp = gm_pDisplayPrefsTrigger;
  gm_pAspectRatiosTrigger->mg_pmgDown = gm_pResolutionsTrigger;
  gm_pAspectRatiosTrigger->mg_boxOnScreen = BoxMediumRow(3);
  gm_pAspectRatiosTrigger->mg_astrTexts = _astrAspectRatios;
  gm_pAspectRatiosTrigger->mg_ctTexts = CT_ASPECTRATIOS;
  gm_pAspectRatiosTrigger->mg_iSelected = 0;
  gm_pAspectRatiosTrigger->mg_strValue = _astrAspectRatios[0];
  gm_pAspectRatiosTrigger->SetTooltip(TRANS("select video mode aspect ratio"));
  AddChild(gm_pAspectRatiosTrigger);

  // Resolution
  gm_pResolutionsTrigger->mg_strLabel = TRANS("RESOLUTION");
  gm_pResolutionsTrigger->mg_pmgUp = gm_pAspectRatiosTrigger;
  gm_pResolutionsTrigger->mg_pmgDown = gm_pWindowModeTrigger;
  gm_pResolutionsTrigger->mg_boxOnScreen = BoxMediumRow(4);
  gm_pResolutionsTrigger->mg_astrTexts = astrNoYes;
  gm_pResolutionsTrigger->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pResolutionsTrigger->mg_iSelected = 0;
  gm_pResolutionsTrigger->mg_strValue = astrNoYes[0];
  gm_pResolutionsTrigger->SetTooltip(TRANS("select video mode resolution"));
  AddChild(gm_pResolutionsTrigger);
  
  // Fullscren
  gm_pWindowModeTrigger->mg_strLabel = TRANS("WINDOW MODE");
  gm_pWindowModeTrigger->mg_pmgUp = gm_pResolutionsTrigger;
  gm_pWindowModeTrigger->mg_pmgDown = gm_pBitsPerPixelTrigger;
  gm_pWindowModeTrigger->mg_boxOnScreen = BoxMediumRow(5);
  gm_pWindowModeTrigger->mg_astrTexts = _astrWindowModes;
  gm_pWindowModeTrigger->mg_ctTexts = E_WM_MAX;
  gm_pWindowModeTrigger->mg_iSelected = 0;
  gm_pWindowModeTrigger->mg_strValue = _astrWindowModes[0];
  gm_pWindowModeTrigger->SetTooltip(TRANS("make game run in a window or in full screen"));
  AddChild(gm_pWindowModeTrigger);
  
  // BPP
  gm_pBitsPerPixelTrigger->mg_strLabel = TRANS("BITS PER PIXEL");
  gm_pBitsPerPixelTrigger->mg_pmgUp = gm_pWindowModeTrigger;
  gm_pBitsPerPixelTrigger->mg_pmgDown = gm_pVideoRendering;
  gm_pBitsPerPixelTrigger->mg_boxOnScreen = BoxMediumRow(6);
  gm_pBitsPerPixelTrigger->mg_astrTexts = astrBitsPerPixelRadioTexts;
  gm_pBitsPerPixelTrigger->mg_ctTexts = sizeof(astrBitsPerPixelRadioTexts) / sizeof(astrBitsPerPixelRadioTexts[0]);
  gm_pBitsPerPixelTrigger->mg_iSelected = 0;
  gm_pBitsPerPixelTrigger->mg_strValue = astrBitsPerPixelRadioTexts[0];
  gm_pBitsPerPixelTrigger->SetTooltip(TRANS("select number of colors used for display"));
  AddChild(gm_pBitsPerPixelTrigger);

  gm_pDisplayPrefsTrigger->mg_pOnTriggerChange = NULL;
  gm_pDisplayAPITrigger->mg_pOnTriggerChange = NULL;
  gm_pDisplayAdaptersTrigger->mg_pOnTriggerChange = NULL;
  gm_pWindowModeTrigger->mg_pOnTriggerChange = NULL;
  gm_pAspectRatiosTrigger->mg_pOnTriggerChange = NULL; // [SSE]
  gm_pResolutionsTrigger->mg_pOnTriggerChange = NULL;
  gm_pBitsPerPixelTrigger->mg_pOnTriggerChange = NULL;

  gm_pVideoRendering->SetText(TRANS("RENDERING OPTIONS"));
  gm_pVideoRendering->mg_bfsFontSize = BFS_MEDIUM;
  gm_pVideoRendering->mg_boxOnScreen = BoxMediumRow(8);
  gm_pVideoRendering->mg_pmgUp = gm_pBitsPerPixelTrigger;
  gm_pVideoRendering->mg_pmgDown = gm_pApply;
  gm_pVideoRendering->SetTooltip(TRANS("manually adjust rendering settings"));
  AddChild(gm_pVideoRendering);
  gm_pVideoRendering->mg_pActivatedFunction = NULL;

  gm_pApply->SetText(TRANS("APPLY"));
  gm_pApply->mg_bfsFontSize = BFS_LARGE;
  gm_pApply->mg_boxOnScreen = BoxBigRow(6.5f);
  gm_pApply->mg_pmgUp = gm_pVideoRendering;
  gm_pApply->mg_pmgDown = gm_pDisplayAPITrigger;
  gm_pApply->SetTooltip(TRANS("apply selected options"));
  AddChild(gm_pApply);
  gm_pApply->mg_pActivatedFunction = NULL;
}

void CVideoOptionsMenu::StartMenu(void)
{
  InitVideoOptionsButtons();

  CGameMenu::StartMenu();

  UpdateVideoOptionsButtons(-1);
}