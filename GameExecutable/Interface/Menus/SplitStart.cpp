/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "LevelInfo.h"
#include "Interface/Common/MenuStuff.h"
#include "SplitStart.h"

extern void UpdateSplitLevel(INDEX iDummy);

CSplitStartMenu::CSplitStartMenu()
{
  gm_pTitle = new CTitleWidget();
  gm_pGameType = new CTriggerWidget();
  gm_pDifficulty = new CTriggerWidget();
  gm_pLevel = new CButtonWidget();
  gm_pCustomizeGame = new CTriggerWidget();
  gm_pGameOptions = new CButtonWidget();
  gm_pStart = new CButtonWidget();
}

void CSplitStartMenu::Initialize_t(void)
{
  // intialize split screen menu
  gm_pTitle->mg_boxOnScreen = BoxTitle();
  gm_pTitle->SetText(TRANS("START SPLIT SCREEN"));

  // game type trigger
  gm_pGameType->mg_strLabel = TRANS("Game type:");
  gm_pGameType->mg_pmgUp = gm_pStart;
  gm_pGameType->mg_pmgDown = gm_pDifficulty;
  gm_pGameType->mg_boxOnScreen = BoxMediumRow(1);
  gm_pGameType->mg_astrTexts = astrGameTypeRadioTexts;
  gm_pGameType->mg_ctTexts = ctGameTypeRadioTexts;
  gm_pGameType->mg_iSelected = 0;
  gm_pGameType->mg_strValue = astrGameTypeRadioTexts[0];
  gm_pGameType->SetTooltip(TRANS("choose type of multiplayer game"));
  gm_pGameType->mg_pOnTriggerChange = &UpdateSplitLevel;

  // difficulty trigger
  gm_pDifficulty->mg_strLabel = TRANS("Difficulty:");
  gm_pDifficulty->mg_pmgUp = gm_pGameType;
  gm_pDifficulty->mg_pmgDown = gm_pLevel;
  gm_pDifficulty->mg_boxOnScreen = BoxMediumRow(2);
  gm_pDifficulty->mg_astrTexts = astrDifficultyRadioTexts;
  gm_pDifficulty->mg_ctTexts = sizeof(astrDifficultyRadioTexts) / sizeof(astrDifficultyRadioTexts[0]);
  gm_pDifficulty->mg_iSelected = 0;
  gm_pDifficulty->mg_strValue = astrDifficultyRadioTexts[0];
  gm_pDifficulty->SetTooltip(TRANS("choose difficulty level"));

  // level name
  gm_pLevel->mg_strText = "";
  gm_pLevel->mg_strLabel = TRANS("Level:");
  gm_pLevel->mg_boxOnScreen = BoxMediumRow(3);
  gm_pLevel->mg_bfsFontSize = BFS_MEDIUM;
  gm_pLevel->mg_iCenterI = -1;
  gm_pLevel->mg_pmgUp = gm_pDifficulty;
  gm_pLevel->mg_pmgDown = gm_pCustomizeGame;
  gm_pLevel->SetTooltip(TRANS("choose the level to start"));
  gm_pLevel->mg_pActivatedFunction = NULL;

  // server visible trigger
  gm_pCustomizeGame->mg_strLabel = TRANS("Customize game:");
  gm_pCustomizeGame->mg_pmgUp = gm_pLevel;
  gm_pCustomizeGame->mg_pmgDown = gm_pGameOptions;
  gm_pCustomizeGame->mg_boxOnScreen = BoxMediumRow(4);
  gm_pCustomizeGame->mg_astrTexts = astrNoYes;
  gm_pCustomizeGame->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pCustomizeGame->mg_iSelected = 0;
  gm_pCustomizeGame->mg_strValue = astrNoYes[0];
  gm_pCustomizeGame->SetTooltip(TRANS(""));

  // options button
  gm_pGameOptions->SetText(TRANS("Game options"));
  gm_pGameOptions->mg_boxOnScreen = BoxMediumRow(5);
  gm_pGameOptions->mg_bfsFontSize = BFS_MEDIUM;
  gm_pGameOptions->mg_iCenterI = 0;
  gm_pGameOptions->mg_pmgUp = gm_pCustomizeGame;
  gm_pGameOptions->mg_pmgDown = gm_pStart;
  gm_pGameOptions->SetTooltip(TRANS("adjust game rules"));
  gm_pGameOptions->mg_pActivatedFunction = NULL;

  // start button
  gm_pStart->SetText(TRANS("START"));
  gm_pStart->mg_bfsFontSize = BFS_LARGE;
  gm_pStart->mg_boxOnScreen = BoxBigRow(6);
  gm_pStart->mg_pmgUp = gm_pGameOptions;
  gm_pStart->mg_pmgDown = gm_pGameType;
  gm_pStart->mg_pActivatedFunction = NULL;

  // Add them.
  AddChild(gm_pTitle);
  AddChild(gm_pDifficulty);
  AddChild(gm_pGameType);
  AddChild(gm_pLevel);
  AddChild(gm_pCustomizeGame);
  AddChild(gm_pGameOptions);
  AddChild(gm_pStart);
}

void CSplitStartMenu::StartMenu(void)
{
  extern INDEX sam_bMentalActivated;
  gm_pDifficulty->mg_ctTexts = sam_bMentalActivated ? 6 : 5;

  gm_pGameType->mg_iSelected = Clamp(_pShell->GetINDEX("gam_iStartMode"), 0L, ctGameTypeRadioTexts - 1L);
  gm_pGameType->ApplyCurrentSelection();
  gm_pDifficulty->mg_iSelected = _pShell->GetINDEX("gam_iStartDifficulty") + 1;
  gm_pDifficulty->ApplyCurrentSelection();

  // clamp maximum number of players to at least 4
  _pShell->SetINDEX("gam_ctMaxPlayers", ClampDn(_pShell->GetINDEX("gam_ctMaxPlayers"), 4L));
  
  gm_pCustomizeGame->mg_iSelected = _pShell->GetINDEX("gam_bCustomizedGame");
  gm_pCustomizeGame->ApplyCurrentSelection();

  UpdateSplitLevel(0);
  CGameMenu::StartMenu();
}

void CSplitStartMenu::EndMenu(void)
{
  _pShell->SetINDEX("gam_iStartDifficulty", gm_pDifficulty->mg_iSelected - 1);
  _pShell->SetINDEX("gam_iStartMode", gm_pGameType->mg_iSelected);
  _pShell->SetINDEX("gam_bCustomizedGame", gm_pCustomizeGame->mg_iSelected);

  CGameMenu::EndMenu();
}