/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "SplitScreen.h"

CSplitScreenMenu::CSplitScreenMenu()
{
  gm_pTitle = new CTitleWidget();
  gm_pStart = new CButtonWidget();
  gm_pQuickLoad = new CButtonWidget();
  gm_pLoad = new CButtonWidget();  
}

void CSplitScreenMenu::Initialize_t(void)
{
  // intialize split screen menu
  gm_pTitle->SetText(TRANS("SPLIT SCREEN"));
  gm_pTitle->mg_boxOnScreen = BoxTitle();
  AddChild(gm_pTitle);

  // New Game
  gm_pStart->SetText(TRANS("NEW GAME"));
  gm_pStart->mg_bfsFontSize = BFS_LARGE;
  gm_pStart->mg_boxOnScreen = BoxBigRow(0);
  gm_pStart->mg_pmgUp = gm_pLoad;
  gm_pStart->mg_pmgDown = gm_pQuickLoad;
  gm_pStart->SetTooltip(TRANS("start new split-screen game"));
  AddChild(gm_pStart);
  gm_pStart->mg_pActivatedFunction = NULL;

  // Quick Load
  gm_pQuickLoad->SetText(TRANS("QUICK LOAD"));
  gm_pQuickLoad->mg_bfsFontSize = BFS_LARGE;
  gm_pQuickLoad->mg_boxOnScreen = BoxBigRow(1);
  gm_pQuickLoad->mg_pmgUp = gm_pStart;
  gm_pQuickLoad->mg_pmgDown = gm_pLoad;
  gm_pQuickLoad->SetTooltip(TRANS("load a quick-saved game (F9)"));
  AddChild(gm_pQuickLoad);
  gm_pQuickLoad->mg_pActivatedFunction = NULL;

  // Load
  gm_pLoad->SetText(TRANS("LOAD"));
  gm_pLoad->mg_bfsFontSize = BFS_LARGE;
  gm_pLoad->mg_boxOnScreen = BoxBigRow(2);
  gm_pLoad->mg_pmgUp = gm_pQuickLoad;
  gm_pLoad->mg_pmgDown = gm_pStart;
  gm_pLoad->SetTooltip(TRANS("load a saved split-screen game"));
  AddChild(gm_pLoad);
  gm_pLoad->mg_pActivatedFunction = NULL;
}

void CSplitScreenMenu::StartMenu(void)
{
  CGameMenu::StartMenu();
}