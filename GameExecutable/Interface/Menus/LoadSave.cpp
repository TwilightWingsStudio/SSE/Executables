/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "Interface/Common/MenuStuff.h"
#include "LoadSave.h"

CLoadSaveMenu::CLoadSaveMenu()
{
  gm_pTitle = new CTitleWidget();
  gm_pNotes = new CButtonWidget();
  gm_pArrowUp = new CArrowWidget();
  gm_pArrowDn = new CArrowWidget();
  
  for (INDEX iButton = 0; iButton < SAVELOAD_BUTTONS_CT; iButton++)
  {
    gm_pButtons[iButton] = new CFileButtonWidget();
  }
}

void CLoadSaveMenu::Initialize_t(void)
{
  gm_pgmNextMenu = NULL;

  gm_pTitle->mg_boxOnScreen = BoxTitle();
  AddChild(gm_pTitle);

  gm_pNotes->mg_boxOnScreen = BoxMediumRow(10.0);
  gm_pNotes->mg_bfsFontSize = BFS_MEDIUM;
  gm_pNotes->mg_iCenterI = -1;
  gm_pNotes->mg_bEnabled = FALSE;
  gm_pNotes->mg_bLabel = TRUE;
  AddChild(gm_pNotes);

  for (INDEX iLabel = 0; iLabel < SAVELOAD_BUTTONS_CT; iLabel++)
  {
    INDEX iPrev = (SAVELOAD_BUTTONS_CT + iLabel - 1) % SAVELOAD_BUTTONS_CT;
    INDEX iNext = (iLabel + 1) % SAVELOAD_BUTTONS_CT;
    // initialize label gadgets
    gm_pButtons[iLabel]->mg_pmgUp = gm_pButtons[iPrev];
    gm_pButtons[iLabel]->mg_pmgDown = gm_pButtons[iNext];
    gm_pButtons[iLabel]->mg_boxOnScreen = BoxSaveLoad(iLabel);
    gm_pButtons[iLabel]->mg_pActivatedFunction = NULL; // never called!
    gm_pButtons[iLabel]->mg_iCenterI = -1;
    AddChild(gm_pButtons[iLabel]);
  }

  AddChild(gm_pArrowUp);
  AddChild(gm_pArrowDn);
  gm_pArrowUp->mg_adDirection = AD_UP;
  gm_pArrowDn->mg_adDirection = AD_DOWN;
  gm_pArrowUp->mg_boxOnScreen = BoxArrow(AD_UP);
  gm_pArrowDn->mg_boxOnScreen = BoxArrow(AD_DOWN);
  gm_pArrowUp->mg_pmgRight = gm_pArrowUp->mg_pmgDown = gm_pButtons[0];
  gm_pArrowDn->mg_pmgRight = gm_pArrowDn->mg_pmgUp = gm_pButtons[SAVELOAD_BUTTONS_CT - 1];

  gm_ctListVisible = SAVELOAD_BUTTONS_CT;
  gm_pmgArrowUp = gm_pArrowUp;
  gm_pmgArrowDn = gm_pArrowDn;
  gm_pmgListTop = gm_pButtons[0];
  gm_pmgListBottom = gm_pButtons[SAVELOAD_BUTTONS_CT - 1];
}

void CLoadSaveMenu::StartMenu(void)
{
  gm_bNoEscape = FALSE;

  // delete all file infos
  FORDELETELIST(CFileInfo, fi_lnNode, gm_lhFileInfos, itfi) {
    delete &itfi.Current();
  }

  // list the directory
  CDynamicStackArray<CTFileName> afnmDir;
  MakeDirList(afnmDir, gm_fnmDirectory, "", 0);
  gm_iLastFile = -1;

  // for each file in the directory
  for (INDEX i = 0; i<afnmDir.Count(); i++) {
    CTFileName fnm = afnmDir[i];

    // if it can be parsed
    CTString strName;
    if (ParseFile(fnm, strName)) {
      // create new info for that file
      CFileInfo *pfi = new CFileInfo;
      pfi->fi_fnFile = fnm;
      pfi->fi_strName = strName;
      // add it to list
      gm_lhFileInfos.AddTail(pfi->fi_lnNode);
    }
  }

  // sort if needed
  switch (gm_iSortType) {
  default: ASSERT(FALSE);
  case LSSORT_NONE: break;
  case LSSORT_NAMEUP:
    gm_lhFileInfos.Sort(qsort_CompareFileInfos_NameUp, offsetof(CFileInfo, fi_lnNode));
    break;
  case LSSORT_NAMEDN:
    gm_lhFileInfos.Sort(qsort_CompareFileInfos_NameDn, offsetof(CFileInfo, fi_lnNode));
    break;
  case LSSORT_FILEUP:
    gm_lhFileInfos.Sort(qsort_CompareFileInfos_FileUp, offsetof(CFileInfo, fi_lnNode));
    break;
  case LSSORT_FILEDN:
    gm_lhFileInfos.Sort(qsort_CompareFileInfos_FileDn, offsetof(CFileInfo, fi_lnNode));
    break;
  }

  // if saving
  if (gm_bSave) {
    // add one info as empty slot
    CFileInfo *pfi = new CFileInfo;
    CTString strNumber;
    strNumber.PrintF("%04d", gm_iLastFile + 1);
    pfi->fi_fnFile = gm_fnmDirectory + gm_fnmBaseName + strNumber + gm_fnmExt;
    pfi->fi_strName = EMPTYSLOTSTRING;
    // add it to beginning
    gm_lhFileInfos.AddHead(pfi->fi_lnNode);
  }

  // set default parameters for the list
  gm_iListOffset = 0;
  gm_ctListTotal = gm_lhFileInfos.Count();

  // find which one should be selected
  gm_iListWantedItem = 0;
  if (gm_fnmSelected != "") {
    INDEX i = 0;
    FOREACHINLIST(CFileInfo, fi_lnNode, gm_lhFileInfos, itfi) {
      CFileInfo &fi = *itfi;
      if (fi.fi_fnFile == gm_fnmSelected) {
        gm_iListWantedItem = i;
        break;
      }
      i++;
    }
  }

  CGameMenu::StartMenu();
}

void CLoadSaveMenu::EndMenu(void)
{
  // delete all file infos
  FORDELETELIST(CFileInfo, fi_lnNode, gm_lhFileInfos, itfi) {
    delete &itfi.Current();
  }
  gm_pgmNextMenu = NULL;
  CGameMenu::EndMenu();
}

void CLoadSaveMenu::FillListItems(void)
{
  // disable all items first
  for (INDEX i = 0; i<SAVELOAD_BUTTONS_CT; i++) {
    gm_pButtons[i]->mg_bEnabled = FALSE;
    gm_pButtons[i]->mg_strText = TRANS("<empty>");
    gm_pButtons[i]->SetTooltip("");
    gm_pButtons[i]->mg_iInList = -2;
  }

  BOOL bHasFirst = FALSE;
  BOOL bHasLast = FALSE;
  INDEX ctLabels = gm_lhFileInfos.Count();
  INDEX iLabel = 0;
  FOREACHINLIST(CFileInfo, fi_lnNode, gm_lhFileInfos, itfi) {
    CFileInfo &fi = *itfi;
    INDEX iInMenu = iLabel - gm_iListOffset;
    if ((iLabel >= gm_iListOffset) &&
      (iLabel<(gm_iListOffset + SAVELOAD_BUTTONS_CT)))
    {
      bHasFirst |= (iLabel == 0);
      bHasLast |= (iLabel == ctLabels - 1);
      gm_pButtons[iInMenu]->mg_iInList = iLabel;
      gm_pButtons[iInMenu]->mg_strDes = fi.fi_strName;
      gm_pButtons[iInMenu]->mg_fnm = fi.fi_fnFile;
      gm_pButtons[iInMenu]->mg_bEnabled = TRUE;
      gm_pButtons[iInMenu]->RefreshText();
      if (gm_bSave) {
        if (!FileExistsForWriting(gm_pButtons[iInMenu]->mg_fnm)) {
          gm_pButtons[iInMenu]->SetTooltip(TRANS("Enter - save in new slot"));
        }
        else {
          gm_pButtons[iInMenu]->SetTooltip(TRANS("Enter - save here, F2 - rename, Del - delete"));
        }
      }
      else if (gm_bManage) {
        gm_pButtons[iInMenu]->SetTooltip(TRANS("Enter - load this, F2 - rename, Del - delete"));
      }
      else {
        gm_pButtons[iInMenu]->SetTooltip(TRANS("Enter - load this"));
      }
    }
    iLabel++;
  }

  // enable/disable up/down arrows
  gm_pArrowUp->mg_bEnabled = !bHasFirst && ctLabels>0;
  gm_pArrowDn->mg_bEnabled = !bHasLast  && ctLabels>0;
}

// called to get info of a file from directory, or to skip it
BOOL CLoadSaveMenu::ParseFile(const CTFileName &fnm, CTString &strName)
{
  if (fnm.FileExt() != gm_fnmExt) {
    return FALSE;
  }

  CTFileName fnSaveGameDescription = fnm.NoExt() + ".des";
  try {
    strName.Load_t(fnSaveGameDescription);
  } catch (char *strError) {
    (void)strError;
    strName = fnm.FileName();

    if (fnm.FileExt() == ".ctl") {
      INDEX iCtl = -1;
      strName.ScanF("Controls%d", &iCtl);
      if (iCtl >= 0 && iCtl <= 7) {
        const CPlayerCharacter &plc = _pGame->GetPlayers()[iCtl];
        strName.PrintF(TRANS("From player: %s"), plc.GetNameForPrinting());
      }
    }
  }

  INDEX iFile = -1;
  fnm.FileName().ScanF((const char*)(gm_fnmBaseName + "%d"), &iFile);

  gm_iLastFile = Max(gm_iLastFile, iFile);

  return TRUE;
}