/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "Interface/Common/MenuStuff.h"
#include "NetworkOpen.h"

CNetworkOpenMenu::CNetworkOpenMenu()
{
  gm_pTitle = new CTitleWidget();
  gm_pAddressLabel = new CButtonWidget();
  gm_pAddress = new CEditWidget();
  gm_pPortLabel = new CButtonWidget();
  gm_pPort = new CEditWidget();
  gm_pJoin = new CButtonWidget();
}

void CNetworkOpenMenu::Initialize_t(void)
{
  // intialize network join menu
  gm_pTitle->mg_boxOnScreen = BoxTitle();
  gm_pTitle->SetText(TRANS("JOIN"));
  AddChild(gm_pTitle);

  // Address Label
  gm_pAddressLabel->mg_strText = TRANS("Address:");
  gm_pAddressLabel->mg_boxOnScreen = BoxMediumLeft(1);
  gm_pAddressLabel->mg_iCenterI = -1;
  AddChild(gm_pAddressLabel);

  // Address Edit
  gm_pAddress->mg_strText = _pGame->GetJoinAddress();
  gm_pAddress->mg_ctMaxStringLen = 20;
  gm_pAddress->mg_pstrToChange = &_pGame->gam_strJoinAddress;
  gm_pAddress->mg_boxOnScreen = BoxMediumMiddle(1);
  gm_pAddress->mg_bfsFontSize = BFS_MEDIUM;
  gm_pAddress->mg_iCenterI = -1;
  gm_pAddress->mg_pmgUp = gm_pJoin;
  gm_pAddress->mg_pmgDown = gm_pPort;
  gm_pAddress->SetTooltip(TRANS("specify server address"));
  AddChild(gm_pAddress);

  // Port
  gm_pPortLabel->mg_strText = TRANS("Port:");
  gm_pPortLabel->mg_boxOnScreen = BoxMediumLeft(2);
  gm_pPortLabel->mg_iCenterI = -1;
  AddChild(gm_pPortLabel);

  // Port Edit
  gm_pPort->mg_strText = "";
  gm_pPort->mg_ctMaxStringLen = 10;
  gm_pPort->mg_pstrToChange = &gm_strPort;
  gm_pPort->mg_boxOnScreen = BoxMediumMiddle(2);
  gm_pPort->mg_bfsFontSize = BFS_MEDIUM;
  gm_pPort->mg_iCenterI = -1;
  gm_pPort->mg_pmgUp = gm_pAddress;
  gm_pPort->mg_pmgDown = gm_pJoin;
  gm_pPort->SetTooltip(TRANS("specify server address"));
  AddChild(gm_pPort);

  // Join
  gm_pJoin->SetText(TRANS("Join"));
  gm_pJoin->mg_boxOnScreen = BoxMediumMiddle(3);
  gm_pJoin->mg_pmgUp = gm_pPort;
  gm_pJoin->mg_pmgDown = gm_pAddress;
  AddChild(gm_pJoin);
  gm_pJoin->mg_pActivatedFunction = NULL;
}

void CNetworkOpenMenu::StartMenu(void)
{
  gm_strPort = _pShell->GetValue("net_iPort");
  gm_pPort->mg_strText = gm_strPort;
}

void CNetworkOpenMenu::EndMenu(void)
{
  _pShell->SetValue("net_iPort", gm_strPort);
}