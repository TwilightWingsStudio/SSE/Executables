/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "Interface/Common/MenuStuff.h"
#include "CustomizeKeyboard.h"

CCustomizeKeyboardMenu::CCustomizeKeyboardMenu()
{
  gm_pTitle = new CTitleWidget();
  gm_pArrowUp = new CArrowWidget();
  gm_pArrowDn = new CArrowWidget();
  
  for (INDEX iKey = 0; iKey < KEYS_ON_SCREEN; iKey++)
  {
    gm_pKey[iKey] = new CKeyDefinitionWidget();
  }
}

void CCustomizeKeyboardMenu::FillListItems(void)
{
  // disable all items first
  for (INDEX i = 0; i<KEYS_ON_SCREEN; i++) {
    gm_pKey[i]->mg_bEnabled = FALSE;
    gm_pKey[i]->mg_iInList = -2;
  }

  BOOL bHasFirst = FALSE;
  BOOL bHasLast = FALSE;

  // set diks to key buttons
  CControls &ctrls = _pGame->GetControlsExtra();
  INDEX iLabel = 0;
  INDEX ctLabels = ctrls.ctrl_lhButtonActions.Count();

  FOREACHINLIST(CButtonAction, ba_lnNode, ctrls.ctrl_lhButtonActions, itAct)
  {
    INDEX iInMenu = iLabel - gm_iListOffset;
    if ((iLabel >= gm_iListOffset) &&
      (iLabel<(gm_iListOffset + gm_ctListVisible)))
    {
      bHasFirst |= (iLabel == 0);
      bHasLast |= (iLabel == ctLabels - 1);
      gm_pKey[iInMenu]->mg_strLabel = TranslateConst(itAct->ba_strName, 0);
      gm_pKey[iInMenu]->mg_iControlNumber = iLabel;
      gm_pKey[iInMenu]->SetBindingNames(FALSE);
      gm_pKey[iInMenu]->SetTooltip(TRANS("Enter - change binding, Backspace - unbind"));
      gm_pKey[iInMenu]->mg_bEnabled = TRUE;
      gm_pKey[iInMenu]->mg_iInList = iLabel;
    }

    iLabel++;
  }

  // enable/disable up/down arrows
  gm_pArrowUp->mg_bEnabled = !bHasFirst && ctLabels>0;
  gm_pArrowDn->mg_bEnabled = !bHasLast  && ctLabels>0;
}

void CCustomizeKeyboardMenu::Initialize_t(void)
{
  // intialize Audio options menu
  gm_pTitle->SetText(TRANS("CUSTOMIZE BUTTONS"));
  gm_pTitle->mg_boxOnScreen = BoxTitle();
  AddChild(gm_pTitle);

#define KL_START 3.0f
#define KL_STEEP -1.45f
  for (INDEX iLabel = 0; iLabel<KEYS_ON_SCREEN; iLabel++)
  {
    INDEX iPrev = (gm_ctListVisible + iLabel - 1) % KEYS_ON_SCREEN;
    INDEX iNext = (iLabel + 1) % KEYS_ON_SCREEN;
    // initialize label entities
    gm_pKey[iLabel]->mg_boxOnScreen = BoxKeyRow(iLabel);
    // initialize label gadgets
    gm_pKey[iLabel]->mg_pmgUp = gm_pKey[iPrev];
    gm_pKey[iLabel]->mg_pmgDown = gm_pKey[iNext];
    gm_pKey[iLabel]->mg_bVisible = TRUE;
    AddChild(gm_pKey[iLabel]);
  }
  // arrows just exist
  AddChild(gm_pArrowDn);
  AddChild(gm_pArrowUp);
  gm_pArrowDn->mg_adDirection = AD_DOWN;
  gm_pArrowUp->mg_adDirection = AD_UP;
  gm_pArrowDn->mg_boxOnScreen = BoxArrow(AD_DOWN);
  gm_pArrowUp->mg_boxOnScreen = BoxArrow(AD_UP);
  gm_pArrowDn->mg_pmgRight = gm_pArrowDn->mg_pmgUp =
    gm_pKey[KEYS_ON_SCREEN - 1];
  gm_pArrowUp->mg_pmgRight = gm_pArrowUp->mg_pmgDown =
    gm_pKey[0];

  gm_ctListVisible = KEYS_ON_SCREEN;
  gm_pmgArrowUp = gm_pArrowUp;
  gm_pmgArrowDn = gm_pArrowDn;
  gm_pmgListTop = gm_pKey[0];
  gm_pmgListBottom = gm_pKey[KEYS_ON_SCREEN - 1];
}

void CCustomizeKeyboardMenu::StartMenu(void)
{
  ControlsMenuOn();
  gm_iListOffset = 0;
  gm_ctListTotal = _pGame->GetControlsExtra().ctrl_lhButtonActions.Count();
  gm_iListWantedItem = 0;
  CGameMenu::StartMenu();
}

void CCustomizeKeyboardMenu::EndMenu(void)
{
  ControlsMenuOff();
  CGameMenu::EndMenu();
}