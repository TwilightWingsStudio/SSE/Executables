/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_GAME_MENU_NETWORKSTART_H
#define SE_INCL_GAME_MENU_NETWORKSTART_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include "GameMenu.h"
#include "Interface/Widgets/Button.h"
#include "Interface/Widgets/Edit.h"
#include "Interface/Widgets/Title.h"
#include "Interface/Widgets/Trigger.h"

class CNetworkStartMenu : public CGameMenu
{
  public:
    CTitleWidget *gm_pTitle;
    CEditWidget *gm_pSessionName;
    CTriggerWidget *gm_pGameType;
    CTriggerWidget *gm_pDifficulty;
    CButtonWidget *gm_pLevel;
    CTriggerWidget *gm_pMaxPlayers;
    CTriggerWidget *gm_pWaitAllPlayers;
    CTriggerWidget *gm_pCustomizeGame;
    CTriggerWidget *gm_pVisible;
    CButtonWidget *gm_pGameOptions;
    CButtonWidget *gm_pStart;

  public:
    //! Constructor.
    CNetworkStartMenu();
  
    void Initialize_t(void);
    void StartMenu(void);
    void EndMenu(void);
};

#endif  /* include-once check. */