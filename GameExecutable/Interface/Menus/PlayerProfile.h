/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_GAME_MENU_PLAYERPROFILE_H
#define SE_INCL_GAME_MENU_PLAYERPROFILE_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include "GameMenu.h"
#include "Interface/Widgets/Button.h"
#include "Interface/Widgets/Edit.h"
#include "Interface/Widgets/Model.h"
#include "Interface/Widgets/Title.h"
#include "Interface/Widgets/Trigger.h"

#define MAX_PLAYER_PROFILES 8

class CPlayerProfileMenu : public CGameMenu
{
  public:
    INDEX *gm_piCurrentPlayer;

    CTitleWidget *gm_pProfileTitle;
    CButtonWidget *gm_pNoLabel;
    CButtonWidget *gm_pNumber[MAX_PLAYER_PROFILES];
    CButtonWidget *gm_pNameLabel;
    CEditWidget *gm_pNameField;
    CButtonWidget *gm_pTeamLabel;
    CEditWidget *gm_pTeam;
    CButtonWidget *gm_pCustomizeControls;
    CTriggerWidget *gm_pCrosshair;
    CTriggerWidget *gm_pWeaponSelect;
    CTriggerWidget *gm_pWeaponHide;
    CTriggerWidget *gm_p3rdPerson;
    CTriggerWidget *gm_pQuotes;
    CTriggerWidget *gm_pAutoSave;
    CTriggerWidget *gm_pCompDoubleClick;
    CTriggerWidget *gm_pViewBobbing;
    CTriggerWidget *gm_pSharpTurning;
    CModelWidget *gm_pModel;

  public:
    //! Constructor.
    CPlayerProfileMenu();
  
    void Initialize_t(void);
    INDEX ComboFromPlayer(INDEX iPlayer);
    INDEX PlayerFromCombo(INDEX iCombo);
    void SelectPlayer(INDEX iPlayer);
    void ApplyComboPlayer(INDEX iPlayer);
    void StartMenu(void);
    void EndMenu(void);
};

#endif  /* include-once check. */