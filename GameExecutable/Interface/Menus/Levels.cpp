/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "LevelInfo.h"
#include "Levels.h"

CLevelsMenu::CLevelsMenu()
{
  gm_pTitle = new CTitleWidget();
  gm_pArrowUp = new CArrowWidget();
  gm_pArrowDn = new CArrowWidget();
  
  for (INDEX iLevel = 0; iLevel < LEVELS_ON_SCREEN; iLevel++)
  {
    gm_pManualLevel[iLevel] = new CLevelButtonWidget();
  }
}

void CLevelsMenu::Initialize_t(void)
{
  gm_pTitle->mg_boxOnScreen = BoxTitle();
  gm_pTitle->SetText(TRANS("CHOOSE LEVEL"));
  AddChild(gm_pTitle);

  for (INDEX iLabel = 0; iLabel<LEVELS_ON_SCREEN; iLabel++)
  {
    INDEX iPrev = (LEVELS_ON_SCREEN + iLabel - 1) % LEVELS_ON_SCREEN;
    INDEX iNext = (iLabel + 1) % LEVELS_ON_SCREEN;
    // initialize label gadgets
    gm_pManualLevel[iLabel]->mg_pmgUp = gm_pManualLevel[iPrev];
    gm_pManualLevel[iLabel]->mg_pmgDown = gm_pManualLevel[iNext];
    gm_pManualLevel[iLabel]->mg_boxOnScreen = BoxMediumRow(iLabel);
    gm_pManualLevel[iLabel]->mg_pActivatedFunction = NULL; // never called!
    AddChild(gm_pManualLevel[iLabel]);
  }

  AddChild(gm_pArrowUp);
  AddChild(gm_pArrowDn);
  gm_pArrowUp->mg_adDirection = AD_UP;
  gm_pArrowDn->mg_adDirection = AD_DOWN;
  gm_pArrowUp->mg_boxOnScreen = BoxArrow(AD_UP);
  gm_pArrowDn->mg_boxOnScreen = BoxArrow(AD_DOWN);
  gm_pArrowUp->mg_pmgRight = gm_pArrowUp->mg_pmgDown =
    gm_pManualLevel[0];
  gm_pArrowDn->mg_pmgRight = gm_pArrowDn->mg_pmgUp =
    gm_pManualLevel[LEVELS_ON_SCREEN - 1];

  gm_ctListVisible = LEVELS_ON_SCREEN;
  gm_pmgArrowUp = gm_pArrowUp;
  gm_pmgArrowDn = gm_pArrowDn;
  gm_pmgListTop = gm_pManualLevel[0];
  gm_pmgListBottom = gm_pManualLevel[LEVELS_ON_SCREEN - 1];
}

void CLevelsMenu::FillListItems(void)
{
  // disable all items first
  for (INDEX i = 0; i<LEVELS_ON_SCREEN; i++) {
    gm_pManualLevel[i]->mg_bEnabled = FALSE;
    gm_pManualLevel[i]->mg_strText = TRANS("<empty>");
    gm_pManualLevel[i]->mg_iInList = -2;
  }

  BOOL bHasFirst = FALSE;
  BOOL bHasLast = FALSE;
  INDEX ctLabels = _lhFilteredLevels.Count();
  INDEX iLabel = 0;
  FOREACHINLIST(CLevelInfo, li_lnNode, _lhFilteredLevels, itli) {
    CLevelInfo &li = *itli;
    INDEX iInMenu = iLabel - gm_iListOffset;
    if ((iLabel >= gm_iListOffset) &&
      (iLabel<(gm_iListOffset + LEVELS_ON_SCREEN)))
    {
      bHasFirst |= (iLabel == 0);
      bHasLast |= (iLabel == ctLabels - 1);
      gm_pManualLevel[iInMenu]->mg_strText = li.li_strName;
      gm_pManualLevel[iInMenu]->mg_fnmLevel = li.li_fnLevel;
      gm_pManualLevel[iInMenu]->mg_bEnabled = TRUE;
      gm_pManualLevel[iInMenu]->mg_iInList = iLabel;
      
      CTString strTip;
      
      if (li.li_eFormat != CLevelInfo::LF_SE100) {
        CTString strFormat;

        if (li.li_eFormat == CLevelInfo::LF_SE150) {
          strFormat = "1.50";
        } else {
          strFormat = "SSR";
        }
        
        strTip.PrintF(TRANS("This is a level in the %s format, it cannot be played!"), strFormat);
      }
      
      gm_pManualLevel[iInMenu]->SetTooltip(strTip);
    }
    iLabel++;
  }

  // enable/disable up/down arrows
  gm_pArrowUp->mg_bEnabled = !bHasFirst && ctLabels>0;
  gm_pArrowDn->mg_bEnabled = !bHasLast  && ctLabels>0;
}

void CLevelsMenu::StartMenu(void)
{
  // set default parameters for the list
  gm_iListOffset = 0;
  gm_ctListTotal = _lhFilteredLevels.Count();
  gm_iListWantedItem = 0;
  // for each level
  INDEX i = 0;
  FOREACHINLIST(CLevelInfo, li_lnNode, _lhFilteredLevels, itlid) {
    CLevelInfo &lid = *itlid;
    // if it is the chosen one
    if (lid.li_fnLevel == _pGame->GetCustomLevel()) {
      // demand focus on it
      gm_iListWantedItem = i;
      break;
    }
    i++;
  }
  CGameMenu::StartMenu();
}