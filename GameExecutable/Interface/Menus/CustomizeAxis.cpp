/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "Interface/Common/MenuStuff.h"
#include "CustomizeAxis.h"

CCustomizeAxisMenu::CCustomizeAxisMenu()
{
  gm_pTitle = new CTitleWidget();
  gm_pActionTrigger = new CTriggerWidget();
  gm_pMountedTrigger = new CTriggerWidget();
  gm_pSensitivity = new CSliderWidget();
  gm_pDeadzone = new CSliderWidget();
  gm_pInvertTrigger = new CTriggerWidget();
  gm_pRelativeTrigger = new CTriggerWidget();
  gm_pSmoothTrigger = new CTriggerWidget();
}

void CCustomizeAxisMenu::Initialize_t(void)
{
  // intialize axis menu
  gm_pTitle->SetText(TRANS("CUSTOMIZE AXIS"));
  gm_pTitle->mg_boxOnScreen = BoxTitle();
  AddChild(gm_pTitle);
  
  // Action
  gm_pActionTrigger->mg_strLabel = TRANS("ACTION");
  gm_pActionTrigger->mg_pmgUp = gm_pSmoothTrigger;
  gm_pActionTrigger->mg_pmgDown = gm_pMountedTrigger;
  gm_pActionTrigger->mg_boxOnScreen = BoxMediumRow(0);
  gm_pActionTrigger->mg_astrTexts = astrNoYes;
  gm_pActionTrigger->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pActionTrigger->mg_iSelected = 0;
  gm_pActionTrigger->mg_strValue = astrNoYes[0];
  AddChild(gm_pActionTrigger);
  gm_pActionTrigger->SetTooltip(TRANS("choose action to customize"));
  
  // Mounted To
  gm_pMountedTrigger->mg_strLabel = TRANS("MOUNTED TO");
  gm_pMountedTrigger->mg_pmgUp = gm_pActionTrigger;
  gm_pMountedTrigger->mg_pmgDown = gm_pSensitivity;
  gm_pMountedTrigger->mg_boxOnScreen = BoxMediumRow(2);
  gm_pMountedTrigger->mg_astrTexts = astrNoYes;
  gm_pMountedTrigger->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pMountedTrigger->mg_iSelected = 0;
  gm_pMountedTrigger->mg_strValue = astrNoYes[0];
  AddChild(gm_pMountedTrigger);
  gm_pMountedTrigger->SetTooltip(TRANS("choose controller axis that will perform the action"));

  gm_pActionTrigger->mg_astrTexts = new CTString[AXIS_ACTIONS_CT];
  gm_pActionTrigger->mg_ctTexts = AXIS_ACTIONS_CT;

  gm_pActionTrigger->mg_pPreTriggerChange = NULL;
  gm_pActionTrigger->mg_pOnTriggerChange = NULL;

  // for all available axis type controlers
  for (INDEX iControler = 0; iControler<AXIS_ACTIONS_CT; iControler++) {
    gm_pActionTrigger->mg_astrTexts[iControler] = TranslateConst(CTString(_pGame->gm_astrAxisNames[iControler]), 0);
  }
  gm_pActionTrigger->mg_iSelected = 3;

  INDEX ctAxis = _pInput->GetAvailableAxisCount();
  gm_pMountedTrigger->mg_astrTexts = new CTString[ctAxis];
  gm_pMountedTrigger->mg_ctTexts = ctAxis;
  // for all axis actions that can be mounted
  for (INDEX iAxis = 0; iAxis<ctAxis; iAxis++) {
    gm_pMountedTrigger->mg_astrTexts[iAxis] = _pInput->GetAxisTransName(iAxis);
  }

  // Sensitivity
  gm_pSensitivity->mg_strText = TRANS("SENSITIVITY");
  gm_pSensitivity->mg_boxOnScreen = BoxMediumRow(3);
  gm_pSensitivity->mg_pmgUp = gm_pMountedTrigger;
  gm_pSensitivity->mg_pmgDown = gm_pDeadzone;
  AddChild(gm_pSensitivity);
  gm_pSensitivity->SetTooltip(TRANS("set sensitivity for this axis"));

  // Dead Zone
  gm_pDeadzone->mg_strText = TRANS("DEAD ZONE");
  gm_pDeadzone->mg_boxOnScreen = BoxMediumRow(4);
  gm_pDeadzone->mg_pmgUp = gm_pSensitivity;
  gm_pDeadzone->mg_pmgDown = gm_pInvertTrigger;
  AddChild(gm_pDeadzone);
  gm_pDeadzone->SetTooltip(TRANS("set dead zone for this axis"));
  
  // Inverted
  gm_pInvertTrigger->mg_strLabel = TRANS("INVERTED");
  gm_pInvertTrigger->mg_pmgUp = gm_pDeadzone;
  gm_pInvertTrigger->mg_pmgDown = gm_pRelativeTrigger;
  gm_pInvertTrigger->mg_boxOnScreen = BoxMediumRow(5);
  gm_pInvertTrigger->mg_astrTexts = astrNoYes;
  gm_pInvertTrigger->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pInvertTrigger->mg_iSelected = 0;
  gm_pInvertTrigger->mg_strValue = astrNoYes[0];
  AddChild(gm_pInvertTrigger);
  gm_pInvertTrigger->SetTooltip(TRANS("choose whether to invert this axis or not"));
  
  // Relative
  gm_pRelativeTrigger->mg_strLabel = TRANS("RELATIVE");
  gm_pRelativeTrigger->mg_pmgUp = gm_pInvertTrigger;
  gm_pRelativeTrigger->mg_pmgDown = gm_pSmoothTrigger;
  gm_pRelativeTrigger->mg_boxOnScreen = BoxMediumRow(6);
  gm_pRelativeTrigger->mg_astrTexts = astrNoYes;
  gm_pRelativeTrigger->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pRelativeTrigger->mg_iSelected = 0;
  gm_pRelativeTrigger->mg_strValue = astrNoYes[0];
  AddChild(gm_pRelativeTrigger);
  gm_pRelativeTrigger->SetTooltip(TRANS("select relative or absolute axis reading"));
  
  // Smooth
  gm_pSmoothTrigger->mg_strLabel = TRANS("SMOOTH");
  gm_pSmoothTrigger->mg_pmgUp = gm_pRelativeTrigger;
  gm_pSmoothTrigger->mg_pmgDown = gm_pActionTrigger;
  gm_pSmoothTrigger->mg_boxOnScreen = BoxMediumRow(7);
  gm_pSmoothTrigger->mg_astrTexts = astrNoYes;
  gm_pSmoothTrigger->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pSmoothTrigger->mg_iSelected = 0;
  gm_pSmoothTrigger->mg_strValue = astrNoYes[0];
  AddChild(gm_pSmoothTrigger);
  gm_pSmoothTrigger->SetTooltip(TRANS("turn this on to filter readings on this axis"));
}

CCustomizeAxisMenu::~CCustomizeAxisMenu(void)
{
  delete[] gm_pActionTrigger->mg_astrTexts;
  delete[] gm_pMountedTrigger->mg_astrTexts;
}

void CCustomizeAxisMenu::ObtainActionSettings(void)
{
  ControlsMenuOn();
  CControls &ctrls = _pGame->GetControlsExtra();
  INDEX iSelectedAction = gm_pActionTrigger->mg_iSelected;
  INDEX iMountedAxis = ctrls.ctrl_aaAxisActions[iSelectedAction].aa_iAxisAction;

  gm_pMountedTrigger->mg_iSelected = iMountedAxis;

  gm_pSensitivity->mg_iMinPos = 0;
  gm_pSensitivity->mg_iMaxPos = 50;
  gm_pSensitivity->mg_iCurPos = ctrls.ctrl_aaAxisActions[iSelectedAction].aa_fSensitivity / 2;
  gm_pSensitivity->ApplyCurrentPosition();

  gm_pDeadzone->mg_iMinPos = 0;
  gm_pDeadzone->mg_iMaxPos = 50;
  gm_pDeadzone->mg_iCurPos = ctrls.ctrl_aaAxisActions[iSelectedAction].aa_fDeadZone / 2;
  gm_pDeadzone->ApplyCurrentPosition();

  gm_pInvertTrigger->mg_iSelected = ctrls.ctrl_aaAxisActions[iSelectedAction].aa_bInvert ? 1 : 0;
  gm_pRelativeTrigger->mg_iSelected = ctrls.ctrl_aaAxisActions[iSelectedAction].aa_bRelativeControler ? 1 : 0;
  gm_pSmoothTrigger->mg_iSelected = ctrls.ctrl_aaAxisActions[iSelectedAction].aa_bSmooth ? 1 : 0;

  gm_pActionTrigger->ApplyCurrentSelection();
  gm_pMountedTrigger->ApplyCurrentSelection();
  gm_pInvertTrigger->ApplyCurrentSelection();
  gm_pRelativeTrigger->ApplyCurrentSelection();
  gm_pSmoothTrigger->ApplyCurrentSelection();
}

void CCustomizeAxisMenu::ApplyActionSettings(void)
{
  CControls &ctrls = _pGame->GetControlsExtra();
  INDEX iSelectedAction = gm_pActionTrigger->mg_iSelected;
  INDEX iMountedAxis = gm_pMountedTrigger->mg_iSelected;
  FLOAT fSensitivity =
    FLOAT(gm_pSensitivity->mg_iCurPos - gm_pSensitivity->mg_iMinPos) /
    FLOAT(gm_pSensitivity->mg_iMaxPos - gm_pSensitivity->mg_iMinPos)*100.0f;
  FLOAT fDeadZone =
    FLOAT(gm_pDeadzone->mg_iCurPos - gm_pDeadzone->mg_iMinPos) /
    FLOAT(gm_pDeadzone->mg_iMaxPos - gm_pDeadzone->mg_iMinPos)*100.0f;

  BOOL bInvert = gm_pInvertTrigger->mg_iSelected != 0;
  BOOL bRelative = gm_pRelativeTrigger->mg_iSelected != 0;
  BOOL bSmooth = gm_pSmoothTrigger->mg_iSelected != 0;

  ctrls.ctrl_aaAxisActions[iSelectedAction].aa_iAxisAction = iMountedAxis;
  if (INDEX(ctrls.ctrl_aaAxisActions[iSelectedAction].aa_fSensitivity) != INDEX(fSensitivity)) {
    ctrls.ctrl_aaAxisActions[iSelectedAction].aa_fSensitivity = fSensitivity;
  }
  if (INDEX(ctrls.ctrl_aaAxisActions[iSelectedAction].aa_fDeadZone) != INDEX(fDeadZone)) {
    ctrls.ctrl_aaAxisActions[iSelectedAction].aa_fDeadZone = fDeadZone;
  }
  ctrls.ctrl_aaAxisActions[iSelectedAction].aa_bInvert = bInvert;
  ctrls.ctrl_aaAxisActions[iSelectedAction].aa_bRelativeControler = bRelative;
  ctrls.ctrl_aaAxisActions[iSelectedAction].aa_bSmooth = bSmooth;
  ctrls.CalculateInfluencesForAllAxis();

  ControlsMenuOff();
}

void CCustomizeAxisMenu::StartMenu(void)
{
  ObtainActionSettings();

  CGameMenu::StartMenu();
}

void CCustomizeAxisMenu::EndMenu(void)
{
  ApplyActionSettings();
  CGameMenu::EndMenu();
}