/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "Interface/Common/MenuStuff.h"
#include "SelectPlayers.h"

#define ADD_GADGET( gd, box, up, dn, lf, rt, txt) \
  gd.mg_boxOnScreen = box; \
  gd.mg_pmgUp = up; \
  gd.mg_pmgDown = dn; \
  gd.mg_pmgLeft = lf; \
  gd.mg_pmgRight = rt; \
  gd.mg_strText = txt; \
  AddChild(gd.mg_lnNode);

extern CTString astrNoYes[2];
extern CTString astrSplitScreenRadioTexts[4];
extern void SelectPlayersFillMenu(void);
extern void SelectPlayersApplyMenu(void);

CSelectPlayersMenu::CSelectPlayersMenu()
{
  gm_pTitle = new CTitleWidget();
  gm_pDedicated = new CTriggerWidget();
  gm_pObserver = new CTriggerWidget();
  gm_pSplitScreenCfg = new CTriggerWidget();
  gm_pPlayer0Change = new CChangePlayerWidget();
  gm_pPlayer1Change = new CChangePlayerWidget();
  gm_pPlayer2Change = new CChangePlayerWidget();
  gm_pPlayer3Change = new CChangePlayerWidget();
  gm_pNotes = new CButtonWidget();
  gm_pStart = new CButtonWidget();
}

void CSelectPlayersMenu::Initialize_t(void)
{
  // intialize split screen menu
  gm_pTitle->mg_boxOnScreen = BoxTitle();
  gm_pTitle->SetText(TRANS("SELECT PLAYERS"));
  AddChild(gm_pTitle);

  // Dedicated?
  gm_pDedicated->mg_strLabel = TRANS("Dedicated:");
  gm_pDedicated->mg_pmgUp = gm_pStart;
  gm_pDedicated->mg_pmgDown = gm_pObserver;
  gm_pDedicated->mg_boxOnScreen = BoxMediumRow(0);
  gm_pDedicated->mg_astrTexts = astrNoYes;
  gm_pDedicated->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pDedicated->mg_iSelected = 0;
  gm_pDedicated->mg_strValue = astrNoYes[0];
  AddChild(gm_pDedicated);
  gm_pDedicated->SetTooltip(TRANS("select to start dedicated server"));
  gm_pDedicated->mg_pOnTriggerChange = NULL;

  // Observer?
  gm_pObserver->mg_strLabel = TRANS("Observer:");
  gm_pObserver->mg_pmgUp = gm_pDedicated;
  gm_pObserver->mg_pmgDown = gm_pSplitScreenCfg;
  gm_pObserver->mg_boxOnScreen = BoxMediumRow(1);
  gm_pObserver->mg_astrTexts = astrNoYes;
  gm_pObserver->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pObserver->mg_iSelected = 0;
  gm_pObserver->mg_strValue = astrNoYes[0];
  AddChild(gm_pObserver);
  gm_pObserver->SetTooltip(TRANS("select to join in for observing, not for playing"));
  gm_pObserver->mg_pOnTriggerChange = NULL;

  // split screen config trigger
  gm_pSplitScreenCfg->mg_strLabel = TRANS("Number of players:");
  gm_pSplitScreenCfg->mg_pmgUp = gm_pObserver;
  gm_pSplitScreenCfg->mg_pmgDown = gm_pPlayer0Change;
  gm_pSplitScreenCfg->mg_boxOnScreen = BoxMediumRow(2);
  gm_pSplitScreenCfg->mg_astrTexts = astrSplitScreenRadioTexts;
  gm_pSplitScreenCfg->mg_ctTexts = sizeof(astrSplitScreenRadioTexts) / sizeof(astrSplitScreenRadioTexts[0]);
  gm_pSplitScreenCfg->mg_iSelected = 0;
  gm_pSplitScreenCfg->mg_strValue = astrSplitScreenRadioTexts[0];
  AddChild(gm_pSplitScreenCfg);
  gm_pSplitScreenCfg->SetTooltip(TRANS("choose more than one player to play in split screen"));
  gm_pSplitScreenCfg->mg_pOnTriggerChange = NULL;

  gm_pPlayer0Change->mg_iCenterI = -1;
  gm_pPlayer1Change->mg_iCenterI = -1;
  gm_pPlayer2Change->mg_iCenterI = -1;
  gm_pPlayer3Change->mg_iCenterI = -1;
  gm_pPlayer0Change->mg_boxOnScreen = BoxMediumMiddle(4);
  gm_pPlayer1Change->mg_boxOnScreen = BoxMediumMiddle(5);
  gm_pPlayer2Change->mg_boxOnScreen = BoxMediumMiddle(6);
  gm_pPlayer3Change->mg_boxOnScreen = BoxMediumMiddle(7);
  gm_pPlayer0Change->SetTooltip(TRANS("select profile for this player"));
  gm_pPlayer1Change->SetTooltip(TRANS("select profile for this player"));
  gm_pPlayer2Change->SetTooltip(TRANS("select profile for this player"));
  gm_pPlayer3Change->SetTooltip(TRANS("select profile for this player"));
  AddChild(gm_pPlayer0Change);
  AddChild(gm_pPlayer1Change);
  AddChild(gm_pPlayer2Change);
  AddChild(gm_pPlayer3Change);

  gm_pNotes->mg_boxOnScreen = BoxMediumRow(9.0);
  gm_pNotes->mg_bfsFontSize = BFS_MEDIUM;
  gm_pNotes->mg_iCenterI = -1;
  gm_pNotes->mg_bEnabled = FALSE;
  gm_pNotes->mg_bLabel = TRUE;
  AddChild(gm_pNotes);
  gm_pNotes->mg_strText = "";

  /*  // options button
  mgSplitOptions.mg_strText = TRANS("Game options");
  mgSplitOptions.mg_boxOnScreen = BoxMediumRow(3);
  mgSplitOptions.mg_bfsFontSize = BFS_MEDIUM;
  mgSplitOptions.mg_iCenterI = 0;
  mgSplitOptions.mg_pmgUp = &mgSplitLevel;
  mgSplitOptions.mg_pmgDown = &mgSplitStartStart;
  mgSplitOptions.SetTooltip(TRANS("adjust game rules"));
  mgSplitOptions.mg_pActivatedFunction = &StartGameOptionsFromSplitScreen;
  AddChild( mgSplitOptions.mg_lnNode);*/

  /*  // start button
  mgSplitStartStart.mg_bfsFontSize = BFS_LARGE;
  mgSplitStartStart.mg_boxOnScreen = BoxBigRow(4);
  mgSplitStartStart.mg_pmgUp = &mgSplitOptions;
  mgSplitStartStart.mg_pmgDown = &mgSplitGameType;
  mgSplitStartStart.mg_strText = TRANS("START");
  AddChild( mgSplitStartStart.mg_lnNode);
  mgSplitStartStart.mg_pActivatedFunction = &CMenuStarters::SelectPlayersFromSplit;
  */

  // start button
  gm_pStart->SetText(TRANS("START"));
  gm_pStart->mg_bfsFontSize = BFS_LARGE;
  gm_pStart->mg_boxOnScreen = BoxBigRow(4);
  gm_pStart->mg_pmgUp = gm_pPlayer3Change;
  gm_pStart->mg_pmgDown = gm_pDedicated;
  gm_pStart->mg_pActivatedFunction = NULL;
  gm_pStart->mg_iCenterI = 0;
  AddChild(gm_pStart);
}

void CSelectPlayersMenu::StartMenu(void)
{
  CGameMenu::StartMenu();
  SelectPlayersFillMenu();
  SelectPlayersApplyMenu();
}

void CSelectPlayersMenu::EndMenu(void)
{
  SelectPlayersApplyMenu();
  CGameMenu::EndMenu();
}