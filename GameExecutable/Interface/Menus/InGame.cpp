/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "InGame.h"

CInGameMenu::CInGameMenu()
{
  gm_mgTitle = new CTitleWidget();
  gm_pLabel1 = new CButtonWidget();
  gm_pLabel2 = new CButtonWidget();
  gm_pQuickLoad = new CButtonWidget();
  gm_pQuickSave = new CButtonWidget();
  gm_pLoad = new CButtonWidget();
  gm_pSave = new CButtonWidget();
  gm_pDemoRec = new CButtonWidget();
  gm_pHighScore = new CButtonWidget();
  gm_pOptions = new CButtonWidget();
  gm_pStop = new CButtonWidget();
  gm_pQuit = new CButtonWidget();
}

void CInGameMenu::Initialize_t(void)
{  
  // intialize main menu
  gm_mgTitle->SetText(TRANS("GAME"));
  gm_mgTitle->mg_boxOnScreen = BoxTitle();
  AddChild(gm_mgTitle);

  gm_pLabel1->SetText("");
  gm_pLabel1->mg_boxOnScreen = BoxMediumRow(-2.0);
  gm_pLabel1->mg_bfsFontSize = BFS_MEDIUM;
  gm_pLabel1->mg_iCenterI = -1;
  gm_pLabel1->mg_bEnabled = FALSE;
  gm_pLabel1->mg_bLabel = TRUE;
  AddChild(gm_pLabel1);

  gm_pLabel2->SetText("");
  gm_pLabel2->mg_boxOnScreen = BoxMediumRow(-1.0);
  gm_pLabel2->mg_bfsFontSize = BFS_MEDIUM;
  gm_pLabel2->mg_iCenterI = -1;
  gm_pLabel2->mg_bEnabled = FALSE;
  gm_pLabel2->mg_bLabel = TRUE;
  AddChild(gm_pLabel2);

  // Quick Load
  gm_pQuickLoad->SetText(TRANS("QUICK LOAD"));
  gm_pQuickLoad->mg_bfsFontSize = BFS_LARGE;
  gm_pQuickLoad->mg_boxOnScreen = BoxBigRow(0.0f);
  gm_pQuickLoad->SetTooltip(TRANS("load a quick-saved game (F9)"));
  AddChild(gm_pQuickLoad);
  gm_pQuickLoad->mg_pmgUp = gm_pQuit;
  gm_pQuickLoad->mg_pmgDown = gm_pQuickSave;
  gm_pQuickLoad->mg_pActivatedFunction = NULL;

  // Quick Save
  gm_pQuickSave->SetText(TRANS("QUICK SAVE"));
  gm_pQuickSave->mg_bfsFontSize = BFS_LARGE;
  gm_pQuickSave->mg_boxOnScreen = BoxBigRow(1.0f);
  gm_pQuickSave->SetTooltip(TRANS("quick-save current game (F6)"));
  AddChild(gm_pQuickSave);
  gm_pQuickSave->mg_pmgUp = gm_pQuickLoad;
  gm_pQuickSave->mg_pmgDown = gm_pLoad;
  gm_pQuickSave->mg_pActivatedFunction = NULL;

  // Load
  gm_pLoad->SetText(TRANS("LOAD"));
  gm_pLoad->mg_bfsFontSize = BFS_LARGE;
  gm_pLoad->mg_boxOnScreen = BoxBigRow(2.0f);
  gm_pLoad->SetTooltip(TRANS("load a saved game"));
  AddChild(gm_pLoad);
  gm_pLoad->mg_pmgUp = gm_pQuickSave;
  gm_pLoad->mg_pmgDown = gm_pSave;
  gm_pLoad->mg_pActivatedFunction = NULL;

  // Save
  gm_pSave->SetText(TRANS("SAVE"));
  gm_pSave->mg_bfsFontSize = BFS_LARGE;
  gm_pSave->mg_boxOnScreen = BoxBigRow(3.0f);
  gm_pSave->SetTooltip(TRANS("save current game (each player has own slots!)"));
  AddChild(gm_pSave);
  gm_pSave->mg_pmgUp = gm_pLoad;
  gm_pSave->mg_pmgDown = gm_pDemoRec;
  gm_pSave->mg_pActivatedFunction = NULL;

  // Demo Record/Stop
  gm_pDemoRec->mg_boxOnScreen = BoxBigRow(4.0f);
  gm_pDemoRec->mg_bfsFontSize = BFS_LARGE;
  gm_pDemoRec->mg_pmgUp = gm_pSave;
  gm_pDemoRec->mg_pmgDown = gm_pHighScore;
  gm_pDemoRec->mg_strText = "Text not set";
  AddChild(gm_pDemoRec);
  gm_pDemoRec->mg_pActivatedFunction = NULL;

  // High Score
  gm_pHighScore->SetText(TRANS("HIGH SCORES"));
  gm_pHighScore->mg_bfsFontSize = BFS_LARGE;
  gm_pHighScore->mg_boxOnScreen = BoxBigRow(5.0f);
  gm_pHighScore->SetTooltip(TRANS("view list of top ten best scores"));
  AddChild(gm_pHighScore);
  gm_pHighScore->mg_pmgUp = gm_pDemoRec;
  gm_pHighScore->mg_pmgDown = gm_pOptions;
  gm_pHighScore->mg_pActivatedFunction = NULL;

  // Options
  gm_pOptions->SetText(TRANS("OPTIONS"));
  gm_pOptions->mg_bfsFontSize = BFS_LARGE;
  gm_pOptions->mg_boxOnScreen = BoxBigRow(6.0f);
  gm_pOptions->SetTooltip(TRANS("adjust video, audio and input options"));
  AddChild(gm_pOptions);
  gm_pOptions->mg_pmgUp = gm_pHighScore;
  gm_pOptions->mg_pmgDown = gm_pStop;
  gm_pOptions->mg_pActivatedFunction = NULL;

  // Stop Game
  gm_pStop->SetText(TRANS("STOP GAME"));
  gm_pStop->mg_bfsFontSize = BFS_LARGE;
  gm_pStop->mg_boxOnScreen = BoxBigRow(7.0f);
  gm_pStop->SetTooltip(TRANS("stop currently running game"));
  AddChild(gm_pStop);
  gm_pStop->mg_pmgUp = gm_pOptions;
  gm_pStop->mg_pmgDown = gm_pQuit;
  gm_pStop->mg_pActivatedFunction = NULL;

  // Quit
  gm_pQuit->SetText(TRANS("QUIT"));
  gm_pQuit->mg_bfsFontSize = BFS_LARGE;
  gm_pQuit->mg_boxOnScreen = BoxBigRow(8.0f);
  gm_pQuit->SetTooltip(TRANS("exit game immediately"));
  AddChild(gm_pQuit);
  gm_pQuit->mg_pmgUp = gm_pStop;
  gm_pQuit->mg_pmgDown = gm_pQuickLoad;
  gm_pQuit->mg_pActivatedFunction = NULL;
}

void CInGameMenu::StartMenu(void)
{
  gm_pQuickLoad->mg_bEnabled = _pNetwork->IsServer();
  gm_pQuickSave->mg_bEnabled = _pNetwork->IsServer();
  gm_pLoad->mg_bEnabled = _pNetwork->IsServer();
  gm_pSave->mg_bEnabled = _pNetwork->IsServer();
  gm_pDemoRec->mg_bEnabled = TRUE;//_pNetwork->IsServer();
  extern void SetDemoStartStopRecText();
  SetDemoStartStopRecText();


  if (_gmRunningGameMode == GM_SINGLE_PLAYER) {
    const INDEX iSinglePlayer = _pGame->GetSinglePlayerIndex();
    const CPlayerCharacter &pc = _pGame->GetPlayers()[iSinglePlayer];
    
    CTString strNameText;
    strNameText.PrintF(TRANS("Player: %s"), pc.GetNameForPrinting());
    gm_pLabel1->SetText(strNameText);
    gm_pLabel2->mg_strText = "";

  } else {
    if (_pNetwork->IsServer()) {

      CTString strHost, strAddress;
      CTString strHostName;
      _pNetwork->GetHostName(strHost, strAddress);
      if (strHost == "") {
        strHostName = TRANS("<not started yet>");
      }
      else {
        strHostName = strHost + " (" + strAddress + ")";
      }

      gm_pLabel1->mg_strText = TRANS("Address: ") + strHostName;
      gm_pLabel2->mg_strText = "";

    } else {

      CTString strConfig;
      strConfig = TRANS("<not adjusted>");
      extern CTString sam_strNetworkSettings;
      if (sam_strNetworkSettings != "") {
        LoadStringVar(CTFileName(sam_strNetworkSettings).NoExt() + ".des", strConfig);
        strConfig.OnlyFirstLine();
      }

      gm_pLabel1->mg_strText = TRANS("Connected to: ") + _pGame->GetJoinAddress();
      gm_pLabel2->mg_strText = TRANS("Connection: ") + strConfig;
    }
  }

  CGameMenu::StartMenu();
}