/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "Interface/Common/MenuStuff.h"
#include "NetworkJoin.h"

CNetworkJoinMenu::CNetworkJoinMenu()
{
  gm_pTitle = new CTitleWidget();
  gm_pLAN = new CButtonWidget();
  gm_pNET = new CButtonWidget();
  gm_pOpen = new CButtonWidget();
}

void CNetworkJoinMenu::Initialize_t(void)
{
  // title
  gm_pTitle->mg_boxOnScreen = BoxTitle();
  gm_pTitle->SetText(TRANS("JOIN GAME"));
  AddChild(gm_pTitle);

  // Search LAN
  gm_pLAN->SetText(TRANS("SEARCH LAN"));
  gm_pLAN->mg_bfsFontSize = BFS_LARGE;
  gm_pLAN->mg_boxOnScreen = BoxBigRow(1.0f);
  gm_pLAN->mg_pmgUp = gm_pOpen;
  gm_pLAN->mg_pmgDown = gm_pNET;
  gm_pLAN->SetTooltip(TRANS("search local network for servers"));
  AddChild(gm_pLAN);
  gm_pLAN->mg_pActivatedFunction = NULL;

  // Search Interned
  gm_pNET->SetText(TRANS("SEARCH INTERNET"));
  gm_pNET->mg_bfsFontSize = BFS_LARGE;
  gm_pNET->mg_boxOnScreen = BoxBigRow(2.0f);
  gm_pNET->mg_pmgUp = gm_pLAN;
  gm_pNET->mg_pmgDown = gm_pOpen;
  gm_pNET->SetTooltip(TRANS("search internet for servers"));
  AddChild(gm_pNET);
  gm_pNET->mg_pActivatedFunction = NULL;

  // Specify Server
  gm_pOpen->SetText(TRANS("SPECIFY SERVER"));
  gm_pOpen->mg_bfsFontSize = BFS_LARGE;
  gm_pOpen->mg_boxOnScreen = BoxBigRow(3.0f);
  gm_pOpen->mg_pmgUp = gm_pNET;
  gm_pOpen->mg_pmgDown = gm_pLAN;
  gm_pOpen->SetTooltip(TRANS("type in server address to connect to"));
  AddChild(gm_pOpen);
  gm_pOpen->mg_pActivatedFunction = NULL;
}