/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "Servers.h"

CTString _strServerFilter[7];
CButtonWidget mgServerColumn[7];
CEditWidget mgServerFilter[7];

CServersMenu::CServersMenu()
{
  gm_pTitle = new CTitleWidget();
  gm_pList = new CServerListWidget();
  gm_pRefresh = new CButtonWidget();
}

void CServersMenu::Initialize_t(void)
{
  gm_pTitle->mg_boxOnScreen = BoxTitle();
  gm_pTitle->SetText(TRANS("CHOOSE SERVER"));
  AddChild(gm_pTitle);

  gm_pList->mg_boxOnScreen = FLOATaabbox2D(FLOAT2D(0, 0), FLOAT2D(1, 1));
  gm_pList->mg_pmgLeft = gm_pList;  // make sure it can get focus
  gm_pList->mg_bEnabled = TRUE;
  AddChild(gm_pList);

  ASSERT(ARRAYCOUNT(mgServerColumn) == ARRAYCOUNT(mgServerFilter));
  for (INDEX i = 0; i<ARRAYCOUNT(mgServerFilter); i++) {
    mgServerColumn[i].mg_strText = "";
    mgServerColumn[i].mg_boxOnScreen = BoxPlayerEdit(5.0);
    mgServerColumn[i].mg_bfsFontSize = BFS_SMALL;
    mgServerColumn[i].mg_iCenterI = -1;
    mgServerColumn[i].mg_pmgUp = gm_pList;
    mgServerColumn[i].mg_pmgDown = &mgServerFilter[i];
    AddChild(&mgServerColumn[i]);

    mgServerFilter[i].mg_ctMaxStringLen = 25;
    mgServerFilter[i].mg_boxOnScreen = BoxPlayerEdit(5.0);
    mgServerFilter[i].mg_bfsFontSize = BFS_SMALL;
    mgServerFilter[i].mg_iCenterI = -1;
    mgServerFilter[i].mg_pmgUp = &mgServerColumn[i];
    mgServerFilter[i].mg_pmgDown = gm_pList;
    AddChild(&mgServerFilter[i]);
    mgServerFilter[i].mg_pstrToChange = &_strServerFilter[i];
    mgServerFilter[i].SetText(*mgServerFilter[i].mg_pstrToChange);
  }

  gm_pRefresh->mg_strText = TRANS("REFRESH");
  gm_pRefresh->mg_boxOnScreen = BoxLeftColumn(15.0);
  gm_pRefresh->mg_bfsFontSize = BFS_SMALL;
  gm_pRefresh->mg_iCenterI = -1;
  gm_pRefresh->mg_pmgDown = gm_pList;
  gm_pRefresh->mg_pActivatedFunction = NULL;
  AddChild(gm_pRefresh);

  CTString astrColumns[7];
  mgServerColumn[0].SetText(TRANS("Server"));
  mgServerColumn[1].SetText(TRANS("Map"));
  mgServerColumn[2].SetText(TRANS("Ping"));
  mgServerColumn[3].SetText(TRANS("Players"));
  mgServerColumn[4].SetText(TRANS("Game"));
  mgServerColumn[5].SetText(TRANS("Mod"));
  mgServerColumn[6].SetText(TRANS("Ver"));
  mgServerColumn[0].mg_pActivatedFunction = NULL;
  mgServerColumn[1].mg_pActivatedFunction = NULL;
  mgServerColumn[2].mg_pActivatedFunction = NULL;
  mgServerColumn[3].mg_pActivatedFunction = NULL;
  mgServerColumn[4].mg_pActivatedFunction = NULL;
  mgServerColumn[5].mg_pActivatedFunction = NULL;
  mgServerColumn[6].mg_pActivatedFunction = NULL;
  mgServerColumn[0].SetTooltip(TRANS("sort by server"));
  mgServerColumn[1].SetTooltip(TRANS("sort by map"));
  mgServerColumn[2].SetTooltip(TRANS("sort by ping"));
  mgServerColumn[3].SetTooltip(TRANS("sort by players"));
  mgServerColumn[4].SetTooltip(TRANS("sort by game"));
  mgServerColumn[5].SetTooltip(TRANS("sort by mod"));
  mgServerColumn[6].SetTooltip(TRANS("sort by version"));
  mgServerFilter[0].SetTooltip(TRANS("filter by server"));
  mgServerFilter[1].SetTooltip(TRANS("filter by map"));
  mgServerFilter[2].SetTooltip(TRANS("filter by ping (ie. <200)"));
  mgServerFilter[3].SetTooltip(TRANS("filter by players (ie. >=2)"));
  mgServerFilter[4].SetTooltip(TRANS("filter by game (ie. coop)"));
  mgServerFilter[5].SetTooltip(TRANS("filter by mod"));
  mgServerFilter[6].SetTooltip(TRANS("filter by version"));
}

void CServersMenu::StartMenu(void)
{
  extern void RefreshServerList(void);
  RefreshServerList();

  CGameMenu::StartMenu();
}

void CServersMenu::Think(void)
{
  if (!_pNetwork->ga_bEnumerationChange) {
    return;
  }

  _pNetwork->ga_bEnumerationChange = FALSE;
}
