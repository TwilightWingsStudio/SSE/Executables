/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "VarList.h"
#include "Var.h"

extern BOOL _bVarChanged;

CVarMenu::CVarMenu()
{
  gm_pTitle = new CTitleWidget();
  gm_pApply = new CButtonWidget();
  gm_pArrowUp = new CArrowWidget();
  gm_pArrowDn = new CArrowWidget();
  
  for (INDEX iVar = 0; iVar < LEVELS_ON_SCREEN; iVar++)
  {
    gm_pVar[iVar] = new CVarButtonWidget();
  }
}

void CVarMenu::Initialize_t(void)
{
  gm_pTitle->mg_boxOnScreen = BoxTitle();
  gm_pTitle->SetText("");
  AddChild(gm_pTitle);

  for (INDEX iLabel = 0; iLabel<VARS_ON_SCREEN; iLabel++)
  {
    INDEX iPrev = (VARS_ON_SCREEN + iLabel - 1) % VARS_ON_SCREEN;
    INDEX iNext = (iLabel + 1) % VARS_ON_SCREEN;
    // initialize label gadgets
    gm_pVar[iLabel]->mg_pmgUp = gm_pVar[iPrev];
    gm_pVar[iLabel]->mg_pmgDown = gm_pVar[iNext];
    gm_pVar[iLabel]->mg_pmgLeft = gm_pApply;
    gm_pVar[iLabel]->mg_boxOnScreen = BoxMediumRow(iLabel);
    gm_pVar[iLabel]->mg_pActivatedFunction = NULL; // never called!
    AddChild(gm_pVar[iLabel]);
  }

  gm_pApply->mg_boxOnScreen = BoxMediumRow(16.5f);
  gm_pApply->mg_bfsFontSize = BFS_LARGE;
  gm_pApply->mg_iCenterI = 1;
  gm_pApply->mg_pmgLeft =
    gm_pApply->mg_pmgRight =
    gm_pApply->mg_pmgUp =
    gm_pApply->mg_pmgDown = gm_pVar[0];
  gm_pApply->SetText(TRANS("APPLY"));
  gm_pApply->SetTooltip(TRANS("apply changes"));
  AddChild(gm_pApply);
  gm_pApply->mg_pActivatedFunction = NULL;

  AddChild(gm_pArrowUp);
  AddChild(gm_pArrowDn);
  gm_pArrowUp->mg_adDirection = AD_UP;
  gm_pArrowDn->mg_adDirection = AD_DOWN;
  gm_pArrowUp->mg_boxOnScreen = BoxArrow(AD_UP);
  gm_pArrowDn->mg_boxOnScreen = BoxArrow(AD_DOWN);
  gm_pArrowUp->mg_pmgRight = gm_pArrowUp->mg_pmgDown = gm_pVar[0];
  gm_pArrowDn->mg_pmgRight = gm_pArrowDn->mg_pmgUp = gm_pVar[VARS_ON_SCREEN - 1];

  gm_ctListVisible = VARS_ON_SCREEN;
  gm_pmgArrowUp = gm_pArrowUp;
  gm_pmgArrowDn = gm_pArrowDn;
  gm_pmgListTop = gm_pVar[0];
  gm_pmgListBottom = gm_pVar[VARS_ON_SCREEN - 1];
}

void CVarMenu::FillListItems(void)
{
  // disable all items first
  for (INDEX i = 0; i<VARS_ON_SCREEN; i++) {
    gm_pVar[i]->mg_bEnabled = FALSE;
    gm_pVar[i]->mg_pvsVar = NULL;
    gm_pVar[i]->mg_iInList = -2;
  }
  BOOL bHasFirst = FALSE;
  BOOL bHasLast = FALSE;
  INDEX ctLabels = _lhVarSettings.Count();
  INDEX iLabel = 0;

  FOREACHINLIST(CVarSetting, vs_lnNode, _lhVarSettings, itvs) {
    CVarSetting &vs = *itvs;
    INDEX iInMenu = iLabel - gm_iListOffset;
    if ((iLabel >= gm_iListOffset) &&
      (iLabel<(gm_iListOffset + VARS_ON_SCREEN)))
    {
      bHasFirst |= (iLabel == 0);
      bHasLast |= (iLabel == ctLabels - 1);
      gm_pVar[iInMenu]->mg_pvsVar = &vs;
      gm_pVar[iInMenu]->SetTooltip(vs.vs_strTip);
      gm_pVar[iInMenu]->mg_bEnabled = gm_pVar[iInMenu]->IsEnabled();
      gm_pVar[iInMenu]->mg_iInList = iLabel;
    }
    iLabel++;
  }
  // enable/disable up/down arrows
  gm_pArrowUp->mg_bEnabled = !bHasFirst && ctLabels>0;
  gm_pArrowDn->mg_bEnabled = !bHasLast  && ctLabels>0;
}

void CVarMenu::StartMenu(void)
{
  LoadVarSettings(gm_fnmMenuCFG);
  // set default parameters for the list
  gm_iListOffset = 0;
  gm_ctListTotal = _lhVarSettings.Count();
  gm_iListWantedItem = 0;
  CGameMenu::StartMenu();
}

void CVarMenu::EndMenu(void)
{
  // disable all items first
  for (INDEX i = 0; i<VARS_ON_SCREEN; i++) {
    gm_pVar[i]->mg_bEnabled = FALSE;
    gm_pVar[i]->mg_pvsVar = NULL;
    gm_pVar[i]->mg_iInList = -2;
  }
  FlushVarSettings(FALSE);
  _bVarChanged = FALSE;
}

void CVarMenu::Think(void)
{
  gm_pApply->mg_bEnabled = _bVarChanged;
  extern void FixupBackButton(CGameMenu *pgm);
  FixupBackButton(this);
}