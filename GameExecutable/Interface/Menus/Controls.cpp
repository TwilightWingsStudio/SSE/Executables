/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "Interface/Common/MenuStuff.h"
#include "Controls.h"

extern CTFileName _fnmControlsToCustomize;

CControlsMenu::CControlsMenu()
{
  gm_pTitle = new CTitleWidget();
  gm_pNameLabel = new CButtonWidget();
  gm_pButtons = new CButtonWidget();
  gm_pAdvanced = new CButtonWidget();
  gm_pSensitivity = new CSliderWidget();
  gm_pInvertTrigger = new CTriggerWidget();
  gm_pSmoothTrigger = new CTriggerWidget();
  gm_pAccelTrigger = new CTriggerWidget();
  gm_pIFeelTrigger = new CTriggerWidget();
  gm_pPredefined = new CButtonWidget();
}

void CControlsMenu::Initialize_t(void)
{
  // intialize player and controls menu
  gm_pTitle->mg_boxOnScreen = BoxTitle();
  gm_pTitle->SetText(TRANS("CONTROLS"));
  AddChild(gm_pTitle);

  gm_pNameLabel->mg_strText = "";
  gm_pNameLabel->mg_boxOnScreen = BoxMediumRow(0.0);
  gm_pNameLabel->mg_bfsFontSize = BFS_MEDIUM;
  gm_pNameLabel->mg_iCenterI = -1;
  gm_pNameLabel->mg_bEnabled = FALSE;
  gm_pNameLabel->mg_bLabel = TRUE;
  AddChild(gm_pNameLabel);

  gm_pButtons->SetText(TRANS("CUSTOMIZE BUTTONS"));
  gm_pButtons->mg_boxOnScreen = BoxMediumRow(2.0);
  gm_pButtons->mg_bfsFontSize = BFS_MEDIUM;
  gm_pButtons->mg_iCenterI = 0;
  AddChild(gm_pButtons);
  gm_pButtons->mg_pmgUp = gm_pPredefined;
  gm_pButtons->mg_pmgDown = gm_pAdvanced;
  gm_pButtons->mg_pActivatedFunction = NULL;
  gm_pButtons->SetTooltip(TRANS("customize buttons in current controls"));

  gm_pAdvanced->SetText(TRANS("ADVANCED JOYSTICK SETUP"));
  gm_pAdvanced->mg_iCenterI = 0;
  gm_pAdvanced->mg_boxOnScreen = BoxMediumRow(3);
  gm_pAdvanced->mg_bfsFontSize = BFS_MEDIUM;
  AddChild(gm_pAdvanced);
  gm_pAdvanced->mg_pmgUp = gm_pButtons;
  gm_pAdvanced->mg_pmgDown = gm_pSensitivity;
  gm_pAdvanced->mg_pActivatedFunction = NULL;
  gm_pAdvanced->SetTooltip(TRANS("adjust advanced settings for joystick axis"));

  gm_pSensitivity->SetText(TRANS("SENSITIVITY"));
  gm_pSensitivity->mg_boxOnScreen = BoxMediumRow(4.5);
  gm_pSensitivity->mg_pmgUp = gm_pAdvanced;
  gm_pSensitivity->mg_pmgDown = gm_pInvertTrigger;
  gm_pSensitivity->SetTooltip(TRANS("sensitivity for all axis in this control set"));
  AddChild(gm_pSensitivity);

  // Invert Look
  gm_pInvertTrigger->mg_strLabel = TRANS("INVERT LOOK");
  gm_pInvertTrigger->mg_pmgUp = gm_pSensitivity;
  gm_pInvertTrigger->mg_pmgDown = gm_pSmoothTrigger;
  gm_pInvertTrigger->mg_boxOnScreen = BoxMediumRow(5.5);
  gm_pInvertTrigger->mg_astrTexts = astrNoYes;
  gm_pInvertTrigger->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pInvertTrigger->mg_iSelected = 0;
  gm_pInvertTrigger->mg_strValue = astrNoYes[0];
  AddChild(gm_pInvertTrigger);
  gm_pInvertTrigger->SetTooltip(TRANS("invert up/down looking"));
  
  // Smooth Axis
  gm_pSmoothTrigger->mg_strLabel = TRANS("SMOOTH AXIS");
  gm_pSmoothTrigger->mg_pmgUp = gm_pInvertTrigger;
  gm_pSmoothTrigger->mg_pmgDown = gm_pAccelTrigger;
  gm_pSmoothTrigger->mg_boxOnScreen = BoxMediumRow(6.5);
  gm_pSmoothTrigger->mg_astrTexts = astrNoYes;
  gm_pSmoothTrigger->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pSmoothTrigger->mg_iSelected = 0;
  gm_pSmoothTrigger->mg_strValue = astrNoYes[0];
  AddChild(gm_pSmoothTrigger);
  gm_pSmoothTrigger->SetTooltip(TRANS("smooth mouse/joystick movements"));

  // Mouse Acceleration
  gm_pAccelTrigger->mg_strLabel = TRANS("MOUSE ACCELERATION");
  gm_pAccelTrigger->mg_pmgUp = gm_pSmoothTrigger;
  gm_pAccelTrigger->mg_pmgDown = gm_pIFeelTrigger;
  gm_pAccelTrigger->mg_boxOnScreen = BoxMediumRow(7.5);
  gm_pAccelTrigger->mg_astrTexts = astrNoYes;
  gm_pAccelTrigger->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pAccelTrigger->mg_iSelected = 0;
  gm_pAccelTrigger->mg_strValue = astrNoYes[0];
  AddChild(gm_pAccelTrigger);
  gm_pAccelTrigger->SetTooltip(TRANS("allow mouse acceleration"));

  // Enable IFEEL
  gm_pIFeelTrigger->mg_strLabel = TRANS("ENABLE IFEEL");
  gm_pIFeelTrigger->mg_pmgUp = gm_pAccelTrigger;
  gm_pIFeelTrigger->mg_pmgDown = gm_pPredefined;
  gm_pIFeelTrigger->mg_boxOnScreen = BoxMediumRow(8.5);
  gm_pIFeelTrigger->mg_astrTexts = astrNoYes;
  gm_pIFeelTrigger->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pIFeelTrigger->mg_iSelected = 0;
  gm_pIFeelTrigger->mg_strValue = astrNoYes[0];
  AddChild(gm_pIFeelTrigger);
  gm_pIFeelTrigger->SetTooltip(TRANS("enable support for iFeel tactile feedback mouse"));

  // Predefined
  gm_pPredefined->SetText(TRANS("LOAD PREDEFINED SETTINGS"));
  gm_pPredefined->mg_iCenterI = 0;
  gm_pPredefined->mg_boxOnScreen = BoxMediumRow(10);
  gm_pPredefined->mg_bfsFontSize = BFS_MEDIUM;
  AddChild(gm_pPredefined);
  gm_pPredefined->mg_pmgUp = gm_pIFeelTrigger;
  gm_pPredefined->mg_pmgDown = gm_pButtons;
  gm_pPredefined->mg_pActivatedFunction = NULL;
  gm_pPredefined->SetTooltip(TRANS("load one of several predefined control settings"));
}

void CControlsMenu::StartMenu(void)
{
  gm_pmgSelectedByDefault = gm_pButtons;
  INDEX iPlayer = _pGame->GetSinglePlayerIndex();

  if (_iLocalPlayer >= 0 && _iLocalPlayer < 4) {
    iPlayer = _pGame->GetMenuLocalPlayers()[_iLocalPlayer];
  }

  _fnmControlsToCustomize.PrintF("UserData\\Controls\\Controls%d.ctl", iPlayer);

  ControlsMenuOn();

  CTString strNameText;
  strNameText.PrintF(TRANS("CONTROLS FOR: %s"), _pGame->GetPlayers()[iPlayer].GetNameForPrinting());
  gm_pNameLabel->SetText(strNameText);

  ObtainActionSettings();
  CGameMenu::StartMenu();
}

void CControlsMenu::EndMenu(void)
{
  ApplyActionSettings();

  ControlsMenuOff();

  CGameMenu::EndMenu();
}

void CControlsMenu::ObtainActionSettings(void)
{
  CControls &ctrls = _pGame->GetControlsExtra();

  gm_pSensitivity->mg_iMinPos = 0;
  gm_pSensitivity->mg_iMaxPos = 50;
  gm_pSensitivity->mg_iCurPos = ctrls.ctrl_fSensitivity / 2;
  gm_pSensitivity->ApplyCurrentPosition();

  gm_pInvertTrigger->mg_iSelected = ctrls.ctrl_bInvertLook ? 1 : 0;
  gm_pSmoothTrigger->mg_iSelected = ctrls.ctrl_bSmoothAxes ? 1 : 0;
  gm_pAccelTrigger->mg_iSelected = _pShell->GetINDEX("inp_bAllowMouseAcceleration") ? 1 : 0;
  gm_pIFeelTrigger->mg_bEnabled = _pShell->GetINDEX("sys_bIFeelEnabled") ? 1 : 0;
  gm_pIFeelTrigger->mg_iSelected = _pShell->GetFLOAT("inp_fIFeelGain")>0 ? 1 : 0;

  gm_pInvertTrigger->ApplyCurrentSelection();
  gm_pSmoothTrigger->ApplyCurrentSelection();
  gm_pAccelTrigger->ApplyCurrentSelection();
  gm_pIFeelTrigger->ApplyCurrentSelection();
}

void CControlsMenu::ApplyActionSettings(void)
{
  CControls &ctrls = _pGame->GetControlsExtra();

  FLOAT fSensitivity =
    FLOAT(gm_pSensitivity->mg_iCurPos - gm_pSensitivity->mg_iMinPos) /
    FLOAT(gm_pSensitivity->mg_iMaxPos - gm_pSensitivity->mg_iMinPos)*100.0f;

  BOOL bInvert = gm_pInvertTrigger->mg_iSelected != 0;
  BOOL bSmooth = gm_pSmoothTrigger->mg_iSelected != 0;
  BOOL bAccel = gm_pAccelTrigger->mg_iSelected != 0;
  BOOL bIFeel = gm_pIFeelTrigger->mg_iSelected != 0;

  if (INDEX(ctrls.ctrl_fSensitivity) != INDEX(fSensitivity)) {
    ctrls.ctrl_fSensitivity = fSensitivity;
  }
  ctrls.ctrl_bInvertLook = bInvert;
  ctrls.ctrl_bSmoothAxes = bSmooth;
  _pShell->SetINDEX("inp_bAllowMouseAcceleration", bAccel);
  _pShell->SetFLOAT("inp_fIFeelGain", bIFeel ? 1.0f : 0.0f);
  ctrls.CalculateInfluencesForAllAxis();
}