/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "Interface/Common/MenuStuff.h"
#include "SinglePlayerNew.h"


void CSinglePlayerNewMenu::Initialize_t(void)
{
  gm_pTitle = new CTitleWidget();
  gm_pTourist = new CButtonWidget();
  gm_pEasy = new CButtonWidget();
  gm_pMedium = new CButtonWidget();
  gm_pHard = new CButtonWidget();
  gm_pSerious = new CButtonWidget();
  gm_pMental = new CButtonWidget();

  // intialize single player new menu
  gm_pTitle->SetText(TRANS("NEW GAME"));
  gm_pTitle->mg_boxOnScreen = BoxTitle();
  AddChild(gm_pTitle);

  gm_pTourist->SetText(TRANS("TOURIST"));
  gm_pTourist->mg_bfsFontSize = BFS_LARGE;
  gm_pTourist->mg_boxOnScreen = BoxBigRow(0.0f);
  gm_pTourist->SetTooltip(TRANS("for non-FPS players"));
  AddChild(gm_pTourist);
  gm_pTourist->mg_pmgUp = gm_pSerious;
  gm_pTourist->mg_pmgDown = gm_pEasy;
  gm_pTourist->mg_pActivatedFunction = NULL;

  gm_pEasy->SetText(TRANS("EASY"));
  gm_pEasy->mg_bfsFontSize = BFS_LARGE;
  gm_pEasy->mg_boxOnScreen = BoxBigRow(1.0f);
  gm_pEasy->SetTooltip(TRANS("for unexperienced FPS players"));
  AddChild(gm_pEasy);
  gm_pEasy->mg_pmgUp = gm_pTourist;
  gm_pEasy->mg_pmgDown = gm_pMedium;
  gm_pEasy->mg_pActivatedFunction = NULL;

  gm_pMedium->SetText(TRANS("NORMAL"));
  gm_pMedium->mg_bfsFontSize = BFS_LARGE;
  gm_pMedium->mg_boxOnScreen = BoxBigRow(2.0f);
  gm_pMedium->SetTooltip(TRANS("for experienced FPS players"));
  AddChild(gm_pMedium);
  gm_pMedium->mg_pmgUp = gm_pEasy;
  gm_pMedium->mg_pmgDown = gm_pHard;
  gm_pMedium->mg_pActivatedFunction = NULL;

  gm_pHard->SetText(TRANS("HARD"));
  gm_pHard->mg_bfsFontSize = BFS_LARGE;
  gm_pHard->mg_boxOnScreen = BoxBigRow(3.0f);
  gm_pHard->SetTooltip(TRANS("for experienced Serious Sam players"));
  AddChild(gm_pHard);
  gm_pHard->mg_pmgUp = gm_pMedium;
  gm_pHard->mg_pmgDown = gm_pSerious;
  gm_pHard->mg_pActivatedFunction = NULL;

  gm_pSerious->SetText(TRANS("SERIOUS"));
  gm_pSerious->mg_bfsFontSize = BFS_LARGE;
  gm_pSerious->mg_boxOnScreen = BoxBigRow(4.0f);
  gm_pSerious->SetTooltip(TRANS("are you serious?"));
  AddChild(gm_pSerious);
  gm_pSerious->mg_pmgUp = gm_pHard;
  gm_pSerious->mg_pmgDown = gm_pTourist;
  gm_pSerious->mg_pActivatedFunction = NULL;

  gm_pMental->SetText(TRANS("MENTAL"));
  gm_pMental->mg_bfsFontSize = BFS_LARGE;
  gm_pMental->mg_boxOnScreen = BoxBigRow(5.0f);
  gm_pMental->SetTooltip(TRANS("you are not serious!"));
  AddChild(gm_pMental);
  gm_pMental->mg_pmgUp = gm_pSerious;
  gm_pMental->mg_pmgDown = gm_pTourist;
  gm_pMental->mg_pActivatedFunction = NULL;
  gm_pMental->mg_bMental = TRUE;
}

void CSinglePlayerNewMenu::StartMenu(void)
{
  CGameMenu::StartMenu();
  extern INDEX sam_bMentalActivated;
  if (sam_bMentalActivated) {
    gm_pMental->Appear();
    gm_pSerious->mg_pmgDown = gm_pMental;
    gm_pTourist->mg_pmgUp = gm_pMental;
  } else {
    gm_pMental->Disappear();
    gm_pSerious->mg_pmgDown = gm_pTourist;
    gm_pTourist->mg_pmgUp = gm_pSerious;
  }
}
