/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "Network.h"

CNetworkMenu::CNetworkMenu()
{
  gm_pTitle = new CTitleWidget();
  gm_pJoin = new CButtonWidget();
  gm_pStart = new CButtonWidget();
  gm_pQuickLoad = new CButtonWidget();
  gm_pLoad = new CButtonWidget();  
}

void CNetworkMenu::Initialize_t(void)
{
  // intialize network menu
  gm_pTitle->mg_boxOnScreen = BoxTitle();
  gm_pTitle->SetText(TRANS("NETWORK"));
  AddChild(gm_pTitle);

  // Join Game
  gm_pJoin->SetText(TRANS("JOIN GAME"));
  gm_pJoin->mg_bfsFontSize = BFS_LARGE;
  gm_pJoin->mg_boxOnScreen = BoxBigRow(1.0f);
  gm_pJoin->mg_pmgUp = gm_pLoad;
  gm_pJoin->mg_pmgDown = gm_pStart;
  gm_pJoin->SetTooltip(TRANS("join a network game"));
  AddChild(gm_pJoin);
  gm_pJoin->mg_pActivatedFunction = NULL;

  // Start Server
  gm_pStart->SetText(TRANS("START SERVER"));
  gm_pStart->mg_bfsFontSize = BFS_LARGE;
  gm_pStart->mg_boxOnScreen = BoxBigRow(2.0f);
  gm_pStart->mg_pmgUp = gm_pJoin;
  gm_pStart->mg_pmgDown = gm_pQuickLoad;
  gm_pStart->SetTooltip(TRANS("start a network game server"));
  AddChild(gm_pStart);
  gm_pStart->mg_pActivatedFunction = NULL;

  // Quick Load
  gm_pQuickLoad->SetText(TRANS("QUICK LOAD"));
  gm_pQuickLoad->mg_bfsFontSize = BFS_LARGE;
  gm_pQuickLoad->mg_boxOnScreen = BoxBigRow(3.0f);
  gm_pQuickLoad->mg_pmgUp = gm_pStart;
  gm_pQuickLoad->mg_pmgDown = gm_pLoad;
  gm_pQuickLoad->SetTooltip(TRANS("load a quick-saved game (F9)"));
  AddChild(gm_pQuickLoad);
  gm_pQuickLoad->mg_pActivatedFunction = NULL;

  // Load
  gm_pLoad->SetText(TRANS("LOAD"));
  gm_pLoad->mg_bfsFontSize = BFS_LARGE;
  gm_pLoad->mg_boxOnScreen = BoxBigRow(4.0f);
  gm_pLoad->mg_pmgUp = gm_pQuickLoad;
  gm_pLoad->mg_pmgDown = gm_pJoin;
  gm_pLoad->SetTooltip(TRANS("start server and load a network game (server only)"));
  AddChild(gm_pLoad);
  gm_pLoad->mg_pActivatedFunction = NULL;
}

void CNetworkMenu::StartMenu(void)
{
  CGameMenu::StartMenu();
}