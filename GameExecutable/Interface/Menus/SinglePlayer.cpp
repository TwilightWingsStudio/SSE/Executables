/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "Interface/Common/MenuStuff.h"
#include "SinglePlayer.h"


void CSinglePlayerMenu::Initialize_t(void)
{
  gm_pTitle = new CTitleWidget();
  gm_pPlayerLabel = new CButtonWidget();
  gm_pNewGame = new CButtonWidget();
  gm_pCustom = new CButtonWidget();
  gm_pQuickLoad = new CButtonWidget();
  gm_pLoad = new CButtonWidget();
  gm_pTraining = new CButtonWidget();
  gm_pTechTest = new CButtonWidget();
  gm_pPlayersAndControls = new CButtonWidget();
  gm_pOptions = new CButtonWidget();

  // intialize single player menu
  gm_pTitle->SetText(TRANS("SINGLE PLAYER"));
  gm_pTitle->mg_boxOnScreen = BoxTitle();
  AddChild(gm_pTitle);

  gm_pPlayerLabel->mg_boxOnScreen = BoxBigRow(-1.0f);
  gm_pPlayerLabel->mg_bfsFontSize = BFS_MEDIUM;
  gm_pPlayerLabel->mg_iCenterI = -1;
  gm_pPlayerLabel->mg_bEnabled = FALSE;
  gm_pPlayerLabel->mg_bLabel = TRUE;
  AddChild(gm_pPlayerLabel);

  // New Game
  gm_pNewGame->SetText(TRANS("NEW GAME"));
  gm_pNewGame->mg_bfsFontSize = BFS_LARGE;
  gm_pNewGame->mg_boxOnScreen = BoxBigRow(0.0f);
  gm_pNewGame->SetTooltip(TRANS("start new game with current player"));
  AddChild(gm_pNewGame);
  gm_pNewGame->mg_pmgUp = gm_pOptions;
  gm_pNewGame->mg_pmgDown = gm_pCustom;
  gm_pNewGame->mg_pActivatedFunction = NULL;

  // Custom Level
  gm_pCustom->SetText(TRANS("CUSTOM LEVEL"));
  gm_pCustom->mg_bfsFontSize = BFS_LARGE;
  gm_pCustom->mg_boxOnScreen = BoxBigRow(1.0f);
  gm_pCustom->SetTooltip(TRANS("start new game on a custom level"));
  AddChild(gm_pCustom);
  gm_pCustom->mg_pmgUp = gm_pNewGame;
  gm_pCustom->mg_pmgDown = gm_pQuickLoad;
  gm_pCustom->mg_pActivatedFunction = NULL;

  // Quick Load
  gm_pQuickLoad->SetText(TRANS("QUICK LOAD"));
  gm_pQuickLoad->mg_bfsFontSize = BFS_LARGE;
  gm_pQuickLoad->mg_boxOnScreen = BoxBigRow(2.0f);
  gm_pQuickLoad->SetTooltip(TRANS("load a quick-saved game (F9)"));
  AddChild(gm_pQuickLoad);
  gm_pQuickLoad->mg_pmgUp = gm_pCustom;
  gm_pQuickLoad->mg_pmgDown = gm_pLoad;
  gm_pQuickLoad->mg_pActivatedFunction = NULL;

  // Load
  gm_pLoad->SetText(TRANS("LOAD"));
  gm_pLoad->mg_bfsFontSize = BFS_LARGE;
  gm_pLoad->mg_boxOnScreen = BoxBigRow(3.0f);
  gm_pLoad->SetTooltip(TRANS("load a saved game of current player"));
  AddChild(gm_pLoad);
  gm_pLoad->mg_pmgUp = gm_pQuickLoad;
  gm_pLoad->mg_pmgDown = gm_pTraining;
  gm_pLoad->mg_pActivatedFunction = NULL;

  // Training
  gm_pTraining->SetText(TRANS("TRAINING"));
  gm_pTraining->mg_bfsFontSize = BFS_LARGE;
  gm_pTraining->mg_boxOnScreen = BoxBigRow(4.0f);
  gm_pTraining->SetTooltip(TRANS("start training level - KarnakDemo"));
  AddChild(gm_pTraining);
  gm_pTraining->mg_pmgUp = gm_pLoad;
  gm_pTraining->mg_pmgDown = gm_pTechTest;
  gm_pTraining->mg_pActivatedFunction = NULL;

  // Technology Test
  gm_pTechTest->SetText(TRANS("TECHNOLOGY TEST"));
  gm_pTechTest->mg_bfsFontSize = BFS_LARGE;
  gm_pTechTest->mg_boxOnScreen = BoxBigRow(5.0f);
  gm_pTechTest->SetTooltip(TRANS("start technology testing level"));
  AddChild(gm_pTechTest);
  gm_pTechTest->mg_pmgUp = gm_pTraining;
  gm_pTechTest->mg_pmgDown = gm_pPlayersAndControls;
  gm_pTechTest->mg_pActivatedFunction = NULL;

  // Player And Controls
  gm_pPlayersAndControls->SetText(TRANS("PLAYERS AND CONTROLS"));
  gm_pPlayersAndControls->mg_bfsFontSize = BFS_LARGE;
  gm_pPlayersAndControls->mg_boxOnScreen = BoxBigRow(6.0f);
  gm_pPlayersAndControls->mg_pmgUp = gm_pTechTest;
  gm_pPlayersAndControls->mg_pmgDown = gm_pOptions;
  gm_pPlayersAndControls->SetTooltip(TRANS("change currently active player or adjust controls"));
  AddChild(gm_pPlayersAndControls);
  gm_pPlayersAndControls->mg_pActivatedFunction = NULL;

  // Game Options
  gm_pOptions->SetText(TRANS("GAME OPTIONS"));
  gm_pOptions->mg_bfsFontSize = BFS_LARGE;
  gm_pOptions->mg_boxOnScreen = BoxBigRow(7.0f);
  gm_pOptions->SetTooltip(TRANS("adjust miscellaneous game options"));
  AddChild(gm_pOptions);
  gm_pOptions->mg_pmgUp = gm_pPlayersAndControls;
  gm_pOptions->mg_pmgDown = gm_pNewGame;
  gm_pOptions->mg_pActivatedFunction = NULL;
}

void CSinglePlayerMenu::StartMenu(void)
{
  gm_pTraining->mg_bEnabled = IsMenuEnabled("Training");
  gm_pTechTest->mg_bEnabled = IsMenuEnabled("Technology Test");

  if (gm_pTraining->mg_bEnabled) {
    if (gm_pTraining->GetParent() != this) {
      AddChild(gm_pTraining);
    }

    gm_pLoad->mg_boxOnScreen = BoxBigRow(3.0f);
    gm_pLoad->mg_pmgUp = gm_pQuickLoad;
    gm_pLoad->mg_pmgDown = gm_pTraining;

    gm_pTraining->mg_boxOnScreen = BoxBigRow(4.0f);
    gm_pTraining->mg_pmgUp = gm_pLoad;
    gm_pTraining->mg_pmgDown = gm_pTechTest;

    gm_pTechTest->mg_boxOnScreen = BoxBigRow(5.0f);
    gm_pTechTest->mg_pmgUp = gm_pTraining;
    gm_pTechTest->mg_pmgDown = gm_pPlayersAndControls;

    gm_pPlayersAndControls->mg_boxOnScreen = BoxBigRow(6.0f);
    gm_pOptions->mg_boxOnScreen = BoxBigRow(7.0f);

  } else {
    if (gm_pTraining->GetParent()) {
      gm_pTraining->SetParent(NULL);
    }

    gm_pLoad->mg_boxOnScreen = BoxBigRow(3.0f);
    gm_pLoad->mg_pmgUp = gm_pQuickLoad;
    gm_pLoad->mg_pmgDown = gm_pTechTest;

    gm_pTechTest->mg_boxOnScreen = BoxBigRow(4.0f);
    gm_pTechTest->mg_pmgUp = gm_pLoad;
    gm_pTechTest->mg_pmgDown = gm_pPlayersAndControls;

    gm_pPlayersAndControls->mg_boxOnScreen = BoxBigRow(5.0f);
    gm_pOptions->mg_boxOnScreen = BoxBigRow(6.0f);
  }

  CGameMenu::StartMenu();

  const INDEX iSinglePlayerProfile = _pGame->GetSinglePlayerIndex();
  const CPlayerCharacter &pc = _pGame->GetPlayers()[iSinglePlayerProfile];

  CTString strPlayer;
  strPlayer.PrintF(TRANS("Player: %s\n"), pc.GetNameForPrinting());
  gm_pPlayerLabel->SetText(strPlayer);
}
