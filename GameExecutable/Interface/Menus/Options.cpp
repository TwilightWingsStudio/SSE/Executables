/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "Options.h"

COptionsMenu::COptionsMenu()
{
  gm_pTitle = new CTitleWidget();
  gm_pVideoOptions = new CButtonWidget();
  gm_pAudioOptions = new CButtonWidget();
  gm_pPlayerProfileOptions = new CButtonWidget();
  gm_pNetworkOptions = new CButtonWidget();
  gm_pCustomOptions = new CButtonWidget();
  gm_pAddonOptions = new CButtonWidget();
}

void COptionsMenu::Initialize_t(void)
{
  // intialize options menu
  gm_pTitle->mg_boxOnScreen = BoxTitle();
  gm_pTitle->SetText(TRANS("OPTIONS"));
  AddChild(gm_pTitle);

  // Video Options
  gm_pVideoOptions->SetText(TRANS("VIDEO OPTIONS"));
  gm_pVideoOptions->mg_bfsFontSize = BFS_LARGE;
  gm_pVideoOptions->mg_boxOnScreen = BoxBigRow(0.0f);
  gm_pVideoOptions->mg_pmgUp = gm_pAddonOptions;
  gm_pVideoOptions->mg_pmgDown = gm_pAudioOptions;
  gm_pVideoOptions->SetTooltip(TRANS("set video mode and driver"));
  AddChild(gm_pVideoOptions);
  gm_pVideoOptions->mg_pActivatedFunction = NULL;

  // Audio Options
  gm_pAudioOptions->SetText(TRANS("AUDIO OPTIONS"));
  gm_pAudioOptions->mg_bfsFontSize = BFS_LARGE;
  gm_pAudioOptions->mg_boxOnScreen = BoxBigRow(1.0f);
  gm_pAudioOptions->mg_pmgUp = gm_pVideoOptions;
  gm_pAudioOptions->mg_pmgDown = gm_pPlayerProfileOptions;
  gm_pAudioOptions->SetTooltip(TRANS("set audio quality and volume"));
  AddChild(gm_pAudioOptions);
  gm_pAudioOptions->mg_pActivatedFunction = NULL;

  // Player And Controls
  gm_pPlayerProfileOptions->SetText(TRANS("PLAYERS AND CONTROLS"));
  gm_pPlayerProfileOptions->mg_bfsFontSize = BFS_LARGE;
  gm_pPlayerProfileOptions->mg_boxOnScreen = BoxBigRow(2.0f);
  gm_pPlayerProfileOptions->mg_pmgUp = gm_pAudioOptions;
  gm_pPlayerProfileOptions->mg_pmgDown = gm_pNetworkOptions;
  gm_pPlayerProfileOptions->SetTooltip(TRANS("change currently active player or adjust controls"));
  AddChild(gm_pPlayerProfileOptions);
  gm_pPlayerProfileOptions->mg_pActivatedFunction = NULL;

  // Network Connection
  gm_pNetworkOptions->SetText(TRANS("NETWORK CONNECTION"));
  gm_pNetworkOptions->mg_bfsFontSize = BFS_LARGE;
  gm_pNetworkOptions->mg_boxOnScreen = BoxBigRow(3);
  gm_pNetworkOptions->mg_pmgUp = gm_pPlayerProfileOptions;
  gm_pNetworkOptions->mg_pmgDown = gm_pCustomOptions;
  gm_pNetworkOptions->SetTooltip(TRANS("choose your connection parameters"));
  AddChild(gm_pNetworkOptions);
  gm_pNetworkOptions->mg_pActivatedFunction = NULL;

  // Custom Options
  gm_pCustomOptions->SetText(TRANS("ADVANCED OPTIONS"));
  gm_pCustomOptions->mg_bfsFontSize = BFS_LARGE;
  gm_pCustomOptions->mg_boxOnScreen = BoxBigRow(4);
  gm_pCustomOptions->mg_pmgUp = gm_pNetworkOptions;
  gm_pCustomOptions->mg_pmgDown = gm_pAddonOptions;
  gm_pCustomOptions->SetTooltip(TRANS("for advanced users only"));
  AddChild(gm_pCustomOptions);
  gm_pCustomOptions->mg_pActivatedFunction = NULL;

  // Execute Addon
  gm_pAddonOptions->SetText(TRANS("EXECUTE ADDON"));
  gm_pAddonOptions->mg_bfsFontSize = BFS_LARGE;
  gm_pAddonOptions->mg_boxOnScreen = BoxBigRow(5);
  gm_pAddonOptions->mg_pmgUp = gm_pCustomOptions;
  gm_pAddonOptions->mg_pmgDown = gm_pVideoOptions;
  gm_pAddonOptions->SetTooltip(TRANS("choose from list of addons to execute"));
  AddChild(gm_pAddonOptions);
  gm_pAddonOptions->mg_pActivatedFunction = NULL;
}