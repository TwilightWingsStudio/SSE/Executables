/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "LevelInfo.h"
#include "Interface/Common/MenuStuff.h"
#include "NetworkStart.h"

extern void UpdateNetworkLevel(INDEX iDummy);

CNetworkStartMenu::CNetworkStartMenu()
{
  gm_pTitle = new CTitleWidget();
  gm_pSessionName = new CEditWidget();
  gm_pGameType = new CTriggerWidget();
  gm_pDifficulty = new CTriggerWidget();
  gm_pLevel = new CButtonWidget();
  gm_pMaxPlayers = new CTriggerWidget();
  gm_pWaitAllPlayers = new CTriggerWidget();
  gm_pCustomizeGame = new CTriggerWidget();
  gm_pVisible = new CTriggerWidget();
  gm_pGameOptions = new CButtonWidget();
  gm_pStart = new CButtonWidget();
}

void CNetworkStartMenu::Initialize_t(void)
{
  // title
  gm_pTitle->mg_boxOnScreen = BoxTitle();
  gm_pTitle->SetText(TRANS("START SERVER"));
  AddChild(gm_pTitle);

  // session name edit box
  gm_pSessionName->mg_strText = _pGame->GetSessionName();
  gm_pSessionName->mg_strLabel = TRANS("Session name:");
  gm_pSessionName->mg_ctMaxStringLen = 25;
  gm_pSessionName->mg_pstrToChange = &_pGame->gam_strSessionName;
  gm_pSessionName->mg_boxOnScreen = BoxMediumRow(1);
  gm_pSessionName->mg_bfsFontSize = BFS_MEDIUM;
  gm_pSessionName->mg_iCenterI = -1;
  gm_pSessionName->mg_pmgUp = gm_pStart;
  gm_pSessionName->mg_pmgDown = gm_pGameType;
  gm_pSessionName->SetTooltip(TRANS("name the session to start"));

  // game type trigger
  gm_pGameType->mg_strLabel = TRANS("Game type:");
  gm_pGameType->mg_pmgUp = gm_pSessionName;
  gm_pGameType->mg_pmgDown = gm_pDifficulty;
  gm_pGameType->mg_boxOnScreen = BoxMediumRow(2);
  gm_pGameType->mg_astrTexts = astrGameTypeRadioTexts;
  gm_pGameType->mg_ctTexts = ctGameTypeRadioTexts;
  gm_pGameType->mg_iSelected = 0;
  gm_pGameType->mg_strValue = astrGameTypeRadioTexts[0];
  gm_pGameType->SetTooltip(TRANS("choose type of multiplayer game"));
  gm_pGameType->mg_pOnTriggerChange = &UpdateNetworkLevel;

  // difficulty trigger
  gm_pDifficulty->mg_strLabel = TRANS("Difficulty:");
  gm_pDifficulty->mg_pmgUp = gm_pGameType;
  gm_pDifficulty->mg_pmgDown = gm_pLevel;
  gm_pDifficulty->mg_boxOnScreen = BoxMediumRow(3);
  gm_pDifficulty->mg_astrTexts = astrDifficultyRadioTexts;
  gm_pDifficulty->mg_ctTexts = sizeof(astrDifficultyRadioTexts) / sizeof(astrDifficultyRadioTexts[0]);
  gm_pDifficulty->mg_iSelected = 0;
  gm_pDifficulty->mg_strValue = astrDifficultyRadioTexts[0];
  gm_pDifficulty->SetTooltip(TRANS("choose difficulty level"));

  // level name
  gm_pLevel->mg_strText = "";
  gm_pLevel->mg_strLabel = TRANS("Level:");
  gm_pLevel->mg_boxOnScreen = BoxMediumRow(4);
  gm_pLevel->mg_bfsFontSize = BFS_MEDIUM;
  gm_pLevel->mg_iCenterI = -1;
  gm_pLevel->mg_pmgUp = gm_pDifficulty;
  gm_pLevel->mg_pmgDown = gm_pMaxPlayers;
  gm_pLevel->SetTooltip(TRANS("choose the level to start"));
  gm_pLevel->mg_pActivatedFunction = NULL;

  // max players trigger
  gm_pMaxPlayers->mg_strLabel = TRANS("Max players:");
  gm_pMaxPlayers->mg_pmgUp = gm_pLevel;
  gm_pMaxPlayers->mg_pmgDown = gm_pWaitAllPlayers;
  gm_pMaxPlayers->mg_boxOnScreen = BoxMediumRow(5);
  gm_pMaxPlayers->mg_astrTexts = astrMaxPlayersRadioTexts;
  gm_pMaxPlayers->mg_ctTexts = sizeof(astrMaxPlayersRadioTexts) / sizeof(astrMaxPlayersRadioTexts[0]);
  gm_pMaxPlayers->mg_iSelected = 0;
  gm_pMaxPlayers->mg_strValue = astrMaxPlayersRadioTexts[0];
  gm_pMaxPlayers->SetTooltip(TRANS("choose maximum allowed number of players"));

  // wait all players trigger
  gm_pWaitAllPlayers->mg_strLabel = TRANS("Wait for all players:");
  gm_pWaitAllPlayers->mg_pmgUp = gm_pMaxPlayers;
  gm_pWaitAllPlayers->mg_pmgDown = gm_pCustomizeGame;
  gm_pWaitAllPlayers->mg_boxOnScreen = BoxMediumRow(6);
  gm_pWaitAllPlayers->mg_astrTexts = astrNoYes;
  gm_pWaitAllPlayers->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pWaitAllPlayers->mg_iSelected = 0;
  gm_pWaitAllPlayers->mg_strValue = astrNoYes[0];
  gm_pWaitAllPlayers->SetTooltip(TRANS("if on, game won't start until all players have joined"));
  
  // server visible trigger
  gm_pCustomizeGame->mg_strLabel = TRANS("Customize game:");
  gm_pCustomizeGame->mg_pmgUp = gm_pWaitAllPlayers;
  gm_pCustomizeGame->mg_pmgDown = gm_pVisible;
  gm_pCustomizeGame->mg_boxOnScreen = BoxMediumRow(7);
  gm_pCustomizeGame->mg_astrTexts = astrNoYes;
  gm_pCustomizeGame->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pCustomizeGame->mg_iSelected = 0;
  gm_pCustomizeGame->mg_strValue = astrNoYes[0];
  gm_pCustomizeGame->SetTooltip(TRANS(""));

  // server visible trigger
  gm_pVisible->mg_strLabel = TRANS("Server visible:");
  gm_pVisible->mg_pmgUp = gm_pCustomizeGame;
  gm_pVisible->mg_pmgDown = gm_pGameOptions;
  gm_pVisible->mg_boxOnScreen = BoxMediumRow(8);
  gm_pVisible->mg_astrTexts = astrNoYes;
  gm_pVisible->mg_ctTexts = sizeof(astrNoYes) / sizeof(astrNoYes[0]);
  gm_pVisible->mg_iSelected = 0;
  gm_pVisible->mg_strValue = astrNoYes[0];
  gm_pVisible->SetTooltip(TRANS("invisible servers are not listed, cleints have to join manually"));

  // options button
  gm_pGameOptions->SetText(TRANS("Game options"));
  gm_pGameOptions->mg_boxOnScreen = BoxMediumRow(9);
  gm_pGameOptions->mg_bfsFontSize = BFS_MEDIUM;
  gm_pGameOptions->mg_iCenterI = 0;
  gm_pGameOptions->mg_pmgUp = gm_pVisible;
  gm_pGameOptions->mg_pmgDown = gm_pStart;
  gm_pGameOptions->SetTooltip(TRANS("adjust game rules"));
  gm_pGameOptions->mg_pActivatedFunction = NULL;

  // start button
  gm_pStart->SetText(TRANS("START"));
  gm_pStart->mg_bfsFontSize = BFS_LARGE;
  gm_pStart->mg_boxOnScreen = BoxBigRow(7);
  gm_pStart->mg_pmgUp = gm_pGameOptions;
  gm_pStart->mg_pmgDown = gm_pSessionName;
  gm_pStart->mg_pActivatedFunction = NULL;

  AddChild(gm_pSessionName);
  AddChild(gm_pGameType);
  AddChild(gm_pDifficulty);
  AddChild(gm_pLevel);
  AddChild(gm_pMaxPlayers);
  AddChild(gm_pWaitAllPlayers);
  AddChild(gm_pCustomizeGame);
  AddChild(gm_pVisible);
  AddChild(gm_pGameOptions);
  AddChild(gm_pStart);
}

void CNetworkStartMenu::StartMenu(void)
{
  extern INDEX sam_bMentalActivated;
  gm_pDifficulty->mg_ctTexts = sam_bMentalActivated ? 6 : 5;

  gm_pGameType->mg_iSelected = Clamp(_pShell->GetINDEX("gam_iStartMode"), 0L, ctGameTypeRadioTexts - 1L);
  gm_pDifficulty->mg_iSelected = _pShell->GetINDEX("gam_iStartDifficulty") + 1;

  _pShell->SetINDEX("gam_iStartMode", CSessionProperties::GM_COOPERATIVE);

  INDEX ctMaxPlayers = _pShell->GetINDEX("gam_ctMaxPlayers");
  if (ctMaxPlayers<2 || ctMaxPlayers>16) {
    ctMaxPlayers = 2;
    _pShell->SetINDEX("gam_ctMaxPlayers", ctMaxPlayers);
  }

  gm_pMaxPlayers->mg_iSelected = ctMaxPlayers - 2;
  gm_pWaitAllPlayers->mg_iSelected = Clamp(_pShell->GetINDEX("gam_bWaitAllPlayers"), 0L, 1L);  
  gm_pCustomizeGame->mg_iSelected = _pShell->GetINDEX("gam_bCustomizedGame");
  gm_pVisible->mg_iSelected = _pShell->GetINDEX("ser_bEnumeration");

  // Apply selection for triggers.
  gm_pGameType->ApplyCurrentSelection();
  gm_pDifficulty->ApplyCurrentSelection();
  gm_pMaxPlayers->ApplyCurrentSelection();
  gm_pWaitAllPlayers->ApplyCurrentSelection();
  gm_pCustomizeGame->ApplyCurrentSelection();
  gm_pVisible->ApplyCurrentSelection();

  UpdateNetworkLevel(0);

  CGameMenu::StartMenu();
}

void CNetworkStartMenu::EndMenu(void)
{
  _pShell->SetINDEX("gam_iStartDifficulty", gm_pDifficulty->mg_iSelected - 1);
  _pShell->SetINDEX("gam_iStartMode", gm_pGameType->mg_iSelected);
  _pShell->SetINDEX("gam_bWaitAllPlayers", gm_pWaitAllPlayers->mg_iSelected);
  _pShell->SetINDEX("gam_ctMaxPlayers", gm_pMaxPlayers->mg_iSelected + 2);
  _pShell->SetINDEX("gam_bCustomizedGame", gm_pCustomizeGame->mg_iSelected);
  _pShell->SetINDEX("ser_bEnumeration", gm_pVisible->mg_iSelected);

  CGameMenu::EndMenu();
}
