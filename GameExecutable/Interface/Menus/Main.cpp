/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "Interface/Common/MenuStuff.h"
#include "Main.h"

CMainMenu::CMainMenu()
{
  gm_pVersionLabel = new CButtonWidget();
  gm_pModLabel = new CButtonWidget();
  gm_pSingle = new CButtonWidget();
  gm_pNetwork = new CButtonWidget();
  gm_pSplitScreen = new CButtonWidget();
  gm_pDemo = new CButtonWidget();
  gm_pMods = new CButtonWidget();
  gm_pHighScore = new CButtonWidget();
  gm_pOptions = new CButtonWidget();
  gm_pQuit = new CButtonWidget();
}

void CMainMenu::Initialize_t(void)
{
  extern CTString sam_strVersion;
  gm_pVersionLabel->SetText(sam_strVersion);
  gm_pVersionLabel->mg_boxOnScreen = BoxVersion();
  gm_pVersionLabel->mg_bfsFontSize = BFS_MEDIUM;
  gm_pVersionLabel->mg_iCenterI = +1;
  gm_pVersionLabel->mg_bEnabled = FALSE;
  gm_pVersionLabel->mg_bLabel = TRUE;
  AddChild(gm_pVersionLabel);

  extern CTString sam_strModName;
  gm_pModLabel->SetText(sam_strModName);
  gm_pModLabel->mg_boxOnScreen = BoxMediumRow(-2.0f);
  gm_pModLabel->mg_bfsFontSize = BFS_MEDIUM;
  gm_pModLabel->mg_iCenterI = 0;
  gm_pModLabel->mg_bEnabled = FALSE;
  gm_pModLabel->mg_bLabel = TRUE;
  AddChild(gm_pModLabel);

  gm_pSingle->SetText(TRANS("SINGLE PLAYER"));
  gm_pSingle->mg_bfsFontSize = BFS_LARGE;
  gm_pSingle->mg_boxOnScreen = BoxBigRow(0.0f);
  gm_pSingle->SetTooltip(TRANS("single player game menus"));
  AddChild(gm_pSingle);
  gm_pSingle->mg_pmgUp = gm_pQuit;
  gm_pSingle->mg_pmgDown = gm_pNetwork;
  gm_pSingle->mg_pActivatedFunction = NULL;

  gm_pNetwork->SetText(TRANS("NETWORK"));
  gm_pNetwork->mg_bfsFontSize = BFS_LARGE;
  gm_pNetwork->mg_boxOnScreen = BoxBigRow(1.0f);
  gm_pNetwork->SetTooltip(TRANS("LAN/iNet multiplayer menus"));
  AddChild(gm_pNetwork);
  gm_pNetwork->mg_pmgUp = gm_pSingle;
  gm_pNetwork->mg_pmgDown = gm_pSplitScreen;
  gm_pNetwork->mg_pActivatedFunction = NULL;

  gm_pSplitScreen->SetText(TRANS("SPLIT SCREEN"));
  gm_pSplitScreen->mg_bfsFontSize = BFS_LARGE;
  gm_pSplitScreen->mg_boxOnScreen = BoxBigRow(2.0f);
  gm_pSplitScreen->SetTooltip(TRANS("play with multiple players on one computer"));
  AddChild(gm_pSplitScreen);
  gm_pSplitScreen->mg_pmgUp = gm_pNetwork;
  gm_pSplitScreen->mg_pmgDown = gm_pDemo;
  gm_pSplitScreen->mg_pActivatedFunction = NULL;

  gm_pDemo->SetText(TRANS("DEMO"));
  gm_pDemo->mg_bfsFontSize = BFS_LARGE;
  gm_pDemo->mg_boxOnScreen = BoxBigRow(3.0f);
  gm_pDemo->SetTooltip(TRANS("play a game demo"));
  AddChild(gm_pDemo);
  gm_pDemo->mg_pmgUp = gm_pSplitScreen;
  gm_pDemo->mg_pmgDown = gm_pMods;
  gm_pDemo->mg_pActivatedFunction = NULL;

  gm_pMods->SetText(TRANS("MODS"));
  gm_pMods->mg_bfsFontSize = BFS_LARGE;
  gm_pMods->mg_boxOnScreen = BoxBigRow(4.0f);
  gm_pMods->SetTooltip(TRANS("run one of installed game modifications"));
  AddChild(gm_pMods);
  gm_pMods->mg_pmgUp = gm_pDemo;
  gm_pMods->mg_pmgDown = gm_pHighScore;
  gm_pMods->mg_pActivatedFunction = NULL;

  gm_pHighScore->SetText(TRANS("HIGH SCORES"));
  gm_pHighScore->mg_bfsFontSize = BFS_LARGE;
  gm_pHighScore->mg_boxOnScreen = BoxBigRow(5.0f);
  gm_pHighScore->SetTooltip(TRANS("view list of top ten best scores"));
  AddChild(gm_pHighScore);
  gm_pHighScore->mg_pmgUp = gm_pMods;
  gm_pHighScore->mg_pmgDown = gm_pOptions;
  gm_pHighScore->mg_pActivatedFunction = NULL;

  gm_pOptions->SetText(TRANS("OPTIONS"));
  gm_pOptions->mg_bfsFontSize = BFS_LARGE;
  gm_pOptions->mg_boxOnScreen = BoxBigRow(6.0f);
  gm_pOptions->SetTooltip(TRANS("adjust video, audio and input options"));
  AddChild(gm_pOptions);
  gm_pOptions->mg_pmgUp = gm_pHighScore;
  gm_pOptions->mg_pmgDown = gm_pQuit;
  gm_pOptions->mg_pActivatedFunction = NULL;

  gm_pQuit->SetText(TRANS("QUIT"));
  gm_pQuit->mg_bfsFontSize = BFS_LARGE;
  gm_pQuit->mg_boxOnScreen = BoxBigRow(7.0f);
  gm_pQuit->SetTooltip(TRANS("exit game immediately"));
  AddChild(gm_pQuit);
  gm_pQuit->mg_pmgUp = gm_pOptions;
  gm_pQuit->mg_pmgDown = gm_pSingle;
  gm_pQuit->mg_pActivatedFunction = NULL;
}

void CMainMenu::StartMenu(void)
{
  gm_pSingle->mg_bEnabled = IsMenuEnabled("Single Player");
  gm_pNetwork->mg_bEnabled = IsMenuEnabled("Network");
  gm_pSplitScreen->mg_bEnabled = IsMenuEnabled("Split Screen");
  gm_pHighScore->mg_bEnabled = IsMenuEnabled("High Score");
  CGameMenu::StartMenu();
}