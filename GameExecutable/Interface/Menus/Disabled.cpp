/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Engine/CurrentVersion.h>
#include "Interface/Common/MenuPrinting.h"
#include "Disabled.h"

CDisabledMenu::CDisabledMenu()
{
  gm_pTitle = new CTitleWidget();
  gm_pButton = new CButtonWidget();
}

void CDisabledMenu::Initialize_t(void)
{
  gm_pTitle->mg_boxOnScreen = BoxTitle();
  AddChild(gm_pTitle);

  gm_pButton->mg_bfsFontSize = BFS_MEDIUM;
  gm_pButton->mg_boxOnScreen = BoxBigRow(0.0f);
  AddChild(gm_pButton);
  gm_pButton->mg_pActivatedFunction = NULL;
}