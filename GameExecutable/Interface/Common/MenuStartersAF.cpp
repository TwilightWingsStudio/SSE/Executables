/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

/* This file contains additional functions called from starters. */

#include "StdH.h"
#include <Core/Objects/ObjectFactory.h>
#include <Engine/Build.h>

#include "MenuManager.h"
#include "MenuStartersAF.h"
#include "MenuStarters.h"
#include "MenuStuff.h"

static CTFileName _fnDemoToPlay;
static CTFileName _fnGameToLoad;

extern CTString sam_strNetworkSettings;

extern CTFileName _fnmModSelected;
extern CTString _strModURLSelected;
extern CTString _strModServerSelected;


BOOL LSLoadSinglePlayer(const CTFileName &fnm)
{
  _pGame->SetStartSplitScreenCfg(CGame::SSC_PLAY1);
  _pGame->ResetStartLocalPlayers();
  _pGame->GetStartLocalPlayers()[0] = _pGame->GetSinglePlayerIndex();
  _pGame->SetNetworkProvider("Local");

  if (_pGame->LoadGame(fnm)) {
    StopMenus();
    _gmRunningGameMode = GM_SINGLE_PLAYER;
  } else {
    _gmRunningGameMode = GM_NONE;
  }

  return TRUE;
}

BOOL LSLoadNetwork(const CTFileName &fnm)
{
  // call local players menu
  _fnGameToLoad = fnm;
  CMenuStarters::SelectPlayersFromNetworkLoad();
  return TRUE;
}

BOOL LSLoadSplitScreen(const CTFileName &fnm)
{
  // call local players menu
  _fnGameToLoad = fnm;
  CMenuStarters::SelectPlayersFromSplitScreenLoad();
  return TRUE;
}

void StartDemoPlay(void)
{
  _pGame->SetStartSplitScreenCfg(CGame::SSC_OBSERVER);

  // play the demo
  _pGame->SetNetworkProvider("Local");

  if (_pGame->StartDemoPlay(_fnDemoToPlay))
  {
    // exit menu and pull up the console
    StopMenus();
    if (_pGame->ConsoleGetState() != CS_OFF) _pGame->ConsoleSetState(CS_TURNINGOFF);
    _gmRunningGameMode = GM_DEMO;
  }
  else {
    _gmRunningGameMode = GM_NONE;
  }
}

extern BOOL LSLoadDemo(const CTFileName &fnm)
{
  // call local players menu
  _fnDemoToPlay = fnm;
  StartDemoPlay();
  return TRUE;
}

BOOL LSLoadPlayerModel(const CTFileName &fnm)
{
  // get base filename
  CTString strBaseName = fnm.FileName();

  // set it for current player
  CPlayerCharacter &pc = _pGame->GetPlayers()[*_pGUIM->gmPlayerProfile.gm_piCurrentPlayer];
  CPlayerSettings *pps = (CPlayerSettings *)pc.pc_aubAppearance;
  memset(pps->ps_achModelFile, 0, sizeof(pps->ps_achModelFile));
  strncpy(pps->ps_achModelFile, strBaseName, sizeof(pps->ps_achModelFile));

  void MenuGoToParent(void);
  MenuGoToParent();
  return TRUE;
}

BOOL LSLoadControls(const CTFileName &fnm)
{
  try {
    ControlsMenuOn();
    _pGame->GetControlsExtra().Load_t(fnm);
    ControlsMenuOff();

  } catch (char *strError) {
    CErrorF("%s", strError);
  }

  void MenuGoToParent(void);
  MenuGoToParent();
  return TRUE;
}

BOOL LSLoadAddon(const CTFileName &fnm)
{
  extern INDEX _iAddonExecState;
  extern CTFileName _fnmAddonToExec;
  _iAddonExecState = 1;
  _fnmAddonToExec = fnm;
  return TRUE;
}

BOOL LSLoadMod(const CTFileName &fnm)
{
  _fnmModSelected = fnm;
  extern void ModConfirm(void);
  ModConfirm();
  return TRUE;
}

BOOL LSLoadCustom(const CTFileName &fnm)
{
  _pGUIM->gmVarMenu.gm_pTitle->SetText(TRANS("ADVANCED OPTIONS"));
  //  LoadStringVar(fnm.NoExt()+".des", mgVarTitle.mg_strText);
  //  mgVarTitle.mg_strText.OnlyFirstLine();
  _pGUIM->gmVarMenu.gm_fnmMenuCFG = fnm;
  _pGUIM->gmVarMenu.gm_pgmParentMenu = &_pGUIM->gmLoadSaveMenu;
  ChangeToMenu(&_pGUIM->gmVarMenu);
  return TRUE;
}

BOOL LSLoadNetSettings(const CTFileName &fnm)
{
  sam_strNetworkSettings = fnm;
  CTString strCmd;
  strCmd.PrintF("include \"%s\"", (const char*)sam_strNetworkSettings);
  _pShell->Execute(strCmd);

  void MenuGoToParent(void);
  MenuGoToParent();
  return TRUE;
}

// same function for saving in singleplay, network and splitscreen
BOOL LSSaveAnyGame(const CTFileName &fnm)
{
  if (_pGame->SaveGame(fnm)) {
    StopMenus();
    return TRUE;
  }
  else {
    return FALSE;
  }
}

BOOL LSSaveDemo(const CTFileName &fnm)
{
  // save the demo
  if (_pGame->StartDemoRec(fnm)) {
    StopMenus();
    return TRUE;
  } else {
    return FALSE;
  }
}

void StartNetworkLoadGame(void)
{
  //  _pGame->SetMenuSplitScreenCfg((enum CGame::SplitScreenCfg) mgSplitScreenCfg.mg_iSelected);
  _pGame->SetStartSplitScreenCfg(_pGame->GetMenuSplitScreenCfg());
  _pGame->SetStartLocalPlayers(_pGame->GetMenuLocalPlayers());
  _pGame->SetNetworkProvider("TCP/IP Server");

  if (_pGame->LoadGame(_fnGameToLoad))
  {
    StopMenus();
    _gmRunningGameMode = GM_NETWORK;
  } else {
    _gmRunningGameMode = GM_NONE;
  }
}

void StartSplitScreenGameLoad(void)
{
  //  _pGame->SetMenuSplitScreenCfg((enum CGame::SplitScreenCfg) mgSplitScreenCfg.mg_iSelected);
  _pGame->SetStartSplitScreenCfg(_pGame->GetMenuSplitScreenCfg());
  _pGame->SetStartLocalPlayers(_pGame->GetMenuLocalPlayers());
  _pGame->SetNetworkProvider("Local");

  if (_pGame->LoadGame(_fnGameToLoad)) {
    StopMenus();
    _gmRunningGameMode = GM_SPLIT_SCREEN;
  }
  else {
    _gmRunningGameMode = GM_NONE;
  }
}

void StartSplitScreenGame(void)
{
  //  _pGame->SetMenuSplitScreenCfg((enum CGame::SplitScreenCfg) mgSplitScreenCfg.mg_iSelected);
  _pGame->SetStartSplitScreenCfg(_pGame->GetMenuSplitScreenCfg());
  _pGame->SetStartLocalPlayers(_pGame->GetMenuLocalPlayers());
  CTFileName fnWorld = _pGame->GetCustomLevel();

  _pGame->SetNetworkProvider("Local");

  if (_pSessionPropertiesFactory == NULL) {
    FatalError("_pSessionPropertiesFactory is invalid!");
  }

  CGameObject* pProperties = _pSessionPropertiesFactory->New();

  _pGame->SetMultiPlayerSession(pProperties);
  if (_pGame->NewGame(fnWorld.FileName(), fnWorld, pProperties))
  {
    StopMenus();
    _gmRunningGameMode = GM_SPLIT_SCREEN;
  } else {
    _gmRunningGameMode = GM_NONE;
  }

  delete pProperties;
}

void StartNetworkGame(void)
{
  //  _pGame->SetMenuSplitScreenCfg((enum CGame::SplitScreenCfg) mgSplitScreenCfg.mg_iSelected);
  _pGame->SetStartSplitScreenCfg(_pGame->GetMenuSplitScreenCfg());
  _pGame->SetStartLocalPlayers(_pGame->GetMenuLocalPlayers());
  CTFileName fnWorld = _pGame->GetCustomLevel();

  _pGame->SetNetworkProvider("TCP/IP Server");

  if (_pSessionPropertiesFactory == NULL) {
    FatalError("_pSessionPropertiesFactory is invalid!");
  }

  CGameObject* pProperties = _pSessionPropertiesFactory->New();

  _pGame->SetMultiPlayerSession(pProperties);

  if (_pGame->NewGame(_pGame->GetSessionName(), fnWorld, pProperties))
  {
    StopMenus();
    _gmRunningGameMode = GM_NETWORK;

    // if starting a dedicated server
    if (_pGame->GetMenuSplitScreenCfg() == CGame::SSC_DEDICATED) {
      // pull down the console
      extern INDEX sam_bToggleConsole;
      sam_bToggleConsole = TRUE;
    }

  } else {
    _gmRunningGameMode = GM_NONE;
  }

  delete pProperties;
}

void JoinNetworkGame(void)
{
  //  _pGame->SetMenuSplitScreenCfg ((enum CGame::SplitScreenCfg) mgSplitScreenCfg.mg_iSelected);
  _pGame->SetStartSplitScreenCfg(_pGame->GetMenuSplitScreenCfg());
  _pGame->SetStartLocalPlayers(_pGame->GetMenuLocalPlayers());
  _pGame->SetNetworkProvider("TCP/IP Client");

  if (_pGame->JoinGame(CNetworkSession(_pGame->GetJoinAddress())))
  {
    StopMenus();
    _gmRunningGameMode = GM_NETWORK;
  } else {
    if (_pNetwork->ga_strRequiredMod != "") {
      extern CTFileName _fnmModToLoad;
      extern CTString _strModServerJoin;
      char strModName[256] = { 0 };
      char strModURL[256] = { 0 };
      _pNetwork->ga_strRequiredMod.ScanF("%250[^\\]\\%s", &strModName, &strModURL);
      _fnmModSelected = CTString(strModName);
      _strModURLSelected = strModURL;
      if (_strModURLSelected = "") {
        _strModURLSelected = "http://www.croteam.com/mods/Old";
      }
      _strModServerSelected.PrintF("%s:%s", _pGame->GetJoinAddress(), _pShell->GetValue("net_iPort"));
      extern void ModConnectConfirm(void);
      ModConnectConfirm();
    }
    _gmRunningGameMode = GM_NONE;
  }
}

void SetQuickLoadNotes(void)
{
  CLoadSaveMenu &gmCurrent = _pGUIM->gmLoadSaveMenu;

  if (_pShell->GetINDEX("gam_iQuickSaveSlots") <= 8) {
    gmCurrent.gm_pNotes->SetText(TRANS(
      "In-game QuickSave shortcuts:\n"
      "F6 - save a new QuickSave\n"
      "F9 - load the last QuickSave\n"));
  } else {
    gmCurrent.gm_pNotes->SetText("");
  }
}