/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_MENU_STARTERS_H
#define SE_INCL_MENU_STARTERS_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

class CMenuStarters
{
  public:
    static void VideoOptions(void);
    static void AudioOptions(void);
    static void Network(void);
    static void NetworkJoin(void);
    static void NetworkStart(void);
    static void NetworkOpen(void);
    static void SplitScreen(void);
    static void SplitStart(void);
    static void SinglePlayerNewCustom(void);
    static void SinglePlayerNew(void);
    static void SinglePlayerQuickLoad(void);
    static void SinglePlayerLoad(void);
    static void SinglePlayerSave(void);
    static void DemoLoad(void);
    static void DemoSave(void);
    static void NetworkQuickLoad(void);
    static void NetworkLoad(void);
    static void NetworkSave(void);
    static void SplitScreenQuickLoad(void);
    static void SplitScreenLoad(void);
    static void SplitScreenSave(void);
    static void VarGameOptions(void);
    static void SinglePlayerGameOptions(void);
    static void GameOptionsFromNetwork(void);
    static void GameOptionsFromSplitScreen(void);
    static void RenderingOptions(void);
    static void CustomizeKeyboard(void);
    static void CustomizeAxis(void);
    static void Options(void);
    static void CurrentLoad();
    static void CurrentSave();
    static void CurrentQuickLoad();
    static void ChangePlayerFromOptions(void);
    static void ChangePlayerFromSinglePlayer(void);
    static void ControlsFromPlayer(void);
    static void ControlsFromOptions(void);
    static void SelectLevelFromSingle(void);
    static void HighScore(void);
    static void SelectPlayersFromSplit(void);
    static void SelectPlayersFromNetwork(void);
    static void SelectPlayersFromOpen(void);
    static void SelectPlayersFromServers(void);
    static void SelectServerLAN(void);
    static void SelectServerNET(void);
    static void SelectLevelFromSplit(void);
    static void SelectLevelFromNetwork(void);
    static void SelectPlayersFromSplitScreen(void);
    static void SelectPlayersFromNetworkLoad(void);
    static void SelectPlayersFromSplitScreenLoad(void);
    static void PlayerModelLoad(void);
    static void ControlsLoad(void);
    static void CustomLoad(void);
    static void AddonsLoad(void);
    static void ModsLoad(void);
    static void NetworkSettings(void);
    static void SinglePlayer(void);
    static void DisabledFunction(void);
};

#endif  /* include-once check. */