/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_MENUMANAGER_H
#define SE_INCL_MENUMANAGER_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include "Interface/Menus/AudioOptions.h"
#include "Interface/Menus/Confirm.h"
#include "Interface/Menus/Controls.h"
#include "Interface/Menus/CustomizeAxis.h"
#include "Interface/Menus/CustomizeKeyboard.h"
#include "Interface/Menus/Credits.h"
#include "Interface/Menus/Disabled.h"
#include "Interface/Menus/HighScore.h"
#include "Interface/Menus/InGame.h"
#include "Interface/Menus/Levels.h"
#include "Interface/Menus/Main.h"
#include "Interface/Menus/Network.h"
#include "Interface/Menus/NetworkJoin.h"
#include "Interface/Menus/NetworkOpen.h"
#include "Interface/Menus/NetworkStart.h"
#include "Interface/Menus/Options.h"
#include "Interface/Menus/RenderingOptions.h"
#include "Interface/Menus/Servers.h"
#include "Interface/Menus/SinglePlayer.h"
#include "Interface/Menus/SinglePlayerNew.h"
#include "Interface/Menus/SplitScreen.h"
#include "Interface/Menus/SplitStart.h"
#include "Interface/Menus/Var.h"
#include "Interface/Menus/VideoOptions.h"

class CMenuManager
{
  public:
    CConfirmMenu gmConfirmMenu;
    CMainMenu gmMainMenu;
    CInGameMenu gmInGameMenu;
    CSinglePlayerMenu gmSinglePlayerMenu;
    CSinglePlayerNewMenu gmSinglePlayerNewMenu;
    CDisabledMenu gmDisabledFunction;
    CLevelsMenu gmLevelsMenu;
    CVarMenu gmVarMenu;
    CPlayerProfileMenu gmPlayerProfile;
    CControlsMenu gmControls;
    CLoadSaveMenu gmLoadSaveMenu;
    CHighScoreMenu gmHighScoreMenu;
    CCustomizeKeyboardMenu gmCustomizeKeyboardMenu;
    CServersMenu gmServersMenu;
    CCustomizeAxisMenu gmCustomizeAxisMenu;
    COptionsMenu gmOptionsMenu;
    CVideoOptionsMenu gmVideoOptionsMenu;
    CAudioOptionsMenu gmAudioOptionsMenu;
    CNetworkMenu gmNetworkMenu;
    CNetworkJoinMenu gmNetworkJoinMenu;
    CNetworkStartMenu gmNetworkStartMenu;
    CNetworkOpenMenu gmNetworkOpenMenu;
    CSplitScreenMenu gmSplitScreenMenu;
    CSplitStartMenu gmSplitStartMenu;
    CSelectPlayersMenu gmSelectPlayersMenu;
};

extern CMenuManager *_pGUIM; // TODO: Make singleton!


#endif  /* include-once check. */