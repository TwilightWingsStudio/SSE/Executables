/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include <Core/Objects/ObjectFactory.h>
#include <Engine/Build.h>

#include "MenuManager.h"
#include "MenuStarters.h"
#include "MenuStuff.h"
#include "Interface/Widgets/Widget.h"
#include "LevelInfo.h"
#include "VarList.h"

// Screen resolution lists and window modes
#include "ScreenResolutions.h"
#include "WindowModes.h"

ENGINE_API extern INDEX snd_iFormat;
extern BOOL _bMouseUsedLast;

extern CWidget *_pmgLastActivatedGadget;
extern CWidget *_pmgUnderCursor;

static INDEX         _ctAdapters = 0;
static CTString     * _astrAdapterTexts = NULL;
static INDEX         _ctResolutions = 0;
static CTString     * _astrResolutionTexts = NULL;
static CDisplayMode *_admResolutionModes = NULL;

#define VOLUME_STEPS  50


// make description for a given resolution
static CTString GetResolutionDescription(CDisplayMode &dm)
{
  CTString str;

  // if dual head
  if (dm.IsDualHead()) {
    str.PrintF(TRANS("%dx%d double"), dm.dm_pixSizeI / 2, dm.dm_pixSizeJ);

  // if widescreen
  } else if (dm.IsWideScreen()) {
    str.PrintF(TRANS("%dx%d wide"), dm.dm_pixSizeI, dm.dm_pixSizeJ);

  // otherwise it is normal
  } else {
    str.PrintF("%dx%d", dm.dm_pixSizeI, dm.dm_pixSizeJ);
  }

  // [SSE] Resolution matches the screen
  if (PIX2D(dm.dm_pixSizeI, dm.dm_pixSizeJ) == _vpixScreenRes) {
    str += TRANS(" (Native)");
  }

  return str;
}

// make description for a given resolution
static void SetResolutionInList(INDEX iRes, PIX2D vpixSize)
{
  ASSERT(iRes >= 0 && iRes < _ctResolutions);

  CTString &str = _astrResolutionTexts[iRes];
  CDisplayMode &dm = _admResolutionModes[iRes];
  dm.dm_pixSizeI = vpixSize(1);
  dm.dm_pixSizeJ = vpixSize(2);
  str = GetResolutionDescription(dm);
}

static void ResolutionToSize(INDEX iRes, PIX2D &vpixSize)
{
  ASSERT(iRes >= 0 && iRes < _ctResolutions);

  CDisplayMode &dm = _admResolutionModes[iRes];
  vpixSize = PIX2D(dm.dm_pixSizeI, dm.dm_pixSizeJ);
}

static void SizeToResolution(PIX2D vpixSize, INDEX &iRes)
{
  for (iRes = 0; iRes < _ctResolutions; iRes++) {
    CDisplayMode &dm = _admResolutionModes[iRes];
    if (dm.dm_pixSizeI == vpixSize(1) && dm.dm_pixSizeJ == vpixSize(2)) {
      return;
    }
  }
  // if none was found, search for default
  for (iRes = 0; iRes < _ctResolutions; iRes++) {
    CDisplayMode &dm = _admResolutionModes[iRes];
    if (dm.dm_pixSizeI == 640 && dm.dm_pixSizeJ == 480) {
      return;
    }
  }
  // if still none found
  ASSERT(FALSE); // this should never happen
  // return first one
  iRes = 0;
}

// Set all resolutions of some aspect ratio in list
static void SetAspectRatioResolutions(const CAspectRatio &arAspectRatio, INDEX &ctResCounter) {
  const INDEX ctResolutions = arAspectRatio.Count();

  for (INDEX iRes = 0; iRes < ctResolutions; iRes++) {
    const PIX2D &vpix = arAspectRatio[iRes];

    if (vpix(1) > _vpixScreenRes(1) || vpix(2) > _vpixScreenRes(2)) {
      continue; // Skip resolutions bigger than the screen
    }

    SetResolutionInList(ctResCounter++, vpix);
  }
};

// ------------------------ CConfirmMenu implementation
extern CTFileName _fnmModToLoad;
extern CTString _strModServerJoin;

CTFileName _fnmModSelected;
CTString _strModURLSelected;
CTString _strModServerSelected;

static void ExitGame(void)
{
  _bRunning = FALSE;
  _bQuitScreen = TRUE;
}

static void ExitConfirm(void)
{
  CConfirmMenu &gmCurrent = _pGUIM->gmConfirmMenu;

  gmCurrent._pConfimedYes = &ExitGame;
  gmCurrent._pConfimedNo = NULL;
  gmCurrent.gm_pConfirmLabel->SetText(TRANS("ARE YOU SERIOUS?"));
  gmCurrent.gm_pgmParentMenu = pgmCurrentMenu;
  gmCurrent.BeLarge();
  ChangeToMenu(&gmCurrent);
}

static void StopCurrentGame(void)
{
  _pGame->StopGame();
  _gmRunningGameMode = GM_NONE;
  StopMenus(TRUE);
  StartMenus("");
}

static void StopConfirm(void)
{
  CConfirmMenu &gmCurrent = _pGUIM->gmConfirmMenu;

  gmCurrent._pConfimedYes = &StopCurrentGame;
  gmCurrent._pConfimedNo = NULL;
  gmCurrent.gm_pConfirmLabel->SetText(TRANS("ARE YOU SERIOUS?"));
  gmCurrent.gm_pgmParentMenu = pgmCurrentMenu;
  gmCurrent.BeLarge();
  ChangeToMenu(&gmCurrent);
}

static void ModLoadYes(void)
{
  _fnmModToLoad = _fnmModSelected;
}

static void ModConnect(void)
{
  _fnmModToLoad = _fnmModSelected;
  _strModServerJoin = _strModServerSelected;
}

extern void ModConnectConfirm(void)
{
  CConfirmMenu &gmCurrent = _pGUIM->gmConfirmMenu;

  if (_fnmModSelected == " ") {
    _fnmModSelected = CTString("SeriousSam");
  }

  CTFileName fnmModPath = "Mods\\" + _fnmModSelected + "\\";
  if (!FileExists(fnmModPath + "BaseWriteInclude.lst")
    && !FileExists(fnmModPath + "BaseWriteExclude.lst")
    && !FileExists(fnmModPath + "BaseBrowseInclude.lst")
    && !FileExists(fnmModPath + "BaseBrowseExclude.lst")) {
    extern void ModNotInstalled(void);
    ModNotInstalled();
    return;
  }

  CWarningF(TRANS("Server is running a different MOD (%s).\nYou need to reload to connect.\n"), _fnmModSelected);
  gmCurrent._pConfimedYes = &ModConnect;
  gmCurrent._pConfimedNo = NULL;
  gmCurrent.gm_pConfirmLabel->SetText(TRANS("CHANGE THE MOD?"));
  gmCurrent.gm_pgmParentMenu = pgmCurrentMenu;
  gmCurrent.BeLarge();
  ChangeToMenu(&gmCurrent);
}

void SaveConfirm(void)
{
  CConfirmMenu &gmCurrent = _pGUIM->gmConfirmMenu;

  extern void OnFileSaveOK(void);
  gmCurrent._pConfimedYes = &OnFileSaveOK;
  gmCurrent._pConfimedNo = NULL;
  gmCurrent.gm_pConfirmLabel->SetText(TRANS("OVERWRITE?"));
  gmCurrent.gm_pgmParentMenu = pgmCurrentMenu;
  gmCurrent.BeLarge();
  ChangeToMenu(&gmCurrent);
}

void ExitAndSpawnExplorer(void)
{
  _bRunning = FALSE;
  _bQuitScreen = FALSE;
  extern CTString _strURLToVisit;
  _strURLToVisit = _strModURLSelected;
}

void ModNotInstalled(void)
{
  CConfirmMenu &gmCurrent = _pGUIM->gmConfirmMenu;

  gmCurrent._pConfimedYes = &ExitAndSpawnExplorer;
  gmCurrent._pConfimedNo = NULL;
  gmCurrent.gm_pConfirmLabel->mg_strText.PrintF(
    TRANS("You don't have MOD '%s' installed.\nDo you want to visit its web site?"), (const char*)_fnmModSelected);
  gmCurrent.gm_pgmParentMenu = pgmCurrentMenu;
  gmCurrent.BeSmall();
  ChangeToMenu(&gmCurrent);
}

extern void ModConfirm(void)
{
  CConfirmMenu &gmCurrent = _pGUIM->gmConfirmMenu;

  gmCurrent._pConfimedYes = &ModLoadYes;
  gmCurrent._pConfimedNo = NULL;
  gmCurrent.gm_pConfirmLabel->SetText(TRANS("LOAD THIS MOD?"));
  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmLoadSaveMenu;
  gmCurrent.BeLarge();
  ChangeToMenu(&gmCurrent);
}

static void RevertVideoSettings(void);

void VideoConfirm(void)
{
  CConfirmMenu &gmCurrent = _pGUIM->gmConfirmMenu;

  // FIXUP: keyboard focus lost when going from full screen to window mode
  // due to WM_MOUSEMOVE being sent
  _bMouseUsedLast = FALSE;
  _pmgUnderCursor = gmCurrent.gm_pmgSelectedByDefault;

  gmCurrent._pConfimedYes = NULL;
  gmCurrent._pConfimedNo = RevertVideoSettings;

  gmCurrent.gm_pConfirmLabel->SetText(TRANS("KEEP THIS SETTING?"));
  gmCurrent.gm_pgmParentMenu = pgmCurrentMenu;
  gmCurrent.BeLarge();
  ChangeToMenu(&gmCurrent);
}

static void ConfirmYes(void)
{
  CConfirmMenu &gmCurrent = _pGUIM->gmConfirmMenu;

  if (gmCurrent._pConfimedYes != NULL) {
    gmCurrent._pConfimedYes();
  }
  void MenuGoToParent(void);
  MenuGoToParent();
}

static void ConfirmNo(void)
{
  CConfirmMenu &gmCurrent = _pGUIM->gmConfirmMenu;

  if (gmCurrent._pConfimedNo != NULL) {
    gmCurrent._pConfimedNo();
  }
  void MenuGoToParent(void);
  MenuGoToParent();
}

void InitActionsForConfirmMenu() {
  CConfirmMenu &gmCurrent = _pGUIM->gmConfirmMenu;

  gmCurrent.gm_pConfirmYes->mg_pActivatedFunction = &ConfirmYes;
  gmCurrent.gm_pConfirmNo->mg_pActivatedFunction = &ConfirmNo;
}

// ------------------------ CMainMenu implementation
void InitActionsForMainMenu() {
  CMainMenu &gmCurrent = _pGUIM->gmMainMenu;

  gmCurrent.gm_pSingle->mg_pActivatedFunction = &CMenuStarters::SinglePlayer;
  gmCurrent.gm_pNetwork->mg_pActivatedFunction = &CMenuStarters::Network;
  gmCurrent.gm_pSplitScreen->mg_pActivatedFunction = &CMenuStarters::SplitScreen;
  gmCurrent.gm_pDemo->mg_pActivatedFunction = &CMenuStarters::DemoLoad;
  gmCurrent.gm_pMods->mg_pActivatedFunction = &CMenuStarters::ModsLoad;
  gmCurrent.gm_pHighScore->mg_pActivatedFunction = &CMenuStarters::HighScore;
  gmCurrent.gm_pOptions->mg_pActivatedFunction = &CMenuStarters::Options;
  gmCurrent.gm_pQuit->mg_pActivatedFunction = &ExitConfirm;
}

// ------------------------ CInGameMenu implementation
// start load/save menus depending on type of game running
static void QuickSaveFromMenu()
{
  _pShell->SetINDEX("gam_bQuickSave", 2); // force save with reporting
  StopMenus(TRUE);
}

static void StopRecordingDemo(void)
{
  _pNetwork->StopDemoRec();
  void SetDemoStartStopRecText(void);
  SetDemoStartStopRecText();
}

void InitActionsForInGameMenu()
{
  CInGameMenu &gmCurrent = _pGUIM->gmInGameMenu;

  gmCurrent.gm_pQuickLoad->mg_pActivatedFunction = &CMenuStarters::CurrentQuickLoad;
  gmCurrent.gm_pQuickSave->mg_pActivatedFunction = &QuickSaveFromMenu;
  gmCurrent.gm_pLoad->mg_pActivatedFunction = &CMenuStarters::CurrentLoad;
  gmCurrent.gm_pSave->mg_pActivatedFunction = &CMenuStarters::CurrentSave;
  gmCurrent.gm_pHighScore->mg_pActivatedFunction = &CMenuStarters::HighScore;
  gmCurrent.gm_pOptions->mg_pActivatedFunction = &CMenuStarters::Options;
  gmCurrent.gm_pStop->mg_pActivatedFunction = &StopConfirm;
  gmCurrent.gm_pQuit->mg_pActivatedFunction = &ExitConfirm;
}

extern void SetDemoStartStopRecText(void)
{
  CInGameMenu &gmCurrent = _pGUIM->gmInGameMenu;

  if (_pNetwork->IsRecordingDemo())
  {
    gmCurrent.gm_pDemoRec->SetText(TRANS("STOP RECORDING"));
    gmCurrent.gm_pDemoRec->SetTooltip(TRANS("stop current recording"));
    gmCurrent.gm_pDemoRec->mg_pActivatedFunction = &StopRecordingDemo;
  } else {
    gmCurrent.gm_pDemoRec->SetText(TRANS("RECORD DEMO"));
    gmCurrent.gm_pDemoRec->SetTooltip(TRANS("start recording current game"));
    gmCurrent.gm_pDemoRec->mg_pActivatedFunction = &CMenuStarters::DemoSave;
  }
}

// ------------------------ CSinglePlayerMenu implementation
extern CTString sam_strTechTestLevel;
extern CTString sam_strTrainingLevel;

static void StartSinglePlayerGame_Normal(void);
static void StartTechTest(void)
{
  _pGUIM->gmSinglePlayerNewMenu.gm_pgmParentMenu = &_pGUIM->gmSinglePlayerMenu;
  _pGame->SetCustomLevel(sam_strTechTestLevel);
  StartSinglePlayerGame_Normal();
}

static void StartTraining(void)
{
  _pGUIM->gmSinglePlayerNewMenu.gm_pgmParentMenu = &_pGUIM->gmSinglePlayerMenu;
  _pGame->SetCustomLevel(sam_strTrainingLevel);
  ChangeToMenu(&_pGUIM->gmSinglePlayerNewMenu);
}

void InitActionsForSinglePlayerMenu()
{
  CSinglePlayerMenu &gmCurrent = _pGUIM->gmSinglePlayerMenu;

  gmCurrent.gm_pNewGame->mg_pActivatedFunction = &CMenuStarters::SinglePlayerNew;
  gmCurrent.gm_pCustom->mg_pActivatedFunction = &CMenuStarters::SelectLevelFromSingle;
  gmCurrent.gm_pQuickLoad->mg_pActivatedFunction = &CMenuStarters::SinglePlayerQuickLoad;
  gmCurrent.gm_pLoad->mg_pActivatedFunction = &CMenuStarters::SinglePlayerLoad;
  gmCurrent.gm_pTraining->mg_pActivatedFunction = &StartTraining;
  gmCurrent.gm_pTechTest->mg_pActivatedFunction = &StartTechTest;
  gmCurrent.gm_pPlayersAndControls->mg_pActivatedFunction = &CMenuStarters::ChangePlayerFromSinglePlayer;
  gmCurrent.gm_pOptions->mg_pActivatedFunction = &CMenuStarters::SinglePlayerGameOptions;
}

// ------------------------ CSinglePlayerNewMenu implementation
void StartSinglePlayerGame(void)
{
  _pGame->SetStartSplitScreenCfg(CGame::SSC_PLAY1);
  _pGame->ResetStartLocalPlayers();
  _pGame->GetStartLocalPlayers()[0] = _pGame->GetSinglePlayerIndex();

  _pGame->SetNetworkProvider("Local");

  if (_pSessionPropertiesFactory == NULL) {
    FatalError("_pSessionPropertiesFactory is invalid!");
  }

  CGameObject* pProperties = _pSessionPropertiesFactory->New();

  _pGame->SetSinglePlayerSession(pProperties);

  // TODO: Check it out!
  if (_pGame->NewGame(_pGame->GetCustomLevel(), _pGame->GetCustomLevel(), pProperties))
  {
    StopMenus();
    _gmRunningGameMode = GM_SINGLE_PLAYER;
  } else {
    _gmRunningGameMode = GM_NONE;
  }

  delete pProperties;
}

static void StartSinglePlayerGame_Tourist(void)
{
  _pShell->SetINDEX("gam_iStartDifficulty", CSessionProperties::GD_TOURIST);
  _pShell->SetINDEX("gam_iStartMode", CSessionProperties::GM_COOPERATIVE);
  StartSinglePlayerGame();
}

static void StartSinglePlayerGame_Easy(void)
{
  _pShell->SetINDEX("gam_iStartDifficulty", CSessionProperties::GD_EASY);
  _pShell->SetINDEX("gam_iStartMode", CSessionProperties::GM_COOPERATIVE);
  StartSinglePlayerGame();
}

static void StartSinglePlayerGame_Normal(void)
{
  _pShell->SetINDEX("gam_iStartDifficulty", CSessionProperties::GD_NORMAL);
  _pShell->SetINDEX("gam_iStartMode", CSessionProperties::GM_COOPERATIVE);
  StartSinglePlayerGame();
}

static void StartSinglePlayerGame_Hard(void)
{
  _pShell->SetINDEX("gam_iStartDifficulty", CSessionProperties::GD_HARD);
  _pShell->SetINDEX("gam_iStartMode", CSessionProperties::GM_COOPERATIVE);
  StartSinglePlayerGame();
}

static void StartSinglePlayerGame_Serious(void)
{
  _pShell->SetINDEX("gam_iStartDifficulty", CSessionProperties::GD_EXTREME);
  _pShell->SetINDEX("gam_iStartMode", CSessionProperties::GM_COOPERATIVE);
  StartSinglePlayerGame();
}

static void StartSinglePlayerGame_Mental(void)
{
  _pShell->SetINDEX("gam_iStartDifficulty", CSessionProperties::GD_EXTREME + 1);
  _pShell->SetINDEX("gam_iStartMode", CSessionProperties::GM_COOPERATIVE);
  StartSinglePlayerGame();
}

void InitActionsForSinglePlayerNewMenu() {
  CSinglePlayerNewMenu &gmCurrent = _pGUIM->gmSinglePlayerNewMenu;

  gmCurrent.gm_pTourist->mg_pActivatedFunction = &StartSinglePlayerGame_Tourist;
  gmCurrent.gm_pEasy->mg_pActivatedFunction = &StartSinglePlayerGame_Easy;
  gmCurrent.gm_pMedium->mg_pActivatedFunction = &StartSinglePlayerGame_Normal;
  gmCurrent.gm_pHard->mg_pActivatedFunction = &StartSinglePlayerGame_Hard;
  gmCurrent.gm_pSerious->mg_pActivatedFunction = &StartSinglePlayerGame_Serious;
  gmCurrent.gm_pMental->mg_pActivatedFunction = &StartSinglePlayerGame_Mental;
}

// ------------------------ CPlayerProfileMenu implementation
static void ChangeCrosshair(INDEX iNew)
{
  INDEX iPlayer = *_pGUIM->gmPlayerProfile.gm_piCurrentPlayer;
  CPlayerCharacter &pc = _pGame->GetPlayers()[iPlayer];
  CPlayerSettings *pps = (CPlayerSettings *)pc.pc_aubAppearance;
  pps->ps_iCrossHairType = iNew - 1;
}

static void ChangeWeaponSelect(INDEX iNew)
{
  INDEX iPlayer = *_pGUIM->gmPlayerProfile.gm_piCurrentPlayer;
  CPlayerCharacter &pc = _pGame->GetPlayers()[iPlayer];
  CPlayerSettings *pps = (CPlayerSettings *)pc.pc_aubAppearance;
  pps->ps_iWeaponAutoSelect = iNew;
}

static void ChangeWeaponHide(INDEX iNew)
{
  INDEX iPlayer = *_pGUIM->gmPlayerProfile.gm_piCurrentPlayer;
  CPlayerCharacter &pc = _pGame->GetPlayers()[iPlayer];
  CPlayerSettings *pps = (CPlayerSettings *)pc.pc_aubAppearance;

  if (iNew) {
    pps->ps_ulFlags |= PSF_HIDEWEAPON;
  } else {
    pps->ps_ulFlags &= ~PSF_HIDEWEAPON;
  }
}

static void Change3rdPerson(INDEX iNew)
{
  INDEX iPlayer = *_pGUIM->gmPlayerProfile.gm_piCurrentPlayer;
  CPlayerCharacter &pc = _pGame->GetPlayers()[iPlayer];
  CPlayerSettings *pps = (CPlayerSettings *)pc.pc_aubAppearance;

  if (iNew) {
    pps->ps_ulFlags |= PSF_PREFER3RDPERSON;
  } else {
    pps->ps_ulFlags &= ~PSF_PREFER3RDPERSON;
  }
}

static void ChangeQuotes(INDEX iNew)
{
  INDEX iPlayer = *_pGUIM->gmPlayerProfile.gm_piCurrentPlayer;
  CPlayerCharacter &pc = _pGame->GetPlayers()[iPlayer];
  CPlayerSettings *pps = (CPlayerSettings *)pc.pc_aubAppearance;

  if (iNew) {
    pps->ps_ulFlags &= ~PSF_NOQUOTES;
  } else {
    pps->ps_ulFlags |= PSF_NOQUOTES;
  }
}

static void ChangeAutoSave(INDEX iNew)
{
  INDEX iPlayer = *_pGUIM->gmPlayerProfile.gm_piCurrentPlayer;
  CPlayerCharacter &pc = _pGame->GetPlayers()[iPlayer];
  CPlayerSettings *pps = (CPlayerSettings *)pc.pc_aubAppearance;

  if (iNew) {
    pps->ps_ulFlags |= PSF_AUTOSAVE;
  } else {
    pps->ps_ulFlags &= ~PSF_AUTOSAVE;
  }
}

static void ChangeCompDoubleClick(INDEX iNew)
{
  INDEX iPlayer = *_pGUIM->gmPlayerProfile.gm_piCurrentPlayer;
  CPlayerCharacter &pc = _pGame->GetPlayers()[iPlayer];
  CPlayerSettings *pps = (CPlayerSettings *)pc.pc_aubAppearance;

  if (iNew) {
    pps->ps_ulFlags &= ~PSF_COMPSINGLECLICK;
  } else {
    pps->ps_ulFlags |= PSF_COMPSINGLECLICK;
  }
}

static void ChangeViewBobbing(INDEX iNew)
{
  INDEX iPlayer = *_pGUIM->gmPlayerProfile.gm_piCurrentPlayer;
  CPlayerCharacter &pc = _pGame->GetPlayers()[iPlayer];
  CPlayerSettings *pps = (CPlayerSettings *)pc.pc_aubAppearance;

  if (iNew) {
    pps->ps_ulFlags &= ~PSF_NOBOBBING;
  } else {
    pps->ps_ulFlags |= PSF_NOBOBBING;
  }
}

static void ChangeSharpTurning(INDEX iNew)
{
  INDEX iPlayer = *_pGUIM->gmPlayerProfile.gm_piCurrentPlayer;
  CPlayerCharacter &pc = _pGame->GetPlayers()[iPlayer];
  CPlayerSettings *pps = (CPlayerSettings *)pc.pc_aubAppearance;

  if (iNew) {
    pps->ps_ulFlags |= PSF_SHARPTURNING;
  } else {
    pps->ps_ulFlags &= ~PSF_SHARPTURNING;
  }
}

extern void PPOnPlayerSelect(void)
{
  ASSERT(_pmgLastActivatedGadget != NULL);
  if (_pmgLastActivatedGadget->mg_bEnabled) {
    _pGUIM->gmPlayerProfile.SelectPlayer(((CButtonWidget *)_pmgLastActivatedGadget)->mg_iIndex);
  }
}

void InitActionsForPlayerProfileMenu()
{
  CPlayerProfileMenu &gmCurrent = _pGUIM->gmPlayerProfile;

  gmCurrent.gm_pCrosshair->mg_pOnTriggerChange = ChangeCrosshair;
  gmCurrent.gm_pWeaponSelect->mg_pOnTriggerChange = ChangeWeaponSelect;
  gmCurrent.gm_pWeaponHide->mg_pOnTriggerChange = ChangeWeaponHide;
  gmCurrent.gm_p3rdPerson->mg_pOnTriggerChange = Change3rdPerson;
  gmCurrent.gm_pQuotes->mg_pOnTriggerChange = ChangeQuotes;
  gmCurrent.gm_pAutoSave->mg_pOnTriggerChange = ChangeAutoSave;
  gmCurrent.gm_pCompDoubleClick->mg_pOnTriggerChange = ChangeCompDoubleClick;
  gmCurrent.gm_pSharpTurning->mg_pOnTriggerChange = ChangeSharpTurning;
  gmCurrent.gm_pViewBobbing->mg_pOnTriggerChange = ChangeViewBobbing;
  gmCurrent.gm_pCustomizeControls->mg_pActivatedFunction = &CMenuStarters::ControlsFromPlayer;
  gmCurrent.gm_pModel->mg_pActivatedFunction = &CMenuStarters::PlayerModelLoad;
}

// ------------------------ CControlsMenu implementation
void InitActionsForControlsMenu()
{
  CControlsMenu &gmCurrent = _pGUIM->gmControls;

  gmCurrent.gm_pButtons->mg_pActivatedFunction = &CMenuStarters::CustomizeKeyboard;
  gmCurrent.gm_pAdvanced->mg_pActivatedFunction = &CMenuStarters::CustomizeAxis;
  gmCurrent.gm_pPredefined->mg_pActivatedFunction = &CMenuStarters::ControlsLoad;
}

// ------------------------ CCustomizeAxisMenu implementation
void PreChangeAxis(INDEX iDummy)
{
  _pGUIM->gmCustomizeAxisMenu.ApplyActionSettings();
}

void PostChangeAxis(INDEX iDummy)
{
  _pGUIM->gmCustomizeAxisMenu.ObtainActionSettings();
}

void InitActionsForCustomizeAxisMenu()
{
  CCustomizeAxisMenu &gmCurrent = _pGUIM->gmCustomizeAxisMenu;

  gmCurrent.gm_pActionTrigger->mg_pPreTriggerChange = PreChangeAxis;
  gmCurrent.gm_pActionTrigger->mg_pOnTriggerChange = PostChangeAxis;
}

// ------------------------ COptionsMenu implementation
void InitActionsForOptionsMenu()
{
  COptionsMenu &gmCurrent = _pGUIM->gmOptionsMenu;

  gmCurrent.gm_pVideoOptions->mg_pActivatedFunction = &CMenuStarters::VideoOptions;
  gmCurrent.gm_pAudioOptions->mg_pActivatedFunction = &CMenuStarters::AudioOptions;
  gmCurrent.gm_pPlayerProfileOptions->mg_pActivatedFunction = &CMenuStarters::ChangePlayerFromOptions;
  gmCurrent.gm_pNetworkOptions->mg_pActivatedFunction = &CMenuStarters::NetworkSettings;
  gmCurrent.gm_pCustomOptions->mg_pActivatedFunction = &CMenuStarters::CustomLoad;
  gmCurrent.gm_pAddonOptions->mg_pActivatedFunction = &CMenuStarters::AddonsLoad;
}

// ------------------------ CVideoOptionsMenu implementation
static INDEX sam_old_iWindowMode; // [SSE] Different window modes
static INDEX sam_old_iScreenSizeI;
static INDEX sam_old_iScreenSizeJ;
static INDEX sam_old_iDisplayDepth;
static INDEX sam_old_iDisplayAdapter;
static INDEX sam_old_iGfxAPI;
static INDEX sam_old_iVideoSetup;  // 0==speed, 1==normal, 2==quality, 3==custom

static void FillResolutionsList(void)
{
  CVideoOptionsMenu &gmCurrent = _pGUIM->gmVideoOptionsMenu;

  // free resolutions
  if (_astrResolutionTexts != NULL) {
    delete[] _astrResolutionTexts;
  }
  if (_admResolutionModes != NULL) {
    delete[] _admResolutionModes;
  }
  _ctResolutions = 0;

  // [SSE] Select current aspect ratio
  const INDEX iAspectRatio = gmCurrent.gm_pAspectRatiosTrigger->mg_iSelected;
  const CAspectRatio &ar = *_aAspectRatios[iAspectRatio];

  // If 4:3 in borderless or fullscreen
  if (iAspectRatio == 0 && gmCurrent.gm_pWindowModeTrigger->mg_iSelected != E_WM_WINDOWED) {
    // Get resolutions from the engine
    INDEX ctEngineRes = 0;

    CDisplayMode *pdm = _pGfx->EnumDisplayModes(ctEngineRes,
      SwitchToAPI(gmCurrent.gm_pDisplayAPITrigger->mg_iSelected), gmCurrent.gm_pDisplayAdaptersTrigger->mg_iSelected);

    _astrResolutionTexts = new CTString[ctEngineRes];
    _admResolutionModes = new CDisplayMode[ctEngineRes];

    // Add all engine resolutions to the list
    for (INDEX iRes = 0; iRes < ctEngineRes; iRes++) {
      SetResolutionInList(iRes, PIX2D(pdm[iRes].dm_pixSizeI, pdm[iRes].dm_pixSizeJ));
    }

    // Remember current amount
    _ctResolutions = ctEngineRes;
    
  // If any other aspect ratio or windowed mode
  } else {
    // Amount of resolutions under this aspect ratio
    _ctResolutions = ar.Count();

    _astrResolutionTexts = new CTString[_ctResolutions];
    _admResolutionModes = new CDisplayMode[_ctResolutions];

    // Add all resolutions from the selected aspect ratio
    INDEX ctRes = 0;
    SetAspectRatioResolutions(ar, ctRes);

    _ctResolutions = ctRes;
  }

  gmCurrent.gm_pResolutionsTrigger->mg_astrTexts = _astrResolutionTexts;
  gmCurrent.gm_pResolutionsTrigger->mg_ctTexts = _ctResolutions;
}

static void FillAdaptersList(void)
{
  CVideoOptionsMenu &gmCurrent = _pGUIM->gmVideoOptionsMenu;

  if (_astrAdapterTexts != NULL) {
    delete[] _astrAdapterTexts;
  }

  _ctAdapters = 0;

  INDEX iApi = SwitchToAPI(gmCurrent.gm_pDisplayAPITrigger->mg_iSelected);
  _ctAdapters = _pGfx->gl_gaAPI[iApi].ga_ctAdapters;
  _astrAdapterTexts = new CTString[_ctAdapters];
  for (INDEX iAdapter = 0; iAdapter<_ctAdapters; iAdapter++) {
    _astrAdapterTexts[iAdapter] = _pGfx->gl_gaAPI[iApi].ga_adaAdapter[iAdapter].da_strRenderer;
  }

  gmCurrent.gm_pDisplayAdaptersTrigger->mg_astrTexts = _astrAdapterTexts;
  gmCurrent.gm_pDisplayAdaptersTrigger->mg_ctTexts = _ctAdapters;
}

extern void UpdateVideoOptionsButtons(INDEX iSelected)
{
  CVideoOptionsMenu &gmCurrent = _pGUIM->gmVideoOptionsMenu;

  const BOOL _bVideoOptionsChanged = (iSelected != -1);

  const BOOL bOGLEnabled = _pGfx->HasAPI(GAT_OGL);
  const BOOL bD3DEnabled = _pGfx->HasAPI(GAT_D3D8);
  ASSERT(bOGLEnabled || bD3DEnabled);

  CDisplayAdapter &da = _pGfx->gl_gaAPI[SwitchToAPI(gmCurrent.gm_pDisplayAPITrigger->mg_iSelected)]
    .ga_adaAdapter[gmCurrent.gm_pDisplayAdaptersTrigger->mg_iSelected];

  // number of available preferences is higher if video setup is custom
  gmCurrent.gm_pDisplayPrefsTrigger->mg_ctTexts = 3;
  if (sam_iVideoSetup == 3) gmCurrent.gm_pDisplayPrefsTrigger->mg_ctTexts++;

  // enumerate adapters
  FillAdaptersList();

  // show or hide buttons
  gmCurrent.gm_pDisplayAPITrigger->mg_bEnabled = bOGLEnabled && bD3DEnabled;
  gmCurrent.gm_pDisplayAdaptersTrigger->mg_bEnabled = _ctAdapters>1;
  gmCurrent.gm_pApply->mg_bEnabled = _bVideoOptionsChanged;
  // determine which should be visible

  gmCurrent.gm_pWindowModeTrigger->mg_bEnabled = TRUE;

  if (da.da_ulFlags&DAF_FULLSCREENONLY) {
    gmCurrent.gm_pWindowModeTrigger->mg_bEnabled = FALSE;
    gmCurrent.gm_pWindowModeTrigger->mg_iSelected = E_WM_FULLSCREEN;
    gmCurrent.gm_pWindowModeTrigger->ApplyCurrentSelection();
  }

  gmCurrent.gm_pBitsPerPixelTrigger->mg_bEnabled = TRUE;

  // If not fullscreen
  if (gmCurrent.gm_pWindowModeTrigger->mg_iSelected != E_WM_FULLSCREEN) {
    gmCurrent.gm_pBitsPerPixelTrigger->mg_bEnabled = FALSE;
    gmCurrent.gm_pBitsPerPixelTrigger->mg_iSelected = DepthToSwitch(DD_DEFAULT);
    gmCurrent.gm_pBitsPerPixelTrigger->ApplyCurrentSelection();
  } else if (da.da_ulFlags&DAF_16BITONLY) {
    gmCurrent.gm_pBitsPerPixelTrigger->mg_bEnabled = FALSE;
    gmCurrent.gm_pBitsPerPixelTrigger->mg_iSelected = DepthToSwitch(DD_16BIT);
    gmCurrent.gm_pBitsPerPixelTrigger->ApplyCurrentSelection();
  }

  // remember current selected resolution
  PIX2D vpixSize;
  ResolutionToSize(gmCurrent.gm_pResolutionsTrigger->mg_iSelected, vpixSize);

  // select same resolution again if possible
  FillResolutionsList();
  SizeToResolution(vpixSize, gmCurrent.gm_pResolutionsTrigger->mg_iSelected);

  // apply adapter and resolutions
  gmCurrent.gm_pDisplayAdaptersTrigger->ApplyCurrentSelection();
  gmCurrent.gm_pAspectRatiosTrigger->ApplyCurrentSelection(); // [SSE]
  gmCurrent.gm_pResolutionsTrigger->ApplyCurrentSelection();
}

extern void InitVideoOptionsButtons(void)
{
  CVideoOptionsMenu &gmCurrent = _pGUIM->gmVideoOptionsMenu;

  // Limit to existing window modes
  INDEX iWindowMode = Clamp(sam_iWindowMode, (INDEX)E_WM_WINDOWED, (INDEX)E_WM_FULLSCREEN);
  gmCurrent.gm_pWindowModeTrigger->mg_iSelected = iWindowMode;

  gmCurrent.gm_pDisplayAPITrigger->mg_iSelected = APIToSwitch((GfxAPIType)(INDEX)sam_iGfxAPI);
  gmCurrent.gm_pDisplayAdaptersTrigger->mg_iSelected = sam_iDisplayAdapter;
  gmCurrent.gm_pBitsPerPixelTrigger->mg_iSelected = DepthToSwitch((enum DisplayDepth)(INDEX)sam_iDisplayDepth);

  // [SSE] Find aspect ratio and the resolution within it
  PIX2D vScreen(sam_iScreenSizeI, sam_iScreenSizeJ);
  SizeToAspectRatio(vScreen, gmCurrent.gm_pAspectRatiosTrigger->mg_iSelected);

  FillResolutionsList();
  SizeToResolution(vScreen, gmCurrent.gm_pResolutionsTrigger->mg_iSelected);

  gmCurrent.gm_pDisplayPrefsTrigger->mg_iSelected = Clamp(int(sam_iVideoSetup), 0, 3);

  gmCurrent.gm_pWindowModeTrigger->ApplyCurrentSelection();
  gmCurrent.gm_pDisplayPrefsTrigger->ApplyCurrentSelection();
  gmCurrent.gm_pDisplayAPITrigger->ApplyCurrentSelection();
  gmCurrent.gm_pDisplayAdaptersTrigger->ApplyCurrentSelection();
  gmCurrent.gm_pAspectRatiosTrigger->ApplyCurrentSelection(); // [SSE]
  gmCurrent.gm_pResolutionsTrigger->ApplyCurrentSelection();
  gmCurrent.gm_pBitsPerPixelTrigger->ApplyCurrentSelection();
}

static void ApplyVideoOptions(void)
{
  CVideoOptionsMenu &gmCurrent = _pGUIM->gmVideoOptionsMenu;

  // Remember old video settings
  sam_old_iWindowMode = sam_iWindowMode;
  sam_old_iScreenSizeI = sam_iScreenSizeI;
  sam_old_iScreenSizeJ = sam_iScreenSizeJ;
  sam_old_iDisplayDepth = sam_iDisplayDepth;
  sam_old_iDisplayAdapter = sam_iDisplayAdapter;
  sam_old_iGfxAPI = sam_iGfxAPI;
  sam_old_iVideoSetup = sam_iVideoSetup;

  // [SSE] Different window modes
  INDEX iWindowMode = gmCurrent.gm_pWindowModeTrigger->mg_iSelected;

  PIX2D vpixWindowSize;
  ResolutionToSize(gmCurrent.gm_pResolutionsTrigger->mg_iSelected, vpixWindowSize);
  enum GfxAPIType gat = SwitchToAPI(gmCurrent.gm_pDisplayAPITrigger->mg_iSelected);
  enum DisplayDepth dd = SwitchToDepth(gmCurrent.gm_pBitsPerPixelTrigger->mg_iSelected);
  const INDEX iAdapter = gmCurrent.gm_pDisplayAdaptersTrigger->mg_iSelected;

  // setup preferences
  extern INDEX _iLastPreferences;
  if (sam_iVideoSetup == 3) _iLastPreferences = 3;
  sam_iVideoSetup = gmCurrent.gm_pDisplayPrefsTrigger->mg_iSelected;

  // force fullscreen mode if needed
  CDisplayAdapter &da = _pGfx->gl_gaAPI[gat].ga_adaAdapter[iAdapter];

  if (da.da_ulFlags & DAF_FULLSCREENONLY) {
    iWindowMode = E_WM_FULLSCREEN;
  }

  if (da.da_ulFlags & DAF_16BITONLY) {
    dd = DD_16BIT;
  }

  // force window to always be in default colors
  if (iWindowMode != E_WM_FULLSCREEN) {
    dd = DD_DEFAULT;
  }

  // (try to) set mode
  StartNewMode(gat, iAdapter, vpixWindowSize(1), vpixWindowSize(2), dd, iWindowMode);

  // refresh buttons
  InitVideoOptionsButtons();
  UpdateVideoOptionsButtons(-1);

  // ask user to keep or restore
  if (iWindowMode == E_WM_FULLSCREEN) {
    VideoConfirm();
  }
}

static void RevertVideoSettings(void)
{
  // restore previous variables
  sam_iWindowMode = sam_old_iWindowMode;
  sam_iScreenSizeI = sam_old_iScreenSizeI;
  sam_iScreenSizeJ = sam_old_iScreenSizeJ;
  sam_iDisplayDepth = sam_old_iDisplayDepth;
  sam_iDisplayAdapter = sam_old_iDisplayAdapter;
  sam_iGfxAPI = sam_old_iGfxAPI;
  sam_iVideoSetup = sam_old_iVideoSetup;

  // update the video mode
  extern void ApplyVideoMode(void);
  ApplyVideoMode();

  // refresh buttons
  InitVideoOptionsButtons();
  UpdateVideoOptionsButtons(-1);
}

void InitActionsForVideoOptionsMenu()
{
  // Prepare arrays with window resolutions
  PrepareVideoResolutions();

  CVideoOptionsMenu &gmCurrent = _pGUIM->gmVideoOptionsMenu;

  gmCurrent.gm_pDisplayPrefsTrigger->mg_pOnTriggerChange = &UpdateVideoOptionsButtons;
  gmCurrent.gm_pDisplayAPITrigger->mg_pOnTriggerChange = &UpdateVideoOptionsButtons;
  gmCurrent.gm_pDisplayAdaptersTrigger->mg_pOnTriggerChange = &UpdateVideoOptionsButtons;
  gmCurrent.gm_pWindowModeTrigger->mg_pOnTriggerChange = &UpdateVideoOptionsButtons;
  gmCurrent.gm_pAspectRatiosTrigger->mg_pOnTriggerChange = &UpdateVideoOptionsButtons; // [SSE]
  gmCurrent.gm_pResolutionsTrigger->mg_pOnTriggerChange = &UpdateVideoOptionsButtons;
  gmCurrent.gm_pBitsPerPixelTrigger->mg_pOnTriggerChange = &UpdateVideoOptionsButtons;
  gmCurrent.gm_pVideoRendering->mg_pActivatedFunction = &CMenuStarters::RenderingOptions;
  gmCurrent.gm_pApply->mg_pActivatedFunction = &ApplyVideoOptions;
}

// ------------------------ CAudioOptionsMenu implementation
extern void RefreshSoundFormat(void)
{
  CAudioOptionsMenu &gmCurrent = _pGUIM->gmAudioOptionsMenu;

  switch (_pSound->GetFormat())
  {
    case CSoundLibrary::SF_NONE:     {gmCurrent.gm_pFrequencyTrigger->mg_iSelected = 0; break; }
    case CSoundLibrary::SF_11025_16: {gmCurrent.gm_pFrequencyTrigger->mg_iSelected = 1; break; }
    case CSoundLibrary::SF_22050_16: {gmCurrent.gm_pFrequencyTrigger->mg_iSelected = 2; break; }
    case CSoundLibrary::SF_44100_16: {gmCurrent.gm_pFrequencyTrigger->mg_iSelected = 3; break; }
    default:                          gmCurrent.gm_pFrequencyTrigger->mg_iSelected = 0;
  }

  gmCurrent.gm_pAudioAutoTrigger->mg_iSelected = Clamp(sam_bAutoAdjustAudio, 0, 1);
  gmCurrent.gm_pAudioAPITrigger->mg_iSelected = Clamp(_pShell->GetINDEX("snd_iInterface"), 0L, 2L);

  gmCurrent.gm_pEffectsVolume->mg_iMinPos = 0;
  gmCurrent.gm_pEffectsVolume->mg_iMaxPos = VOLUME_STEPS;
  gmCurrent.gm_pEffectsVolume->mg_iCurPos = (INDEX)(_pShell->GetFLOAT("snd_fSoundVolume")*VOLUME_STEPS + 0.5f);
  gmCurrent.gm_pEffectsVolume->ApplyCurrentPosition();

  gmCurrent.gm_pMusicVolume->mg_iMinPos = 0;
  gmCurrent.gm_pMusicVolume->mg_iMaxPos = VOLUME_STEPS;
  gmCurrent.gm_pMusicVolume->mg_iCurPos = (INDEX)(_pShell->GetFLOAT("snd_fMusicVolume")*VOLUME_STEPS + 0.5f);
  gmCurrent.gm_pMusicVolume->ApplyCurrentPosition();

  gmCurrent.gm_pAudioAutoTrigger->ApplyCurrentSelection();
  gmCurrent.gm_pAudioAPITrigger->ApplyCurrentSelection();
  gmCurrent.gm_pFrequencyTrigger->ApplyCurrentSelection();
}

static void ApplyAudioOptions(void)
{
  CAudioOptionsMenu &gmCurrent = _pGUIM->gmAudioOptionsMenu;

  sam_bAutoAdjustAudio = gmCurrent.gm_pAudioAutoTrigger->mg_iSelected;
  if (sam_bAutoAdjustAudio) {
    _pShell->Execute("include \"Scripts\\Addons\\SFX-AutoAdjust.ini\"");
  } else {
    _pShell->SetINDEX("snd_iInterface", gmCurrent.gm_pAudioAPITrigger->mg_iSelected);

    switch (gmCurrent.gm_pFrequencyTrigger->mg_iSelected)
    {
      case 0: {_pSound->SetFormat(CSoundLibrary::SF_NONE); break; }
      case 1: {_pSound->SetFormat(CSoundLibrary::SF_11025_16); break; }
      case 2: {_pSound->SetFormat(CSoundLibrary::SF_22050_16); break; }
      case 3: {_pSound->SetFormat(CSoundLibrary::SF_44100_16); break; }
      default: _pSound->SetFormat(CSoundLibrary::SF_NONE);
    }
  }

  RefreshSoundFormat();
  snd_iFormat = _pSound->GetFormat();
}

static void OnWaveVolumeChange(INDEX iCurPos)
{
  _pShell->SetFLOAT("snd_fSoundVolume", iCurPos / FLOAT(VOLUME_STEPS));
}

static void WaveSliderChange(void)
{
  CAudioOptionsMenu &gmCurrent = _pGUIM->gmAudioOptionsMenu;

  gmCurrent.gm_pEffectsVolume->mg_iCurPos -= 5;
  gmCurrent.gm_pEffectsVolume->ApplyCurrentPosition();
}

static void FrequencyTriggerChange(INDEX iDummy)
{
  CAudioOptionsMenu &gmCurrent = _pGUIM->gmAudioOptionsMenu;

  sam_bAutoAdjustAudio = 0;
  gmCurrent.gm_pAudioAutoTrigger->mg_iSelected = 0;
  gmCurrent.gm_pAudioAutoTrigger->ApplyCurrentSelection();
}

static void MPEGSliderChange(void)
{
  CAudioOptionsMenu &gmCurrent = _pGUIM->gmAudioOptionsMenu;

  gmCurrent.gm_pMusicVolume->mg_iCurPos -= 5;
  gmCurrent.gm_pMusicVolume->ApplyCurrentPosition();
}

static void OnMPEGVolumeChange(INDEX iCurPos)
{
  _pShell->SetFLOAT("snd_fMusicVolume", iCurPos / FLOAT(VOLUME_STEPS));
}

void InitActionsForAudioOptionsMenu()
{
  CAudioOptionsMenu &gmCurrent = _pGUIM->gmAudioOptionsMenu;

  gmCurrent.gm_pFrequencyTrigger->mg_pOnTriggerChange = FrequencyTriggerChange;
  gmCurrent.gm_pEffectsVolume->mg_pOnSliderChange = &OnWaveVolumeChange;
  gmCurrent.gm_pEffectsVolume->mg_pActivatedFunction = WaveSliderChange;
  gmCurrent.gm_pMusicVolume->mg_pOnSliderChange = &OnMPEGVolumeChange;
  gmCurrent.gm_pMusicVolume->mg_pActivatedFunction = MPEGSliderChange;
  gmCurrent.gm_pApply->mg_pActivatedFunction = &ApplyAudioOptions;
}

// ------------------------ CVarMenu implementation
static void VarApply(void)
{
  CVarMenu &gmCurrent = _pGUIM->gmVarMenu;

  FlushVarSettings(TRUE);
  gmCurrent.EndMenu();
  gmCurrent.StartMenu();
}

void InitActionsForVarMenu() {
  _pGUIM->gmVarMenu.gm_pApply->mg_pActivatedFunction = &VarApply;
}

// ------------------------ CServersMenu implementation

extern CButtonWidget mgServerColumn[7];
extern CEditWidget mgServerFilter[7];

static void SortByColumn(int i)
{
  CServersMenu &gmCurrent = _pGUIM->gmServersMenu;

  if (gmCurrent.gm_pList->mg_iSort == i) {
    gmCurrent.gm_pList->mg_bSortDown = !gmCurrent.gm_pList->mg_bSortDown;
  } else {
    gmCurrent.gm_pList->mg_bSortDown = FALSE;
  }
  gmCurrent.gm_pList->mg_iSort = i;
}

static void SortByServer(void) { SortByColumn(0); }
static void SortByMap(void)    { SortByColumn(1); }
static void SortByPing(void)   { SortByColumn(2); }
static void SortByPlayers(void){ SortByColumn(3); }
static void SortByGame(void)   { SortByColumn(4); }
static void SortByMod(void)    { SortByColumn(5); }
static void SortByVer(void)    { SortByColumn(6); }

extern void RefreshServerList(void)
{
  _pNetwork->EnumSessions(_pGUIM->gmServersMenu.m_bInternet);
}

void RefreshServerListManually(void)
{
  ChangeToMenu(&_pGUIM->gmServersMenu); // this refreshes the list and sets focuses
}

void InitActionsForServersMenu() {
  _pGUIM->gmServersMenu.gm_pRefresh->mg_pActivatedFunction = &RefreshServerList;

  mgServerColumn[0].mg_pActivatedFunction = SortByServer;
  mgServerColumn[1].mg_pActivatedFunction = SortByMap;
  mgServerColumn[2].mg_pActivatedFunction = SortByPing;
  mgServerColumn[3].mg_pActivatedFunction = SortByPlayers;
  mgServerColumn[4].mg_pActivatedFunction = SortByGame;
  mgServerColumn[5].mg_pActivatedFunction = SortByMod;
  mgServerColumn[6].mg_pActivatedFunction = SortByVer;
}

// ------------------------ CNetworkMenu implementation
void InitActionsForNetworkMenu()
{
  CNetworkMenu &gmCurrent = _pGUIM->gmNetworkMenu;

  gmCurrent.gm_pJoin->mg_pActivatedFunction = &CMenuStarters::NetworkJoin;
  gmCurrent.gm_pStart->mg_pActivatedFunction = &CMenuStarters::NetworkStart;
  gmCurrent.gm_pQuickLoad->mg_pActivatedFunction = &CMenuStarters::NetworkQuickLoad;
  gmCurrent.gm_pLoad->mg_pActivatedFunction = &CMenuStarters::NetworkLoad;
}

// ------------------------ CNetworkJoinMenu implementation
void InitActionsForNetworkJoinMenu()
{
  CNetworkJoinMenu &gmCurrent = _pGUIM->gmNetworkJoinMenu;

  gmCurrent.gm_pLAN->mg_pActivatedFunction = &CMenuStarters::SelectServerLAN;
  gmCurrent.gm_pNET->mg_pActivatedFunction = &CMenuStarters::SelectServerNET;
  gmCurrent.gm_pOpen->mg_pActivatedFunction = &CMenuStarters::NetworkOpen;
}

// ------------------------ CNetworkStartMenu implementation
extern void UpdateNetworkLevel(INDEX iDummy)
{
  CTString strLevel = _pGame->GetCustomLevel();

  ValidateLevelForFlags(strLevel,
    GetSpawnFlagsForGameType(_pGUIM->gmNetworkStartMenu.gm_pGameType->mg_iSelected));

  _pGame->SetCustomLevel(strLevel);

  _pGUIM->gmNetworkStartMenu.gm_pLevel->mg_strText = FindLevelByFileName(_pGame->GetCustomLevel()).li_strName;
}

void InitActionsForNetworkStartMenu()
{
  CNetworkStartMenu &gmCurrent = _pGUIM->gmNetworkStartMenu;

  gmCurrent.gm_pLevel->mg_pActivatedFunction = &CMenuStarters::SelectLevelFromNetwork;
  gmCurrent.gm_pGameOptions->mg_pActivatedFunction = &CMenuStarters::GameOptionsFromNetwork;
  gmCurrent.gm_pStart->mg_pActivatedFunction = &CMenuStarters::SelectPlayersFromNetwork;
}

// ------------------------ CSelectPlayersMenu implementation
static INDEX FindUnusedPlayer(void)
{
  INDEX iPlayer = 0;

  for (; iPlayer < 8; iPlayer++)
  {
    BOOL bUsed = FALSE;

    for (INDEX iLocal = 0; iLocal < 4; iLocal++)
    {
      if (_pGame->GetMenuLocalPlayers()[iLocal] == iPlayer) {
        bUsed = TRUE;
        break;
      }
    }
    if (!bUsed) {
      return iPlayer;
    }
  }

  ASSERT(FALSE);
  return iPlayer;
}

extern void SelectPlayersFillMenu(void)
{
  CSelectPlayersMenu &gmCurrent = _pGUIM->gmSelectPlayersMenu;

  gmCurrent.gm_pPlayer0Change->mg_iLocalPlayer = 0;
  gmCurrent.gm_pPlayer1Change->mg_iLocalPlayer = 1;
  gmCurrent.gm_pPlayer2Change->mg_iLocalPlayer = 2;
  gmCurrent.gm_pPlayer3Change->mg_iLocalPlayer = 3;

  if (gmCurrent.gm_bAllowDedicated && _pGame->GetMenuSplitScreenCfg() == CGame::SSC_DEDICATED) {
    gmCurrent.gm_pDedicated->mg_iSelected = 1;
  } else {
    gmCurrent.gm_pDedicated->mg_iSelected = 0;
  }

  gmCurrent.gm_pDedicated->ApplyCurrentSelection();

  if (gmCurrent.gm_bAllowObserving && _pGame->GetMenuSplitScreenCfg() == CGame::SSC_OBSERVER) {
    gmCurrent.gm_pObserver->mg_iSelected = 1;
  } else {
    gmCurrent.gm_pObserver->mg_iSelected = 0;
  }

  gmCurrent.gm_pObserver->ApplyCurrentSelection();

  if (_pGame->GetMenuSplitScreenCfg() >= CGame::SSC_PLAY1) {
    gmCurrent.gm_pSplitScreenCfg->mg_iSelected = _pGame->GetMenuSplitScreenCfg();
    gmCurrent.gm_pSplitScreenCfg->ApplyCurrentSelection();
  }

  BOOL bHasDedicated = gmCurrent.gm_bAllowDedicated;
  BOOL bHasObserver = gmCurrent.gm_bAllowObserving;
  BOOL bHasPlayers = TRUE;

  if (bHasDedicated && gmCurrent.gm_pDedicated->mg_iSelected) {
    bHasObserver = FALSE;
    bHasPlayers = FALSE;
  }

  if (bHasObserver && gmCurrent.gm_pObserver->mg_iSelected) {
    bHasPlayers = FALSE;
  }

  CWidget *apmg[8];
  memset(apmg, 0, sizeof(apmg));
  INDEX i = 0;

  if (bHasDedicated) {
    gmCurrent.gm_pDedicated->Appear();
    apmg[i++] = gmCurrent.gm_pDedicated;
  } else {
    gmCurrent.gm_pDedicated->Disappear();
  }

  if (bHasObserver) {
    gmCurrent.gm_pObserver->Appear();
    apmg[i++] = gmCurrent.gm_pObserver;
  } else {
    gmCurrent.gm_pObserver->Disappear();
  }

  for (INDEX iLocal = 0; iLocal < 4; iLocal++)
  {
    INDEX &iLocalRef = _pGame->GetMenuLocalPlayers()[iLocal];

    if (iLocalRef < 0 || iLocalRef > 7) {
      iLocalRef = 0;
    }

    for (INDEX iCopy = 0; iCopy < iLocal; iCopy++)
    {
      if (_pGame->GetMenuLocalPlayers()[iCopy] == iLocalRef) {
        iLocalRef = FindUnusedPlayer();
      }
    }
  }

  gmCurrent.gm_pPlayer0Change->Disappear();
  gmCurrent.gm_pPlayer1Change->Disappear();
  gmCurrent.gm_pPlayer2Change->Disappear();
  gmCurrent.gm_pPlayer3Change->Disappear();

  if (bHasPlayers) {
    gmCurrent.gm_pSplitScreenCfg->Appear();
    apmg[i++] = gmCurrent.gm_pSplitScreenCfg;
    gmCurrent.gm_pPlayer0Change->Appear();
    apmg[i++] = gmCurrent.gm_pPlayer0Change;
    if (gmCurrent.gm_pSplitScreenCfg->mg_iSelected >= 1) {
      gmCurrent.gm_pPlayer1Change->Appear();
      apmg[i++] = gmCurrent.gm_pPlayer1Change;
    }
    if (gmCurrent.gm_pSplitScreenCfg->mg_iSelected >= 2) {
      gmCurrent.gm_pPlayer2Change->Appear();
      apmg[i++] = gmCurrent.gm_pPlayer2Change;
    }
    if (gmCurrent.gm_pSplitScreenCfg->mg_iSelected >= 3) {
      gmCurrent.gm_pPlayer3Change->Appear();
      apmg[i++] = gmCurrent.gm_pPlayer3Change;
    }
  } else {
    gmCurrent.gm_pSplitScreenCfg->Disappear();
  }
  apmg[i++] = gmCurrent.gm_pStart;

  // relink
  for (INDEX img = 0; img<8; img++) {
    if (apmg[img] == NULL) {
      continue;
    }
    INDEX imgPred = (img + 8 - 1) % 8;
    for (; imgPred != img; imgPred = (imgPred + 8 - 1) % 8) {
      if (apmg[imgPred] != NULL) {
        break;
      }
    }
    INDEX imgSucc = (img + 1) % 8;
    for (; imgSucc != img; imgSucc = (imgSucc + 1) % 8) {
      if (apmg[imgSucc] != NULL) {
        break;
      }
    }
    apmg[img]->mg_pmgUp = apmg[imgPred];
    apmg[img]->mg_pmgDown = apmg[imgSucc];
  }

  gmCurrent.gm_pPlayer0Change->SetPlayerText();
  gmCurrent.gm_pPlayer1Change->SetPlayerText();
  gmCurrent.gm_pPlayer2Change->SetPlayerText();
  gmCurrent.gm_pPlayer3Change->SetPlayerText();

  if (bHasPlayers && gmCurrent.gm_pSplitScreenCfg->mg_iSelected >= 1) {
    gmCurrent.gm_pNotes->mg_strText = TRANS("Make sure you set different controls for each player!");
  } else {
    gmCurrent.gm_pNotes->mg_strText = "";
  }
}

extern void SelectPlayersApplyMenu(void)
{
  CSelectPlayersMenu &gmCurrent = _pGUIM->gmSelectPlayersMenu;

  if (gmCurrent.gm_bAllowDedicated && gmCurrent.gm_pDedicated->mg_iSelected) {
    _pGame->SetMenuSplitScreenCfg(CGame::SSC_DEDICATED);
    return;
  }

  if (gmCurrent.gm_bAllowObserving && gmCurrent.gm_pObserver->mg_iSelected) {
    _pGame->SetMenuSplitScreenCfg(CGame::SSC_OBSERVER);
    return;
  }

  _pGame->SetMenuSplitScreenCfg((enum CGame::SplitScreenCfg) gmCurrent.gm_pSplitScreenCfg->mg_iSelected);
}

static void UpdateSelectPlayers(INDEX i)
{
  SelectPlayersApplyMenu();
  SelectPlayersFillMenu();
}

void InitActionsForSelectPlayersMenu()
{
  CSelectPlayersMenu &gmCurrent = _pGUIM->gmSelectPlayersMenu;

  gmCurrent.gm_pDedicated->mg_pOnTriggerChange = UpdateSelectPlayers;
  gmCurrent.gm_pObserver->mg_pOnTriggerChange = UpdateSelectPlayers;
  gmCurrent.gm_pSplitScreenCfg->mg_pOnTriggerChange = UpdateSelectPlayers;
}

// ------------------------ CNetworkOpenMenu implementation
void InitActionsForNetworkOpenMenu()
{
  _pGUIM->gmNetworkOpenMenu.gm_pJoin->mg_pActivatedFunction = &CMenuStarters::SelectPlayersFromOpen;
}

// ------------------------ CSplitScreenMenu implementation
void InitActionsForSplitScreenMenu()
{
  CSplitScreenMenu &gmCurrent = _pGUIM->gmSplitScreenMenu;

  gmCurrent.gm_pStart->mg_pActivatedFunction = &CMenuStarters::SplitStart;
  gmCurrent.gm_pQuickLoad->mg_pActivatedFunction = &CMenuStarters::SplitScreenQuickLoad;
  gmCurrent.gm_pLoad->mg_pActivatedFunction = &CMenuStarters::SplitScreenLoad;
}

// ------------------------ CSplitStartMenu implementation
void InitActionsForSplitStartMenu()
{
  CSplitStartMenu &gmCurrent = _pGUIM->gmSplitStartMenu;

  gmCurrent.gm_pLevel->mg_pActivatedFunction = &CMenuStarters::SelectLevelFromSplit;
  gmCurrent.gm_pGameOptions->mg_pActivatedFunction = &CMenuStarters::GameOptionsFromSplitScreen;
  gmCurrent.gm_pStart->mg_pActivatedFunction = &CMenuStarters::SelectPlayersFromSplit;
}

extern void UpdateSplitLevel(INDEX iDummy)
{
  CSplitStartMenu &gmCurrent = _pGUIM->gmSplitStartMenu;

  CTString strLevel = _pGame->GetCustomLevel();

  ValidateLevelForFlags(strLevel, GetSpawnFlagsForGameType(gmCurrent.gm_pGameType->mg_iSelected));

  _pGame->SetCustomLevel(strLevel);

  gmCurrent.gm_pLevel->mg_strText = FindLevelByFileName(_pGame->GetCustomLevel()).li_strName;
}