/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

/* This file contains starter fuctions for all menus. */

#include "StdH.h"
#include <Engine/Build.h>

#include "MenuManager.h"
#include "MenuStarters.h"
#include "MenuStartersAF.h"
#include "MenuStuff.h"
#include "LevelInfo.h"

extern void(*_pAfterLevelChosen)(void);
extern BOOL _bPlayerMenuFromSinglePlayer;

extern CTString _strLastPlayerAppearance;
extern CTString sam_strNetworkSettings;

extern CTFileName _fnmModSelected;
extern CTString _strModURLSelected;
extern CTString _strModServerSelected;


void CMenuStarters::VideoOptions(void)
{
  ChangeToMenu(&_pGUIM->gmVideoOptionsMenu);
}

void CMenuStarters::AudioOptions(void)
{
  ChangeToMenu(&_pGUIM->gmAudioOptionsMenu);
}

void CMenuStarters::SinglePlayer(void)
{
  ChangeToMenu(&_pGUIM->gmSinglePlayerMenu);
}

void CMenuStarters::Network(void)
{
  ChangeToMenu(&_pGUIM->gmNetworkMenu);
}

void CMenuStarters::NetworkJoin(void)
{
  ChangeToMenu(&_pGUIM->gmNetworkJoinMenu);
}

void CMenuStarters::NetworkStart(void)
{
  ChangeToMenu(&_pGUIM->gmNetworkStartMenu);
}

void CMenuStarters::NetworkOpen(void)
{
  ChangeToMenu(&_pGUIM->gmNetworkOpenMenu);
}

void CMenuStarters::SplitScreen(void)
{
  ChangeToMenu(&_pGUIM->gmSplitScreenMenu);
}

void CMenuStarters::SplitStart(void)
{
  ChangeToMenu(&_pGUIM->gmSplitStartMenu);
}

void CMenuStarters::SinglePlayerNewCustom(void)
{
  _pGUIM->gmSinglePlayerNewMenu.gm_pgmParentMenu = &_pGUIM->gmLevelsMenu;
  ChangeToMenu(&_pGUIM->gmSinglePlayerNewMenu);
}

extern CTString sam_strFirstLevel;

void CMenuStarters::SinglePlayerNew(void)
{
  CSinglePlayerNewMenu &gmCurrent = _pGUIM->gmSinglePlayerNewMenu;

  _pGame->SetCustomLevel(sam_strFirstLevel);

  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmSinglePlayerMenu;
  ChangeToMenu(&gmCurrent);
}

// game options var settings
void CMenuStarters::VarGameOptions(void)
{
  CVarMenu &gmCurrent = _pGUIM->gmVarMenu;

  gmCurrent.gm_pTitle->SetText(TRANS("GAME OPTIONS"));
  gmCurrent.gm_fnmMenuCFG = CTFILENAME("Scripts\\Menu\\GameOptions.cfg");
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::SinglePlayerGameOptions(void)
{
  CVarMenu &gmCurrent = _pGUIM->gmVarMenu;

  gmCurrent.gm_pTitle->SetText(TRANS("GAME OPTIONS"));
  gmCurrent.gm_fnmMenuCFG = CTFILENAME("Scripts\\Menu\\SPOptions.cfg");
  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmSinglePlayerMenu;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::GameOptionsFromNetwork(void)
{
  CMenuStarters::VarGameOptions();
  _pGUIM->gmVarMenu.gm_pgmParentMenu = &_pGUIM->gmNetworkStartMenu;
}

void CMenuStarters::GameOptionsFromSplitScreen(void)
{
  CMenuStarters::VarGameOptions();
  _pGUIM->gmVarMenu.gm_pgmParentMenu = &_pGUIM->gmSplitStartMenu;
}

// rendering options var settings
void CMenuStarters::RenderingOptions(void)
{
  CVarMenu &gmCurrent = _pGUIM->gmVarMenu;

  gmCurrent.gm_pTitle->SetText(TRANS("RENDERING OPTIONS"));
  gmCurrent.gm_fnmMenuCFG = CTFILENAME("Scripts\\Menu\\RenderingOptions.cfg");
  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmVideoOptionsMenu;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::CustomizeKeyboard(void)
{
  ChangeToMenu(&_pGUIM->gmCustomizeKeyboardMenu);
}

void CMenuStarters::CustomizeAxis(void)
{
  ChangeToMenu(&_pGUIM->gmCustomizeAxisMenu);
}

void CMenuStarters::Options(void)
{
  _pGUIM->gmOptionsMenu.gm_pgmParentMenu = pgmCurrentMenu;
  ChangeToMenu(&_pGUIM->gmOptionsMenu);
}

void CMenuStarters::CurrentLoad()
{
  if (_gmRunningGameMode == GM_NETWORK) {
    NetworkLoad();
  } else if (_gmRunningGameMode == GM_SPLIT_SCREEN) {
    SplitScreenLoad();
  } else {
    SinglePlayerLoad();
  }
}

void CMenuStarters::CurrentSave()
{
  if (_gmRunningGameMode == GM_NETWORK) {
    CMenuStarters::NetworkSave();
  } else if (_gmRunningGameMode == GM_SPLIT_SCREEN) {
    CMenuStarters::SplitScreenSave();
  } else {
    CMenuStarters::SinglePlayerSave();
  }
}

void CMenuStarters::CurrentQuickLoad()
{
  if (_gmRunningGameMode == GM_NETWORK) {
    NetworkQuickLoad();
  } else if (_gmRunningGameMode == GM_SPLIT_SCREEN) {
    SplitScreenQuickLoad();
  } else {
    SinglePlayerQuickLoad();
  }
}

void CMenuStarters::ChangePlayerFromOptions(void)
{
  _bPlayerMenuFromSinglePlayer = FALSE;
  _pGUIM->gmPlayerProfile.gm_piCurrentPlayer = &_pGame->GetSinglePlayerIndexRef();
  _pGUIM->gmPlayerProfile.gm_pgmParentMenu = &_pGUIM->gmOptionsMenu;
  ChangeToMenu(&_pGUIM->gmPlayerProfile);
}

void CMenuStarters::ChangePlayerFromSinglePlayer(void)
{
  _iLocalPlayer = -1;
  _bPlayerMenuFromSinglePlayer = TRUE;
  _pGUIM->gmPlayerProfile.gm_piCurrentPlayer = &_pGame->GetSinglePlayerIndexRef();
  _pGUIM->gmPlayerProfile.gm_pgmParentMenu = &_pGUIM->gmSinglePlayerMenu;
  ChangeToMenu(&_pGUIM->gmPlayerProfile);
}

void CMenuStarters::ControlsFromPlayer(void)
{
  _pGUIM->gmControls.gm_pgmParentMenu = &_pGUIM->gmPlayerProfile;
  ChangeToMenu(&_pGUIM->gmControls);
}

void CMenuStarters::ControlsFromOptions(void)
{
  _pGUIM->gmControls.gm_pgmParentMenu = &_pGUIM->gmOptionsMenu;
  ChangeToMenu(&_pGUIM->gmControls);
}

void CMenuStarters::HighScore(void)
{
  _pGUIM->gmHighScoreMenu.gm_pgmParentMenu = pgmCurrentMenu;
  ChangeToMenu(&_pGUIM->gmHighScoreMenu);
}

// -------- Servers Menu Functions
void CMenuStarters::SelectServerLAN(void)
{
  CServersMenu &gmCurrent = _pGUIM->gmServersMenu;

  gmCurrent.m_bInternet = FALSE;
  ChangeToMenu(&gmCurrent);
  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmNetworkJoinMenu;
}

void CMenuStarters::SelectServerNET(void)
{
  CServersMenu &gmCurrent = _pGUIM->gmServersMenu;

  gmCurrent.m_bInternet = TRUE;
  ChangeToMenu(&gmCurrent);
  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmNetworkJoinMenu;
}

// -------- Levels Menu Functions
void CMenuStarters::SelectLevelFromSingle(void)
{
  CLevelsMenu &gmCurrent = _pGUIM->gmLevelsMenu;

  FilterLevels(GetSpawnFlagsForGameType(-1));
  _pAfterLevelChosen = &SinglePlayerNewCustom;
  ChangeToMenu(&gmCurrent);
  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmSinglePlayerMenu;
}

void CMenuStarters::SelectLevelFromSplit(void)
{
  CLevelsMenu &gmCurrent = _pGUIM->gmLevelsMenu;

  FilterLevels(GetSpawnFlagsForGameType(_pGUIM->gmSplitStartMenu.gm_pGameType->mg_iSelected));
  _pAfterLevelChosen = &SplitStart;
  ChangeToMenu(&gmCurrent);
  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmSplitStartMenu;
}

void CMenuStarters::SelectLevelFromNetwork(void)
{
  CLevelsMenu &gmCurrent = _pGUIM->gmLevelsMenu;

  FilterLevels(GetSpawnFlagsForGameType(_pGUIM->gmNetworkStartMenu.gm_pGameType->mg_iSelected));
  _pAfterLevelChosen = &NetworkStart;
  ChangeToMenu(&gmCurrent);
  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmNetworkStartMenu;
}

// -------- Players Selection Menu Functions
void CMenuStarters::SelectPlayersFromSplit(void)
{
  CSelectPlayersMenu &gmCurrent = _pGUIM->gmSelectPlayersMenu;

  gmCurrent.gm_bAllowDedicated = FALSE;
  gmCurrent.gm_bAllowObserving = FALSE;
  gmCurrent.gm_pStart->mg_pActivatedFunction = &StartSplitScreenGame;
  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmSplitStartMenu;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::SelectPlayersFromNetwork(void)
{
  CSelectPlayersMenu &gmCurrent = _pGUIM->gmSelectPlayersMenu;

  gmCurrent.gm_bAllowDedicated = TRUE;
  gmCurrent.gm_bAllowObserving = TRUE;
  gmCurrent.gm_pStart->mg_pActivatedFunction = &StartNetworkGame;
  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmNetworkStartMenu;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::SelectPlayersFromNetworkLoad(void)
{
  CSelectPlayersMenu &gmCurrent = _pGUIM->gmSelectPlayersMenu;

  gmCurrent.gm_bAllowDedicated = FALSE;
  gmCurrent.gm_bAllowObserving = TRUE;
  gmCurrent.gm_pStart->mg_pActivatedFunction = &StartNetworkLoadGame;
  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmLoadSaveMenu;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::SelectPlayersFromSplitScreenLoad(void)
{
  CSelectPlayersMenu &gmCurrent = _pGUIM->gmSelectPlayersMenu;

  gmCurrent.gm_bAllowDedicated = FALSE;
  gmCurrent.gm_bAllowObserving = FALSE;
  gmCurrent.gm_pStart->mg_pActivatedFunction = &StartSplitScreenGameLoad;
  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmLoadSaveMenu;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::SelectPlayersFromOpen(void)
{
  CSelectPlayersMenu &gmCurrent = _pGUIM->gmSelectPlayersMenu;

  gmCurrent.gm_bAllowDedicated = FALSE;
  gmCurrent.gm_bAllowObserving = TRUE;
  gmCurrent.gm_pStart->mg_pActivatedFunction = &JoinNetworkGame;
  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmNetworkOpenMenu;
  ChangeToMenu(&gmCurrent);

  /*if (sam_strNetworkSettings=="")*/ {
    NetworkSettings();
    _pGUIM->gmLoadSaveMenu.gm_bNoEscape = TRUE;
    _pGUIM->gmLoadSaveMenu.gm_pgmParentMenu = &_pGUIM->gmNetworkOpenMenu;
    _pGUIM->gmLoadSaveMenu.gm_pgmNextMenu = &gmCurrent;
  }
}

void CMenuStarters::SelectPlayersFromServers(void)
{
  CSelectPlayersMenu &gmCurrent = _pGUIM->gmSelectPlayersMenu;

  gmCurrent.gm_bAllowDedicated = FALSE;
  gmCurrent.gm_bAllowObserving = TRUE;
  gmCurrent.gm_pStart->mg_pActivatedFunction = &JoinNetworkGame;
  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmServersMenu;
  ChangeToMenu(&gmCurrent);

  /*if (sam_strNetworkSettings=="")*/ {
    NetworkSettings();
    _pGUIM->gmLoadSaveMenu.gm_bNoEscape = TRUE;
    _pGUIM->gmLoadSaveMenu.gm_pgmParentMenu = &_pGUIM->gmServersMenu;
    _pGUIM->gmLoadSaveMenu.gm_pgmNextMenu = &gmCurrent;
  }
}

// -------- Save/Load Menu Calling Functions
void CMenuStarters::PlayerModelLoad(void)
{
  CLoadSaveMenu &gmCurrent = _pGUIM->gmLoadSaveMenu;

  gmCurrent.gm_pTitle->SetText(TRANS("CHOOSE MODEL"));
  gmCurrent.gm_bAllowThumbnails = TRUE;
  gmCurrent.gm_iSortType = LSSORT_FILEUP;
  gmCurrent.gm_bSave = FALSE;
  gmCurrent.gm_bManage = FALSE;
  gmCurrent.gm_fnmDirectory = CTString("Models\\Player\\");
  gmCurrent.gm_fnmSelected = _strLastPlayerAppearance;
  gmCurrent.gm_fnmExt = CTString(".amc");
  gmCurrent.gm_pAfterFileChosen = &LSLoadPlayerModel;
  gmCurrent.gm_pNotes->SetText("");

  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmPlayerProfile;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::ControlsLoad(void)
{
  CLoadSaveMenu &gmCurrent = _pGUIM->gmLoadSaveMenu;

  gmCurrent.gm_pTitle->SetText(TRANS("LOAD CONTROLS"));
  gmCurrent.gm_bAllowThumbnails = FALSE;
  gmCurrent.gm_iSortType = LSSORT_FILEUP;
  gmCurrent.gm_bSave = FALSE;
  gmCurrent.gm_bManage = FALSE;
  gmCurrent.gm_fnmDirectory = CTString("Controls\\");
  gmCurrent.gm_fnmSelected = CTString("");
  gmCurrent.gm_fnmExt = CTString(".ctl");
  gmCurrent.gm_pAfterFileChosen = &LSLoadControls;
  gmCurrent.gm_pNotes->SetText("");

  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmControls;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::CustomLoad(void)
{
  CLoadSaveMenu &gmCurrent = _pGUIM->gmLoadSaveMenu;

  gmCurrent.gm_pTitle->SetText(TRANS("ADVANCED OPTIONS"));
  gmCurrent.gm_bAllowThumbnails = FALSE;
  gmCurrent.gm_iSortType = LSSORT_NAMEUP;
  gmCurrent.gm_bSave = FALSE;
  gmCurrent.gm_bManage = FALSE;
  gmCurrent.gm_fnmDirectory = CTString("Scripts\\CustomOptions\\");
  gmCurrent.gm_fnmSelected = CTString("");
  gmCurrent.gm_fnmExt = CTString(".cfg");
  gmCurrent.gm_pAfterFileChosen = &LSLoadCustom;
  gmCurrent.gm_pNotes->SetText("");

  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmOptionsMenu;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::AddonsLoad(void)
{
  CLoadSaveMenu &gmCurrent = _pGUIM->gmLoadSaveMenu;

  gmCurrent.gm_pTitle->SetText(TRANS("EXECUTE ADDON"));
  gmCurrent.gm_bAllowThumbnails = FALSE;
  gmCurrent.gm_iSortType = LSSORT_NAMEUP;
  gmCurrent.gm_bSave = FALSE;
  gmCurrent.gm_bManage = FALSE;
  gmCurrent.gm_fnmDirectory = CTString("Scripts\\Addons\\");
  gmCurrent.gm_fnmSelected = CTString("");
  gmCurrent.gm_fnmExt = CTString(".ini");
  gmCurrent.gm_pAfterFileChosen = &LSLoadAddon;
  gmCurrent.gm_pNotes->SetText("");

  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmOptionsMenu;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::ModsLoad(void)
{
  CLoadSaveMenu &gmCurrent = _pGUIM->gmLoadSaveMenu;

  gmCurrent.gm_pTitle->SetText(TRANS("CHOOSE MOD"));
  gmCurrent.gm_bAllowThumbnails = TRUE;
  gmCurrent.gm_iSortType = LSSORT_NAMEUP;
  gmCurrent.gm_bSave = FALSE;
  gmCurrent.gm_bManage = FALSE;
  gmCurrent.gm_fnmDirectory = CTString("Mods\\");
  gmCurrent.gm_fnmSelected = CTString("");
  gmCurrent.gm_fnmExt = CTString(".des");
  gmCurrent.gm_pAfterFileChosen = &LSLoadMod;

  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmMainMenu;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::NetworkSettings(void)
{
  CLoadSaveMenu &gmCurrent = _pGUIM->gmLoadSaveMenu;

  gmCurrent.gm_pTitle->SetText(TRANS("CONNECTION SETTINGS"));
  gmCurrent.gm_bAllowThumbnails = FALSE;
  gmCurrent.gm_iSortType = LSSORT_FILEUP;
  gmCurrent.gm_bSave = FALSE;
  gmCurrent.gm_bManage = FALSE;
  gmCurrent.gm_fnmDirectory = CTString("Scripts\\NetSettings\\");
  gmCurrent.gm_fnmSelected = sam_strNetworkSettings;
  gmCurrent.gm_fnmExt = CTString(".ini");
  gmCurrent.gm_pAfterFileChosen = &LSLoadNetSettings;

  if (sam_strNetworkSettings == "") {
    gmCurrent.gm_pNotes->SetText(TRANS(
      "Before joining a network game,\n"
      "you have to adjust your connection parameters.\n"
      "Choose one option from the list.\n"
      "If you have problems with connection, you can adjust\n"
      "these parameters again from the Options menu.\n"
      ));
  } else {
    gmCurrent.gm_pNotes->SetText("");
  }

  gmCurrent.gm_pgmParentMenu = &_pGUIM->gmOptionsMenu;
  ChangeToMenu(&gmCurrent);
}


void CMenuStarters::SinglePlayerQuickLoad(void)
{
  CLoadSaveMenu &gmCurrent = _pGUIM->gmLoadSaveMenu;

  _gmMenuGameMode = GM_SINGLE_PLAYER;

  gmCurrent.gm_pTitle->SetText(TRANS("QUICK LOAD"));
  gmCurrent.gm_bAllowThumbnails = TRUE;
  gmCurrent.gm_iSortType = LSSORT_FILEDN;
  gmCurrent.gm_bSave = FALSE;
  gmCurrent.gm_bManage = TRUE;
  gmCurrent.gm_fnmDirectory.PrintF("UserData\\SaveGame\\Player%d\\Quick\\", _pGame->GetSinglePlayerIndex());
  gmCurrent.gm_fnmSelected = CTString("");
  gmCurrent.gm_fnmExt = CTString(".sav");
  gmCurrent.gm_pAfterFileChosen = &LSLoadSinglePlayer;
  SetQuickLoadNotes();

  gmCurrent.gm_pgmParentMenu = pgmCurrentMenu;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::SinglePlayerLoad(void)
{
  CLoadSaveMenu &gmCurrent = _pGUIM->gmLoadSaveMenu;

  _gmMenuGameMode = GM_SINGLE_PLAYER;

  gmCurrent.gm_pTitle->SetText(TRANS("LOAD"));
  gmCurrent.gm_bAllowThumbnails = TRUE;
  gmCurrent.gm_iSortType = LSSORT_FILEDN;
  gmCurrent.gm_bSave = FALSE;
  gmCurrent.gm_bManage = TRUE;
  gmCurrent.gm_fnmDirectory.PrintF("UserData\\SaveGame\\Player%d\\", _pGame->GetSinglePlayerIndex());
  gmCurrent.gm_fnmSelected = CTString("");
  gmCurrent.gm_fnmExt = CTString(".sav");
  gmCurrent.gm_pAfterFileChosen = &LSLoadSinglePlayer;
  gmCurrent.gm_pNotes->SetText("");

  gmCurrent.gm_pgmParentMenu = pgmCurrentMenu;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::SinglePlayerSave(void)
{
  if (_gmRunningGameMode != GM_SINGLE_PLAYER) return;

  // if no live players
  if (_pGame->GetPlayersCount()>0 && _pGame->GetLivePlayersCount() <= 0) {
    // do nothing
    return;
  }

  CLoadSaveMenu &gmCurrent = _pGUIM->gmLoadSaveMenu;

  _gmMenuGameMode = GM_SINGLE_PLAYER;

  gmCurrent.gm_pTitle->SetText(TRANS("SAVE"));
  gmCurrent.gm_bAllowThumbnails = TRUE;
  gmCurrent.gm_iSortType = LSSORT_FILEDN;
  gmCurrent.gm_bSave = TRUE;
  gmCurrent.gm_bManage = TRUE;
  gmCurrent.gm_fnmDirectory.PrintF("UserData\\SaveGame\\Player%d\\", _pGame->GetSinglePlayerIndex());
  gmCurrent.gm_fnmSelected = CTString("");
  gmCurrent.gm_fnmBaseName = CTString("SaveGame");
  gmCurrent.gm_fnmExt = CTString(".sav");
  gmCurrent.gm_pAfterFileChosen = &LSSaveAnyGame;
  gmCurrent.gm_pNotes->SetText("");
  gmCurrent.gm_strSaveDes = _pGame->GetDefaultGameDescription(TRUE);

  gmCurrent.gm_pgmParentMenu = pgmCurrentMenu;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::DemoLoad(void)
{
  CLoadSaveMenu &gmCurrent = _pGUIM->gmLoadSaveMenu;

  _gmMenuGameMode = GM_DEMO;

  gmCurrent.gm_pTitle->SetText(TRANS("PLAY DEMO"));
  gmCurrent.gm_bAllowThumbnails = TRUE;
  gmCurrent.gm_iSortType = LSSORT_FILEDN;
  gmCurrent.gm_bSave = FALSE;
  gmCurrent.gm_bManage = TRUE;
  gmCurrent.gm_fnmDirectory = CTString("UserData\\Demos\\");
  gmCurrent.gm_fnmSelected = CTString("");
  gmCurrent.gm_fnmExt = CTString(".dem");
  gmCurrent.gm_pAfterFileChosen = &LSLoadDemo;
  gmCurrent.gm_pNotes->SetText("");

  gmCurrent.gm_pgmParentMenu = pgmCurrentMenu;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::DemoSave(void)
{
  CLoadSaveMenu &gmCurrent = _pGUIM->gmLoadSaveMenu;

  if (_gmRunningGameMode == GM_NONE) return;
  _gmMenuGameMode = GM_DEMO;

  gmCurrent.gm_pTitle->SetText(TRANS("RECORD DEMO"));
  gmCurrent.gm_bAllowThumbnails = TRUE;
  gmCurrent.gm_iSortType = LSSORT_FILEUP;
  gmCurrent.gm_bSave = TRUE;
  gmCurrent.gm_bManage = TRUE;
  gmCurrent.gm_fnmDirectory = CTString("UserData\\Demos\\");
  gmCurrent.gm_fnmSelected = CTString("");
  gmCurrent.gm_fnmBaseName = CTString("Demo");
  gmCurrent.gm_fnmExt = CTString(".dem");
  gmCurrent.gm_pAfterFileChosen = &LSSaveDemo;
  gmCurrent.gm_pNotes->SetText("");
  gmCurrent.gm_strSaveDes = _pGame->GetDefaultGameDescription(FALSE);

  gmCurrent.gm_pgmParentMenu = pgmCurrentMenu;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::NetworkQuickLoad(void)
{
  CLoadSaveMenu &gmCurrent = _pGUIM->gmLoadSaveMenu;

  _gmMenuGameMode = GM_NETWORK;

  gmCurrent.gm_pTitle->SetText(TRANS("QUICK LOAD"));
  gmCurrent.gm_bAllowThumbnails = TRUE;
  gmCurrent.gm_iSortType = LSSORT_FILEDN;
  gmCurrent.gm_bSave = FALSE;
  gmCurrent.gm_bManage = TRUE;
  gmCurrent.gm_fnmDirectory = CTString("UserData\\SaveGame\\Network\\Quick\\");
  gmCurrent.gm_fnmSelected = CTString("");
  gmCurrent.gm_fnmExt = CTString(".sav");
  gmCurrent.gm_pAfterFileChosen = &LSLoadNetwork;
  SetQuickLoadNotes();

  gmCurrent.gm_pgmParentMenu = pgmCurrentMenu;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::NetworkLoad(void)
{
  CLoadSaveMenu &gmCurrent = _pGUIM->gmLoadSaveMenu;

  _gmMenuGameMode = GM_NETWORK;

  gmCurrent.gm_pTitle->SetText(TRANS("LOAD"));
  gmCurrent.gm_bAllowThumbnails = TRUE;
  gmCurrent.gm_iSortType = LSSORT_FILEDN;
  gmCurrent.gm_bSave = FALSE;
  gmCurrent.gm_bManage = TRUE;
  gmCurrent.gm_fnmDirectory = CTString("UserData\\SaveGame\\Network\\");
  gmCurrent.gm_fnmSelected = CTString("");
  gmCurrent.gm_fnmExt = CTString(".sav");
  gmCurrent.gm_pAfterFileChosen = &LSLoadNetwork;
  gmCurrent.gm_pNotes->SetText("");

  gmCurrent.gm_pgmParentMenu = pgmCurrentMenu;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::NetworkSave(void)
{
  CLoadSaveMenu &gmCurrent = _pGUIM->gmLoadSaveMenu;

  if (_gmRunningGameMode != GM_NETWORK) return;
  _gmMenuGameMode = GM_NETWORK;

  gmCurrent.gm_pTitle->SetText(TRANS("SAVE"));
  gmCurrent.gm_bAllowThumbnails = TRUE;
  gmCurrent.gm_iSortType = LSSORT_FILEDN;
  gmCurrent.gm_bSave = TRUE;
  gmCurrent.gm_bManage = TRUE;
  gmCurrent.gm_fnmDirectory = CTString("UserData\\SaveGame\\Network\\");
  gmCurrent.gm_fnmSelected = CTString("");
  gmCurrent.gm_fnmBaseName = CTString("SaveGame");
  gmCurrent.gm_fnmExt = CTString(".sav");
  gmCurrent.gm_pAfterFileChosen = &LSSaveAnyGame;
  gmCurrent.gm_pNotes->SetText("");
  gmCurrent.gm_strSaveDes = _pGame->GetDefaultGameDescription(TRUE);

  gmCurrent.gm_pgmParentMenu = pgmCurrentMenu;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::SplitScreenQuickLoad(void)
{
  CLoadSaveMenu &gmCurrent = _pGUIM->gmLoadSaveMenu;

  _gmMenuGameMode = GM_SPLIT_SCREEN;

  gmCurrent.gm_pTitle->SetText(TRANS("QUICK LOAD"));
  gmCurrent.gm_bAllowThumbnails = TRUE;
  gmCurrent.gm_iSortType = LSSORT_FILEDN;
  gmCurrent.gm_bSave = FALSE;
  gmCurrent.gm_bManage = TRUE;
  gmCurrent.gm_fnmDirectory = CTString("UserData\\SaveGame\\SplitScreen\\Quick\\");
  gmCurrent.gm_fnmSelected = CTString("");
  gmCurrent.gm_fnmExt = CTString(".sav");
  gmCurrent.gm_pAfterFileChosen = &LSLoadSplitScreen;
  SetQuickLoadNotes();

  gmCurrent.gm_pgmParentMenu = pgmCurrentMenu;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::SplitScreenLoad(void)
{
  CLoadSaveMenu &gmCurrent = _pGUIM->gmLoadSaveMenu;

  _gmMenuGameMode = GM_SPLIT_SCREEN;

  gmCurrent.gm_pTitle->SetText(TRANS("LOAD"));
  gmCurrent.gm_bAllowThumbnails = TRUE;
  gmCurrent.gm_iSortType = LSSORT_FILEDN;
  gmCurrent.gm_bSave = FALSE;
  gmCurrent.gm_bManage = TRUE;
  gmCurrent.gm_fnmDirectory = CTString("UserData\\SaveGame\\SplitScreen\\");
  gmCurrent.gm_fnmSelected = CTString("");
  gmCurrent.gm_fnmExt = CTString(".sav");
  gmCurrent.gm_pAfterFileChosen = &LSLoadSplitScreen;
  gmCurrent.gm_pNotes->SetText("");

  gmCurrent.gm_pgmParentMenu = pgmCurrentMenu;
  ChangeToMenu(&gmCurrent);
}

void CMenuStarters::SplitScreenSave(void)
{
  CLoadSaveMenu &gmCurrent = _pGUIM->gmLoadSaveMenu;

  if (_gmRunningGameMode != GM_SPLIT_SCREEN) return;
  _gmMenuGameMode = GM_SPLIT_SCREEN;

  gmCurrent.gm_pTitle->SetText(TRANS("SAVE"));
  gmCurrent.gm_bAllowThumbnails = TRUE;
  gmCurrent.gm_iSortType = LSSORT_FILEDN;
  gmCurrent.gm_bSave = TRUE;
  gmCurrent.gm_bManage = TRUE;
  gmCurrent.gm_fnmDirectory = CTString("UserData\\SaveGame\\SplitScreen\\");
  gmCurrent.gm_fnmSelected = CTString("");
  gmCurrent.gm_fnmBaseName = CTString("SaveGame");
  gmCurrent.gm_fnmExt = CTString(".sav");
  gmCurrent.gm_pAfterFileChosen = &LSSaveAnyGame;
  gmCurrent.gm_pNotes->SetText("");
  gmCurrent.gm_strSaveDes = _pGame->GetDefaultGameDescription(TRUE);

  gmCurrent.gm_pgmParentMenu = pgmCurrentMenu;
  ChangeToMenu(&gmCurrent);
}

// -------- Disabled Menu Calling Function
void CMenuStarters::DisabledFunction(void)
{
  CDisabledMenu &gmCurrent = _pGUIM->gmDisabledFunction;

  gmCurrent.gm_pgmParentMenu = pgmCurrentMenu;
  gmCurrent.gm_pButton->SetText(TRANS("The feature is not available in this version!"));
  gmCurrent.gm_pTitle->SetText(TRANS("DISABLED"));
  ChangeToMenu(&gmCurrent);
}